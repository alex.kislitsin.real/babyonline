<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 03.09.19
 * Time: 15:25
 */

namespace app\models;
use yii\base\Model;

class Id extends Model{

    public $id;

    public function attributeLabels(){
        return [
            'id' => 'id',
        ];
    }

    public function rules(){
        return [
            ['id','integer'],
        ];
    }
}