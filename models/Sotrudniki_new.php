<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sotrudniki".
 *
 * @property int $id
 * @property string|null $name
 * @property int $schet
 * @property string|null $inS
 * @property string|null $outS
 * @property string|null $rozdso
 * @property string|null $dol
 * @property string $pitanie
 * @property string $pol
 * @property string $tel
 * @property string $address
 * @property string $coment
 * @property int $prikaz_in
 * @property int $prikaz_out
 * @property string $snils
 * @property string $polis
 * @property int $id_gruppa
 * @property int $seria
 * @property int $num_passport
 * @property string $kem_vidan
 * @property string $date_vidacha
 * @property int $inn
 *
 * @property AllOklad[] $allOklads
 * @property Crujki[] $crujkis
 * @property Gogos[] $gogos
 */
class Sotrudniki_new extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sotrudniki';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['schet', 'prikaz_in', 'prikaz_out', 'id_gruppa', 'seria', 'num_passport', 'inn'], 'integer'],
            [['inS', 'outS', 'rozdso', 'date_vidacha'], 'safe'],
            [['name', 'dol'], 'string', 'max' => 100],
            [['pitanie'], 'string', 'max' => 30],
            [['pol'], 'string', 'max' => 3],
            [['tel'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 200],
            [['coment'], 'string', 'max' => 1000],
            [['snils', 'polis'], 'string', 'max' => 25],
            [['kem_vidan'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'schet' => 'Schet',
            'inS' => 'In S',
            'outS' => 'Out S',
            'rozdso' => 'Rozdso',
            'dol' => 'Dol',
            'pitanie' => 'Pitanie',
            'pol' => 'Pol',
            'tel' => 'Tel',
            'address' => 'Address',
            'coment' => 'Coment',
            'prikaz_in' => 'Prikaz In',
            'prikaz_out' => 'Prikaz Out',
            'snils' => 'Snils',
            'polis' => 'Polis',
            'id_gruppa' => 'Id Gruppa',
            'seria' => 'Seria',
            'num_passport' => 'Num Passport',
            'kem_vidan' => 'Kem Vidan',
            'date_vidacha' => 'Date Vidacha',
            'inn' => 'Inn',
        ];
    }

    /**
     * Gets query for [[AllOklads]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\AllOkladQuery
     */
    /*public function getAllOklads()
    {
        return $this->hasMany(AllOklad::className(), ['idso' => 'id']);
    }*/

    /**
     * Gets query for [[Crujkis]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\CrujkiQuery
     */
    /*public function getCrujkis()
    {
        return $this->hasMany(Crujki::className(), ['id_so' => 'id']);
    }*/

    /**
     * Gets query for [[Gogos]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\GogosQuery
     */
    /*public function getGogos()
    {
        return $this->hasMany(Gogos::className(), ['id' => 'id']);
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\query\SotrudnikiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SotrudnikiQuery(get_called_class());
    }
}
