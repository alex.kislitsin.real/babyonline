<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

class Queries extends Model
{
    public function rules()
    {
        return [];
    }



    public function show_sp($date,$id_group){
        $id_group = preg_replace('/[^0-9]/','',$id_group);
        $date = Yii::$app->formatter->asTime($date);
        $file_query = dirname(__DIR__, 2).'\test\query\ideal_pit_deti_03032020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'date_in' => $date,
            'id_group' => $id_group,
        ])->queryAll();
        return $array;
    }

    public function show_sp_delete_item_date($date,$id_group){
        $id_group = preg_replace('/[^0-9]/','',$id_group);
        $date = Yii::$app->formatter->asTime($date);

        $transaction = Yii::$app->db->beginTransaction();
        try{
            $query = "DELETE FROM gogo WHERE datenotgo = :dat AND id_child IN (SELECT id FROM (SELECT id,`name`,case when LAST_DAY(CONCAT(:dat)) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where
((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))a WHERE a.id_gruppa = :id_group)";
            Yii::$app->db->createCommand($query,[
                'dat' => $date,
                'id_group' => $id_group,
            ])->execute();

            $query1 = "SELECT 0 AS `id`,a.id AS `id_child`,:dat AS `datenotgo` FROM (SELECT id,`name`,case when LAST_DAY(CONCAT(:dat)) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where 
((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))a WHERE a.id_gruppa = :id_group AND a.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = :dat)";
            $array_for_insert = Yii::$app->db->createCommand($query1,[
                'dat' => $date,
                'id_group' => $id_group,
            ])->queryAll();

            Yii::$app->db->createCommand()->batchInsert('gogo', ['id', 'id_child', 'datenotgo'], $array_for_insert)->execute();

            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }

    }



    public function show_sp_delete_item_date_so($date){
        $date = Yii::$app->formatter->asTime($date);

        $transaction = Yii::$app->db->beginTransaction();
        try{
            $query = "DELETE FROM gogos WHERE dateS = :dat AND id IN (SELECT id FROM sotrudniki where
((MONTH(:dat)>=MONTH(`inS`) and YEAR(:dat)=year(`inS`)) or YEAR(:dat)>year(`inS`))
and (((MONTH(:dat)<=month(`outS`) and YEAR(:dat)<=year(`outS`)) or YEAR(:dat)<year(`outS`)) or `outS` is NULL));";
            Yii::$app->db->createCommand($query,[
                'dat' => $date,
            ])->execute();

            $query1 = "SELECT id,:dat as `dat` from sotrudniki where
((MONTH(:dat)>=MONTH(`inS`) and YEAR(:dat)=year(`inS`)) or YEAR(:dat)>year(`inS`))
and (((MONTH(:dat)<=month(`outS`) and YEAR(:dat)<=year(`outS`)) or YEAR(:dat)<year(`outS`)) or `outS` is NULL) AND id NOT IN (SELECT id FROM gogos WHERE dateS = :dat)";
            $array_for_insert = Yii::$app->db->createCommand($query1,[
                'dat' => $date,
            ])->queryAll();

            Yii::$app->db->createCommand()->batchInsert('gogos', ['id', 'dateS'], $array_for_insert)->execute();

            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }

    }



    public function show_sp_calendar($id,$date){
        $id = preg_replace('/[^0-9]/','',$id);
        $date = Yii::$app->formatter->asTime($date);
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $query = "select gogo.*,reason.NAME AS reason from gogo LEFT OUTER JOIN reason ON if(sovsem > 0, sovsem=reason.id, gogo.id=reason.id)  where id_child =:id and month(datenotgo)=:mon and year(datenotgo)=:yea;";
        $array = Yii::$app->db->createCommand($query,[
                'id' => $id,
                'mon' => $month,
                'yea' => $year,
            ]
        )->queryAll();
        return $array;
    }




    public function show_sp_so($date){
        $date = Yii::$app->formatter->asTime($date);
        $file_query = dirname(__DIR__, 2).'\test\query\ideal_pit_so_03032020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'date_in' => $date,
        ])->queryAll();
        return $array;
    }

    public function show_sp_antro($period,$year,$id_group){

        switch ($period){
            case 1:
                $file_query = dirname(__DIR__, 2).'\test\query\antro_spisok_spring_04032020.sql';
                break;
            case 2:
                $file_query = dirname(__DIR__, 2).'\test\query\antro_spisok_otem_04032020.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'year' => $year,
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }

    public function show_sp_antro_excel($year,$current_month,$id_group){

//        $current_month = date('n');
        if ($current_month >= 1 && $current_month < 7){//spring
            $period = 1;
        }elseif($current_month >= 7 && $current_month <= 12){//ourtemn
            $period = 2;
        }

        switch ($period){
            case 1:
                $file_query = dirname(__DIR__, 2).'\test\query\antro-spisok-spring.sql';
                break;
            case 2:
                $file_query = dirname(__DIR__, 2).'\test\query\antro-spisok-otem.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }

    public function show_sp_antro_excel_all_years($id_group){
        switch ($id_group){
            case 0:
                $file_query = dirname(__DIR__, 2).'\test\query\antro-all-year-all-group.sql';
                $query = file_get_contents($file_query);
                $array = Yii::$app->db->createCommand($query,[
                ])->queryAll();
                break;
            default:
                $file_query = dirname(__DIR__, 2).'\test\query\antro-all-year.sql';
                $query = file_get_contents($file_query);
                $array = Yii::$app->db->createCommand($query,[
                    'id_group' => $id_group
                ])->queryAll();
                break;
        }


        return $array;
    }

    public function show_sp_antro_mebel($period,$year){

        switch ($period){
            case 1:
                $m1 = 1;
                $m2 = 6;
                break;
            case 2:
                $m1 = 7;
                $m2 = 12;
                break;
        }
        $file_query = dirname(__DIR__, 2).'\test\query\antro_mebel_04032020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'm1' => $m1,
            'm2' => $m2
        ])->queryAll();
        return $array;
    }

    public function show_tabel_deti($year,$month,$id_group){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_28.sql';
                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_29.sql';
                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_30.sql';
                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_31.sql';
                break;
        }

        $query = file_get_contents($file_query);

        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
                $deti = 'deti'.$month.$year;
                $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
                if (!empty($array)){
                    $query = str_replace('deti',$deti,$query);
                }
            }
            $array = Yii::$app->db->createCommand($query,[
                'yea' => $year,
                'mon' => $month,
                'id_group' => $id_group
            ])->queryAll();
            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }


        return $array;
    }


    public function show_tabel_so($year,$month){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_28_so.sql';
                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_29_so.sql';
                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_30_so.sql';
                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_31_so.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
        ])->queryAll();
        return $array;
    }

    public function show_spravka($year,$month){

        $transaction = Yii::$app->db->beginTransaction();
        try {
            Yii::$app->db->createCommand("CALL count_works_days (:yea,:mon,@c);",[
                'yea' => $year,
                'mon' => $month,
            ])->execute();
            $array_count = Yii::$app->db->createCommand("select @c as `c`;")->queryOne();
            $file_query = dirname(__DIR__, 2).'\test\query\spravka08032020.sql';
            $query = file_get_contents($file_query);
            if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
                $deti = 'deti'.$month.$year;
                $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
                if (!empty($array)){
                    $query = str_replace('deti',$deti,$query);
                }
            }
            $array = Yii::$app->db->createCommand($query,[
                'yea' => $year,
                'mon' => $month,
                'c' => $array_count['c'],
            ])->queryAll();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
        }

        return $array;
    }

    public function show_zabolevaemost($year,$month){

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $file_query = dirname(__DIR__, 2).'\test\query\posesh_month.sql';
            $query = file_get_contents($file_query);

            if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
                $deti = 'deti'.$month.$year;
                $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
                if (!empty($array)){
                    $query = str_replace('deti',$deti,$query);
                }
            }

            Yii::$app->db->createCommand("CALL count_works_days (:yea,:mon,@c);",[
                'yea' => $year,
                'mon' => $month,
            ])->execute();
            $array_count = Yii::$app->db->createCommand("select @c as `c`;")->queryOne();



            $array = Yii::$app->db->createCommand($query,[
                'yea' => $year,
                'mon' => $month,
                'c' => $array_count['c'],
            ])->queryAll();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
        }

        return $array;
    }



    public function show_mantu($id_group){
        $file_query = dirname(__DIR__, 2).'\test\query\journal_mantu.sql';
        $query = file_get_contents($file_query);
        /*if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
            $deti = 'deti'.$month.$year;
            $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
            if (!empty($array)){
                $query = str_replace('deti',$deti,$query);
            }
        }*/
        try {
            $array = Yii::$app->db->createCommand($query, [
                'id_group' => $id_group,
            ])->queryAll();
        } catch (\Exception $e) {
        }
        return $array;

    }

    public function show_mantu_pokazateli(){
        $file_query = dirname(__DIR__, 2).'\test\query\pokazateli_mantu.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query)->queryAll();
        return $array;
    }

    public function show_five($year,$month){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        $now = date('Y-m-d');

        for($i=0;$i<=31;$i++){
            $day = $i < 10 ? '0'.$i : $i;
            $iter_date = date('Y-m-d',strtotime(date($year.'-'.$month.'-'.$day)));

            $model_d_date = Yii::$app->cache->get('array_dates');
            $model_d_antidate = Yii::$app->cache->get('array_antidates');

            if(empty($model_d_date)){
                $model_d_date = array();
            }
            if(empty($model_d_antidate)){
                $model_d_antidate = array();
            }

//            $now = '2020-03-04';


//            $now2 = $now;

            /*while(true){
                if(((date('N', strtotime($now)) < 6) && (!in_array($now,$model_d_date))) || in_array($now,$model_d_antidate)){
                    break;
                }else{
                    $now = date('Y-m-d',strtotime($now. " 1 day"));
                }
            }*/

            if ($iter_date > $now){
                if(verifi_date_five($iter_date,$model_d_date,$model_d_antidate)==4 && (date('m',$iter_date)==date('m')&&date('Y',$iter_date)==date('Y'))){
                    ${'v'.$i} = 1;
                }else{
                    ${'v'.$i} = 0;
                }
            }else{
                ${'v'.$i} = 1;
            }
        }


        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_28.sql';

                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_29.sql';

                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_30.sql';

                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_31.sql';

                break;
        }



        $query = file_get_contents($file_query);



        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
                $deti = 'deti'.$month.$year;
                $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
                if (!empty($array)){

                    $query = str_replace('deti',$deti,$query);
//                    $transaction->rollBack();
//                    return 400;
                }
            }

            switch ($count_days_all){
                case 28:
                    $array = Yii::$app->db->createCommand($query,[
                        'yea' => $year,
                        'mon' => $month,
                        'v1' => $v1,'v2' => $v2,'v3' => $v3,'v4' => $v4,'v5' => $v5,
                        'v6' => $v6,'v7' => $v7,'v8' => $v8,'v9' => $v9,'v10' => $v10,
                        'v11' => $v11,'v12' => $v12,'v13' => $v13,'v14' => $v14,'v15' => $v15,
                        'v16' => $v16,'v17' => $v17,'v18' => $v18,'v19' => $v19,'v20' => $v20,
                        'v21' => $v21,'v22' => $v22,'v23' => $v23,'v24' => $v24,'v25' => $v25,
                        'v26' => $v26,'v27' => $v27,'v28' => $v28
                    ])->queryAll();

                    break;
                case 29:
                    $array = Yii::$app->db->createCommand($query,[
                        'yea' => $year,
                        'mon' => $month,
                        'v1' => $v1,'v2' => $v2,'v3' => $v3,'v4' => $v4,'v5' => $v5,
                        'v6' => $v6,'v7' => $v7,'v8' => $v8,'v9' => $v9,'v10' => $v10,
                        'v11' => $v11,'v12' => $v12,'v13' => $v13,'v14' => $v14,'v15' => $v15,
                        'v16' => $v16,'v17' => $v17,'v18' => $v18,'v19' => $v19,'v20' => $v20,
                        'v21' => $v21,'v22' => $v22,'v23' => $v23,'v24' => $v24,'v25' => $v25,
                        'v26' => $v26,'v27' => $v27,'v28' => $v28,'v29' => $v29
                    ])->queryAll();

                    break;
                case 30:
                    $array = Yii::$app->db->createCommand($query,[
                        'yea' => $year,
                        'mon' => $month,
                        'v1' => $v1,'v2' => $v2,'v3' => $v3,'v4' => $v4,'v5' => $v5,
                        'v6' => $v6,'v7' => $v7,'v8' => $v8,'v9' => $v9,'v10' => $v10,
                        'v11' => $v11,'v12' => $v12,'v13' => $v13,'v14' => $v14,'v15' => $v15,
                        'v16' => $v16,'v17' => $v17,'v18' => $v18,'v19' => $v19,'v20' => $v20,
                        'v21' => $v21,'v22' => $v22,'v23' => $v23,'v24' => $v24,'v25' => $v25,
                        'v26' => $v26,'v27' => $v27,'v28' => $v28,'v29' => $v29,'v30' => $v30
                    ])->queryAll();

                    break;
                case 31:
                    $array = Yii::$app->db->createCommand($query,[
                        'yea' => $year,
                        'mon' => $month,
                        'v1' => $v1,'v2' => $v2,'v3' => $v3,'v4' => $v4,'v5' => $v5,
                        'v6' => $v6,'v7' => $v7,'v8' => $v8,'v9' => $v9,'v10' => $v10,
                        'v11' => $v11,'v12' => $v12,'v13' => $v13,'v14' => $v14,'v15' => $v15,
                        'v16' => $v16,'v17' => $v17,'v18' => $v18,'v19' => $v19,'v20' => $v20,
                        'v21' => $v21,'v22' => $v22,'v23' => $v23,'v24' => $v24,'v25' => $v25,
                        'v26' => $v26,'v27' => $v27,'v28' => $v28,'v29' => $v29,'v30' => $v30,
                        'v31' => $v31
                    ])->queryAll();

                    break;
            }



            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }

        return $array;
    }


    public function show_test(){
        $file_query = dirname(__DIR__, 2).'\test\query\test.sql';
        $query = file_get_contents($file_query);
        $query = str_replace('det','deti',$query);
        $array = Yii::$app->db->createCommand($query)->queryAll();
        return $array;
    }

    public function show_kal($id_group){
        $file_query = dirname(__DIR__, 2).'\test\query\journal_kal.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }

    public function show_journal_injection($id_group){

        $transaction = Yii::$app->db->beginTransaction();
        try{
            $file_query = dirname(__DIR__, 2).'\test\query\journal_injection.sql';
            $query = file_get_contents($file_query);
            if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
                $deti = 'deti'.$month.$year;
                $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
                if (!empty($array)){
                    $query = str_replace('deti',$deti,$query);
                }
            }
            $array = Yii::$app->db->createCommand($query,[
                'yea' => date('Y'),
                'mon' => date('n'),
                'id_group' => $id_group
            ])->queryAll();
            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }

        return $array;
    }

    public function show_tabel_cru($year,$month,$id_crujok){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
            'id_crujok' => $id_crujok
        ])->queryAll();
        return $array;
    }

    public function show_plan_injection(){
        $array = Yii::$app->db->createCommand("call Show_plan_injection_run ();")->queryAll();
        return $array;
    }

    public function show_cru_akt($year,$month){
        $file_query = dirname(__DIR__, 2).'\test\query\cru_akt.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
        ])->queryAll();
        return $array;
    }

    public function show_cru_dogovora($year,$month){
        $file_query = dirname(__DIR__, 2).'\test\query\cru_dogovora.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
        ])->queryAll();
        return $array;
    }

    public function show_dvijenie($dat){
        $file_query = dirname(__DIR__, 2).'\test\query\dvijenie.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'dat' => $dat,
        ])->queryAll();
        return $array;
    }

    public function show_dvijenie_alergiki($dat){
        $file_query = dirname(__DIR__, 2).'\test\query\show_alergiki_ander_dvij.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'dat' => $dat,
        ])->queryAll();
        return $array;
    }

    public function show_hot_button_orz($dat){
        $file_query = dirname(__DIR__, 2).'\test\query\hot_button_orz.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'dat' => $dat,
        ])->queryAll();
        return $array;
    }

    public static  function show_grzdor_index($year, $id_gruppa){
        $file_query = dirname(__DIR__, 2).'\test\query\grzdor_index_08082020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'id_group' => $id_gruppa,
            'year' => $year,
        ])->queryAll();
        return $array;
    }

    public static  function show_grzdor_svodka($year, $id_gruppa){
        $file_query = dirname(__DIR__, 2).'\test\query\grzdor_svodka.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'id_group' => $id_gruppa,
            'year' => $year,
        ])->queryAll();
        return $array;
    }





}
