<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 21.12.19
 * Time: 16:51
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Mantu1 extends ActiveRecord{

    public $id_child;
    public $dat;
    public $el1;
    public $el2;
    public $coment;

    public static function tableName(){
        return 'mantu';
    }

    public function rules(){
        return[
            [['el1','el2','coment'],'trim'],
            [['el1','el2','coment'],'string'],
            [['el1','el2','coment'],'default','value' => ''],
            [['id_child'],'integer'],
            [['dat'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],

        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Педагог',
//            'price' => 'Стоимость',
        ];
    }
} 