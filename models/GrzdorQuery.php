<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Grzdor]].
 *
 * @see Grzdor
 */
class GrzdorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Grzdor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Grzdor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
