<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 17.11.19
 * Time: 14:06
 */

namespace app\models;


use yii\base\Model;
use Yii;

class Injection extends Model{

    const SCENARIO1 = 'get_array_one_child';

    public $id_child;
    
    public $date_inj_v1;
    public $date_inj_v2;
    public $date_inj_v3;
    public $date_inj_r1;
    public $date_inj_r2;
    public $date_inj_r3;
    
    public $seriya_v1;
    public $seriya_v2;
    public $seriya_v3;
    public $seriya_r1;
    public $seriya_r2;
    public $seriya_r3;
    
    public $doza_v1;
    public $doza_v2;
    public $doza_v3;
    public $doza_r1;
    public $doza_r2;
    public $doza_r3;
    
    public $name_preparat_v1;
    public $name_preparat_v2;
    public $name_preparat_v3;
    public $name_preparat_r1;
    public $name_preparat_r2;
    public $name_preparat_r3;
    
    public $reaction_v1;
    public $reaction_v2;
    public $reaction_v3;
    public $reaction_r1;
    public $reaction_r2;
    public $reaction_r3;
    
    public $medotvod_v1;
    public $medotvod_v2;
    public $medotvod_v3;
    public $medotvod_r1;
    public $medotvod_r2;
    public $medotvod_r3;
    
    public $coment_v1;
    public $coment_v2;
    public $coment_v3;
    public $coment_r1;
    public $coment_r2;
    public $coment_r3;
    
    
    
    public $id_inj;
//    public $doza;
//    public $seriya;
//    public $name_preparat;
//    public $reaction;
    /*public $v1;
    public $v2;
    public $v3;
    public $r1;
    public $r2;
    public $r3;*/
    
    public $vv1;
    public $vv2;
    public $vv3;
    public $rr1;
    public $rr2;
    public $rr3;
    
    
//    public $medotvod;
    public $napravlen;

    public function rules(){
        return [
//            [['id_child','date_inj_v1','date_inj_v2','date_inj_v3','date_inj_r1','date_inj_r2','date_inj_r3','id_inj'],'required','message' => false],
//            [['v1','r1','v2','r2','v3','r3'],'safe'],
            [['id_child','id_inj','vv1','rr1','vv2','rr2','vv3','rr3'],'integer'],
            [['vv1','rr1','vv2','rr2','vv3','rr3'], 'default','value' => 0],
            [['date_inj_v1','date_inj_v2','date_inj_v3','date_inj_r1','date_inj_r2','date_inj_r3','napravlen'], 'date','format' => 'php:d.m.Y','message' => false],
            [['date_inj_v1','date_inj_v2','date_inj_v3','date_inj_r1','date_inj_r2','date_inj_r3','napravlen'], 'default','value' => date('1900-01-01')],
            [[
                'doza_v1','doza_v2','doza_v3','doza_r1','doza_r2','doza_r3',
                'seriya_v1','seriya_v2','seriya_v3','seriya_r1','seriya_r2','seriya_r3',
                'name_preparat_v1','name_preparat_v2','name_preparat_v3','name_preparat_r1','name_preparat_r2','name_preparat_r3',
                'medotvod_v1','medotvod_v2','medotvod_v3','medotvod_r1','medotvod_r2','medotvod_r3',
                'coment_v1','coment_v2','coment_v3','coment_r1','coment_r2','coment_r3',
                'reaction_v1','reaction_v2','reaction_v3','reaction_r1','reaction_r2','reaction_r3'], 'trim'],
            [[
                'doza_v1','doza_v2','doza_v3','doza_r1','doza_r2','doza_r3',
                'seriya_v1','seriya_v2','seriya_v3','seriya_r1','seriya_r2','seriya_r3',
                'name_preparat_v1','name_preparat_v2','name_preparat_v3','name_preparat_r1','name_preparat_r2','name_preparat_r3',
                'medotvod_v1','medotvod_v2','medotvod_v3','medotvod_r1','medotvod_r2','medotvod_r3',
                'coment_v1','coment_v2','coment_v3','coment_r1','coment_r2','coment_r3',
                'reaction_v1','reaction_v2','reaction_v3','reaction_r1','reaction_r2','reaction_r3'], 'string'],
            [['doza_v1','doza_v2','doza_v3','doza_r1','doza_r2','doza_r3',
                'seriya_v1','seriya_v2','seriya_v3','seriya_r1','seriya_r2','seriya_r3',
                'name_preparat_v1','name_preparat_v2','name_preparat_v3','name_preparat_r1','name_preparat_r2','name_preparat_r3',
                'medotvod_v1','medotvod_v2','medotvod_v3','medotvod_r1','medotvod_r2','medotvod_r3',
                'coment_v1','coment_v2','coment_v3','coment_r1','coment_r2','coment_r3',
                'reaction_v1','reaction_v2','reaction_v3','reaction_r1','reaction_r2','reaction_r3'],'default','value' => ''],
            [['doza_v1','doza_v2','doza_v3','doza_r1','doza_r2','doza_r3',
                'seriya_v1','seriya_v2','seriya_v3','seriya_r1','seriya_r2','seriya_r3',
                'name_preparat_v1','name_preparat_v2','name_preparat_v3','name_preparat_r1','name_preparat_r2','name_preparat_r3',
                'medotvod_v1','medotvod_v2','medotvod_v3','medotvod_r1','medotvod_r2','medotvod_r3',
                'coment_v1','coment_v2','coment_v3','coment_r1','coment_r2','coment_r3',
                'reaction_v1','reaction_v2','reaction_v3','reaction_r1','reaction_r2','reaction_r3'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[\'\"\&\#\;]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],
            [['id_child','id_inj','vv1','rr1','vv2','rr2','vv3','rr3'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],
            [['date_inj_v1','date_inj_v2','date_inj_v3','date_inj_r1','date_inj_r2','date_inj_r3','napravlen'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],




        ];
    }

    public function attributeLabels(){
        return [
            'date_inj_v1' => 'Дата',
            'id_inj' => 'Наименование',
            'doza_v1' => 'Доза',
            'seriya_v1' => 'Серия',
            'name_preparat_v1' => 'Наим.преп.',
            'reaction_v1' => 'Реакция',
//            'v1' => 'V',
//            'r1' => 'R',
            'medotvod_v1' => 'Медотвод',
            'coment_v1' => 'Примечание',

        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // Валидация по указанному сценарию применяется только для поля "name"
        $scenarios[self::SCENARIO1] = ['id_child','id_inj'];
        return $scenarios;
    }

}

