<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 03.09.19
 * Time: 15:25
 */

namespace app\models;
use yii\base\Model;

class Item_date extends Model{

    public $item_date;

//    public function attributeLabels(){
//        return [
//            'item_date' => 'Дата',
//        ];
//    }

    public function rules(){
        return [
//            [['name'],'required','message' => 'Обязательно для заполнения'],
              ['item_date','date','format' => 'php: d.m.Y']
        ];
    }

//    public function myRule($attr){
//        if(!in_array($this->$attr, ['hello','world'])){
//            $this->addError($attr,'Введите корректные данные');
//        }
//    }

} 