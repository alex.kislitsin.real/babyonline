<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grzdor".
 *
 * @property int $id
 * @property int $child_id
 * @property int $year
 * @property int $number_grzdor
 * @property string|null $comment
 * @property int|null $number_fisculture
 *
 * @property Deti $child
 */
class Grzdor extends \yii\db\ActiveRecord
{
    public $name;

    const FISCULT_TYPE_1 = 1;
    const FISCULT_TYPE_2 = 2;
    const FISCULT_TYPE_3 = 3;
    const FISCULT_TYPE_4 = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grzdor';
    }

    /**
     * @return array
     */
    public static function fiscultTypeLabels()
    {
        return [
            self::FISCULT_TYPE_1 => 'Основная',
            self::FISCULT_TYPE_2 => 'Подготовит.',
            self::FISCULT_TYPE_3 => 'Спец.',
            self::FISCULT_TYPE_4 => 'Освобожден',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name','safe'],
            [['child_id', 'year', 'number_grzdor'], 'required'],
            [['child_id', 'year', 'number_grzdor', 'number_fisculture'], 'integer'],
            [['number_grzdor'],'in','range' => [1,2,3,4,5]],
            [['number_fisculture'],'in','range' => [self::FISCULT_TYPE_1,self::FISCULT_TYPE_2,self::FISCULT_TYPE_3,self::FISCULT_TYPE_4]],
            [['comment'], 'string', 'max' => 255],
            [['comment'], 'default', 'value' => null],
//            [['child_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deti::className(), 'targetAttribute' => ['child_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Фамилия и Имя',
            'child_id' => 'Child ID',
            'year' => 'Year',
            'number_grzdor' => 'Number Grzdor',
            'comment' => 'Диагноз',
            'number_fisculture' => 'Number Fisculture',
        ];
    }

    /**
     * Gets query for [[Child]].
     *
     * @return \yii\db\ActiveQuery|DetiQuery
     */
    public function getChild()
    {
        return $this->hasOne(Deti::className(), ['id' => 'child_id']);
    }


}
