<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CruDate extends Model
{
    public $date_cru;
    public $id_child;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['date_cru','id_child'], 'required','message' => false],

            [['date_cru'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],
            [['id_child'],'filter','filter' => function($value){
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }]
        ];
    }

    public function attributeLabels()
    {
        return [
//            'datbegin' => 'начальная дата',
//            'datend' => 'конечная дата',
//            'search' => 'введите данные для быстрого поиска ребенка',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
