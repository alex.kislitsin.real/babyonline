<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Search extends Model
{
    public $id;
    public $name;
//    public $id_gruppa;
//    public $number_schet;
//    public $start;
//    public $in;
//    public $out;
//    public $rozd;
//    public $otche;
//    public $pol;
//    public $polis;
//    public $snils;
//    public $adress;
//    public $old_id_gruppa;
//    public $perevod;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [





            [['name'], 'required','message' => false],
            [['id'],'safe'],

            [['name'], 'trim'],
            [['name'], 'string'],
            [['name'],'default','value' => ''],


            [['name'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[\'\"\;]/','',$value);
                    $result = str_replace('--','',$result);
                    $result = str_replace('table','',$result);
                    $result = str_replace('drop','',$result);
                    $result = str_replace('select','',$result);
                    $result = str_replace('from','',$result);
                    $result = str_replace('delete','',$result);
                    return $result;
                }],


        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя',
//            'id_gruppa' => 'id_gruppa',
//            'number_schet' => 'Номер лиц.счета',
//            'in' => 'Дата зачисления',
//            'out' => 'Дата отчисления',
//            'rozd' => 'Дата рождения',
//            'start' => 'start',
//            'pol' => 'Пол',
//            'otche' => 'Отчество',
//            'adress' => 'Адрес',
//            'polis' => 'Полис',
//            'snils' => 'Снилс',
//            'old_id_gruppa' => 'old_id_gruppa',
//            'perevod' => 'Дата перевода',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
