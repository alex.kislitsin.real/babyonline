<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 03.09.19
 * Time: 15:25
 */

namespace app\models;
use yii\base\Model;

class Month extends Model{

    public $id;
    public $name;

    public function attributeLabels(){
        return [
            'id' => 'id',
            'name' => 'name',
        ];
    }

    public function rules(){
        return [
              [['id'],'number'],
              ['name','safe']
        ];
    }

//    public function myRule($attr){
//        if(!in_array($this->$attr, ['hello','world'])){
//            $this->addError($attr,'Введите корректные данные');
//        }
//    }

} 