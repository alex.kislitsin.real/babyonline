<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Mantu_new]].
 *
 * @see \app\models\Mantu_new
 */
class MantuQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\Mantu_new[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Mantu_new|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
