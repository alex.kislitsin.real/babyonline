<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\MantuN_new]].
 *
 * @see \app\models\MantuN_new
 */
class MantuNQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\MantuN_new[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\MantuN_new|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
