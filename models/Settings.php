<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Settings extends Model
{
    public $city;
    public $boss;
    public $boss_rod;
    public $lico_cru;
    public $lico_cru2;
    public $medsestra;
    public $day_tabel;
    public $flagActive;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['day_tabel'], 'integer'],
            [['flagActive'], 'boolean'],
            [['city','boss','lico_cru','lico_cru2','medsestra','boss_rod'], 'trim'],
            [['city','boss','lico_cru','lico_cru2','medsestra','boss_rod'], 'string'],
            [['city','boss','lico_cru','lico_cru2','medsestra','boss_rod'],'default','value' => ''],
            [['flagActive'],'default','value' => 0],

            [['day_tabel'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    $result < 2 ? $result = 2 : null;
                    $result > 5 ? $result = 5 : null;
                    return $result;
                }],
            [['city','boss','lico_cru','lico_cru2','medsestra','boss_rod'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[\'\"\;]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],

        ];
    }

    public function attributeLabels()
    {
        return [
            'day_tabel' => 'Количество дней до закрытия табеля',
            'city' => 'Название города в родительном падеже',
            'boss' => 'ФИО руководителя в именительном падеже',
            'boss_rod' => 'ФИО руководителя в родительном падеже',
            'medsestra' => 'ФИО старшей медсестры в именительном падеже',
            'lico_cru' => 'ФИО отв. по кружкам в родительном падеже',
            'lico_cru2' => 'ФИО отв. по кружкам в именительном падеже',
            'flagActive' => 'Автоматическая отправка детодней',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
