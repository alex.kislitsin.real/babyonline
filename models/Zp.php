<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Zp extends Model
{
    public $id_so;
    public $id_oklad;
    public $ppk;
    public $sever;
    public $children;
    public $vred;
    public $percent;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['id_so'], 'required','message' => false],
//            [['id_so','id_oklad','ppk','sever','children','vred','percent'],'message' => false],
            [['ppk'], 'number'],
            [['id_so','id_oklad','sever','children','vred','percent'], 'integer'],
            [['ppk','sever','children','vred'],'default','value' => 0],

            /*[['coment'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    return $result;
                }],

            [['id_child','year'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],

            [['dat'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],*/
        ];
    }

    public function attributeLabels()
    {
        return [
            'ppk' => 'Повышающий коэффициент',
            'sever' => 'Северные',
            'children' => 'Детские',
            'vred' => 'За вредность',
            'percent' => 'Процент от оклада',
            'id_oklad' => 'Должностной оклад',
        ];
    }

    public function show_zp_all_distinct(){
        $query = "select J.id_s,J.dol,J.nameso,sum(summ)[summ2],sum(posle_vicheta_naloga)[posle] from (select sotrudniki.id[id_s],sotrudniki.dol,sotrudniki.name[nameso],case when oklad is not null then convert(numeric (9,2),(
 (((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) + (((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) / 100)*sever)
)) else null end [summ]
,case when oklad is not null then convert(numeric (9,2),(
((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) + (((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) / 100)*sever)) -
((((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) + (((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) / 100)*sever)) - children)*0.13)
)) else null end [posle_vicheta_naloga] from sotrudniki left outer join all_oklad on sotrudniki.id = all_oklad.idso left outer join dol_oklad on all_oklad.id_oklad = dol_oklad.id where outS is null and sotrudniki.dol not like '%латные услуги%')J group by id_s,dol,nameso order by nameso";
        $array = Yii::$app->db->createCommand($query)->queryAll();
        return $array;
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
