<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 03.09.19
 * Time: 15:25
 */

namespace app\models;
use yii\base\Model;

class Period_month extends Model{

    public $id1;
    public $id2;
    public $name1;
    public $name2;

    public function attributeLabels(){
        return [
            'id1' => 'id1',
            'id2' => 'id2',
            'name1' => 'name1',
            'name2' => 'name2',
        ];
    }

    public function rules(){
        return [
              [['id1','id2'],'number'],
              [['name1','name2'],'safe']
        ];
    }

//    public function myRule($attr){
//        if(!in_array($this->$attr, ['hello','world'])){
//            $this->addError($attr,'Введите корректные данные');
//        }
//    }

} 