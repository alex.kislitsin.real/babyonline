<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 21.12.19
 * Time: 16:51
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Crujki extends ActiveRecord{

    public $id;
    public $name;
    public $price;
    public $pedagog;
    public $pn;
    public $vt;
    public $sr;
    public $ch;
    public $pt;
    public $zp;

    public static function tableName(){
        return 'crujki';
    }

    public function rules(){
        return[
            [['name'],'required'],
            [['name','pedagog'],'trim'],
            [['name','pedagog'],'string'],
            [['name','pedagog'],'default','value' => ''],
            [['id','price','pn','vt','sr','ch','pt','zp'],'default','value' => 0],
            [['id','price','pn','vt','sr','ch','pt','zp'],'integer'],
            [['name','pedagog'],'filter','filter' => function($value){
                    $value = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    $result = str_replace('--','',$value);
                    return $result;
                }],
            [['id','price','pn','vt','sr','ch','pt'],'filter','filter' => function($value){
                    $result = preg_replace('/[^0-9]/','',$value);
//                    if ($result < 0)$result = 0;
                    return $result;
                }],
            [['zp'],'filter','filter' => function($value){
                    $result = preg_replace('/[^0-9]/','',$value);
                    if ($result > 100){
                        $x = $result - 100;
                        return $result = $result - $x;
                    }else{
                        return $result;
                    }
                }],

        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название кружка',
            'price' => 'Стоимость кружка',
            'ped' => 'Педагог',
            'zp' => 'Зарплата %',
        ];
    }

    public function show_crujki_edit_modal(){
        $array = "select crujki.*,ifnull(sotrudniki.name,'') as `ped` from crujki left outer join sotrudniki on crujki.id_so = sotrudniki.id ORDER BY category_id,`name`";
        $array = Yii::$app->db->createCommand($array)->queryAll();
        $array = ArrayHelper::index($array,'id');
        return $array;
    }

    public function show_crujki_so(){
        $array_so = "select * from sotrudniki where outS is null order by name";
        $array_so = Yii::$app->db->createCommand($array_so)->queryAll();
        $array_so = ArrayHelper::map($array_so,'id','name');
        return $array_so;
    }

    public function getNamePed($id_crujok){
        $name_ped = Yii::$app->db->createCommand('select sotrudniki.name as `nameso` from crujki left outer join sotrudniki on crujki.id_so = sotrudniki.id where crujki.id =:id_crujok',['id_crujok' => $id_crujok])->queryOne();
        return $name_ped;
    }


} 