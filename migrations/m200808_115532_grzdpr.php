<?php

use yii\db\Migration;

/**
 * Class m200808_115532_grzdpr
 */
class m200808_115532_grzdpr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $query = "CREATE TABLE `grzdor` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `child_id` BIGINT(20) UNSIGNED NOT NULL,
            `year` SMALLINT(6) NOT NULL,
            `number_grzdor` SMALLINT(6) NOT NULL,
            `comment` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
            `number_fisculture` SMALLINT(6) NULL DEFAULT NULL,
            PRIMARY KEY (`child_id`, `year`),
            UNIQUE INDEX `id` (`id`),
            CONSTRAINT `grzdor_fk_child_id` FOREIGN KEY (`child_id`) REFERENCES `deti` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
        )
        COLLATE='utf8_unicode_ci'
        ENGINE=InnoDB
        ;";

        Yii::$app->db->createCommand($query)->execute();



        /*$tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%grzdor}}', [
            'id' => $this->primaryKey(),
            'child_id' => $this->bigInteger()->unsigned()->notNull(),
            'year' => $this->smallInteger()->notNull(),
            'number_grzdor' => $this->smallInteger()->notNull(),
            'comment' => $this->string(),
            'number_fisculture' => $this->smallInteger()
        ], $tableOptions);

        $this->addForeignKey('grzdor_fk_child_id','{{%grzdor}}','child_id','{{%deti}}','id','CASCADE','CASCADE');
        $this->createIndex('grzdor_idx_primary','{{%grzdor}}',['child_id','year','number_grzdor']);*/

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('grzdor_fk_child_id','{{%grzdor}}');
        $this->dropTable('{{%grzdor}}');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200808_115532_grzdpr cannot be reverted.\n";

        return false;
    }
    */
}
