<?php

use yii\db\Migration;

/**
 * Class m201108_145419_add_covidka
 */
class m201108_145419_add_covidka extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    \Yii::$app->db->createCommand('insert into reason (id,`name`,id_parent) values (68,"Covid-19",1)')->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201108_145419_add_covidka cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201108_145419_add_covidka cannot be reverted.\n";

        return false;
    }
    */
}
