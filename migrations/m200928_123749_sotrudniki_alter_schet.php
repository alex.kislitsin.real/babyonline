<?php

use yii\db\Migration;

/**
 * Class m200928_123749_sotrudniki_alter_schet
 */
class m200928_123749_sotrudniki_alter_schet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('sotrudniki','schet', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200928_123749_sotrudniki_alter_schet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200928_123749_sotrudniki_alter_schet cannot be reverted.\n";

        return false;
    }
    */
}
