<?php

use yii\db\Migration;

/**
 * Class m200815_153243_add_fields_deti_phone_parents_allergy_is_active
 */
class m200815_153243_add_fields_deti_phone_parents_allergy_is_active extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%deti}}', 'phone_parents', $this->string()->null());
        $this->addColumn('{{%deti}}', 'allergy_is_active', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%deti}}', 'phone_parents');
        $this->dropColumn('{{%deti}}', 'allergy_is_active');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200815_153243_add_fields_deti_phone_parents_allergy_is_active cannot be reverted.\n";

        return false;
    }
    */
}
