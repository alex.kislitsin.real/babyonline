<?php

use yii\db\Migration;

/**
 * Class m201221_185412_add_date
 */
class m201221_185412_add_date extends Migration
{
	/**
	 * {@inheritdoc}
	 * @throws \yii\db\Exception
	 */
    public function safeUp()
    {
	    $array = [
	    	'2020-12-31',
	    	'2021-01-01',
	    	'2021-01-04',
	    	'2021-01-05',
	    	'2021-01-06',
	    	'2021-01-07',
	    	'2021-01-08',
	    	'2021-02-22',
	    	'2021-02-23',
	    	'2021-03-08',
	    	'2021-05-03',
	    	'2021-05-10',
	    	'2021-06-14',
	    	'2021-11-04',
	    	'2021-11-05',
	    	'2021-12-31'
	    ];
	    foreach ($array as $item){
	    	$this->insert('{{%holidays}}',['date_hol' => $item]);
	    }

	    $array = [
		    '2020-12-26',
		    '2021-02-20'
	    ];
	    foreach ($array as $item){
		    $this->insert('{{%works_day}}',['date_work' => $item]);
	    }

	    \Yii::$app->db->createCommand("delete from gogo WHERE datenotgo = '2020-12-31'")->execute();
	    \Yii::$app->db->createCommand("delete from gogoS WHERE dateS = '2020-12-31'")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201221_185412_add_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201221_185412_add_date cannot be reverted.\n";

        return false;
    }
    */
}
