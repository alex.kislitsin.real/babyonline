<?php

use yii\db\Migration;

/**
 * Class m200815_162356_deti_insert_if_alergia_not_empty_allergy_is_active
 */
class m200815_162356_deti_insert_if_alergia_not_empty_allergy_is_active extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \Yii::$app->db->createCommand('update deti set allergy_is_active = 1 where length(rtrim(alergia)) > 0')->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \Yii::$app->db->createCommand('update deti set allergy_is_active = 0')->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200815_162356_deti_insert_if_alergia_not_empty_allergy_is_active cannot be reverted.\n";

        return false;
    }
    */
}
