<?php

namespace app\controllers;

use app\models\Gruppa;
use app\models\Grzdor;
use app\models\Id;
use app\models\Queries;
use app\models\Year;

class GrzdorController extends \yii\web\Controller
{
    public function actionGrzdor()
    {
        $model_id = new Id();
        $model_y = new Year();
        $model_group = new Gruppa();
        $model_grzdor = new Grzdor();
        $qu = new Queries();
        $array_number_grzdor = [1,2,3,4,5];
        $array_number_fisculture = [
            1 => 'dsgdfhghgf',
            2 => '45645745676'
        ];

        $array_gruppa = json_decode(\Yii::$app->request->cookies->getValue('array_group'), true);

        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model_id->load(\Yii::$app->request->post());
            if ($model_id->validate()){
                switch($model_id->id){
                    case 0:
                        if ($model_y->load(\Yii::$app->request->post()) &&
                            ($model_group->load(\Yii::$app->request->post())) &&
                                $model_grzdor->load(\Yii::$app->request->post())){
                            $model_grzdor->year = $model_y->name;
                            if ($model_grzdor->save()){
                                $array = $qu::show_grzdor_index($model_y->name,$model_group->name);
                                $array2 = $qu::show_grzdor_svodka($model_y->name, $model_group->name);
                                return $this->renderAjax('grzdor_all_table',compact(
                                    'array2',
                                    'array',
                                    'model_grzdor',
                                    'model_id',
                                    'array_number_grzdor',
                                    'array_number_fisculture'
                                ));
                            }else{
                                return $model_grzdor->errors;
                            }
                        }else{
                            return 400;
                        }
                        break;
                    case 1:
                        if ($model_y->load(\Yii::$app->request->post()) &&
                            ($model_group->load(\Yii::$app->request->post())) &&
                            $model_grzdor->load(\Yii::$app->request->post())){
                            /** @var $mmm Grzdor */
                            $mmm = Grzdor::find()->where([
                                'child_id' => $model_grzdor->child_id,
                                'year' => $model_y->name,
                            ])->one();
                            $mmm->year = $model_y->name;
                            $mmm->number_grzdor = $model_grzdor->number_grzdor;
                            $mmm->number_fisculture = $model_grzdor->number_fisculture;
                            $mmm->comment = $model_grzdor->comment;

                            if ($mmm->save()){
                                $array = $qu::show_grzdor_index($model_y->name,$model_group->name);
                                $array2 = $qu::show_grzdor_svodka($model_y->name, $model_group->name);
                                return $this->renderAjax('grzdor_all_table',compact(
                                    'array2',
                                    'array',
                                    'model_grzdor',
                                    'model_id',
                                    'array_number_grzdor',
                                    'array_number_fisculture'
                                ));
                            }else{
                                return $mmm->errors;
                            }
                        }else{
                            return 400;
                        }
                        break;
                    case 2:
                        if ($model_y->load(\Yii::$app->request->post()) &&
                            ($model_group->load(\Yii::$app->request->post())) &&
                            $model_grzdor->load(\Yii::$app->request->post())){

                            Grzdor::find()->where([
                                'child_id' => $model_grzdor->child_id,
                                'year' => $model_y->name,
                            ])->one()->delete();

                            $array = $qu::show_grzdor_index($model_y->name,$model_group->name);
                            $array2 = $qu::show_grzdor_svodka($model_y->name, $model_group->name);
                            return $this->renderAjax('grzdor_all_table',compact(
                                'array2',
                                'array',
                                'model_grzdor',
                                'model_id',
                                'array_number_grzdor',
                                'array_number_fisculture'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 3:
                        if ($model_y->load(\Yii::$app->request->post()) && ($model_group->load(\Yii::$app->request->post()))){
                            $array = $qu::show_grzdor_index($model_y->name,$model_group->name);
                            $array2 = $qu::show_grzdor_svodka($model_y->name, $model_group->name);
                            return $this->renderAjax('grzdor_all_table',compact(
                                'array2',
                                'array',
                                'model_grzdor',
                                'model_id',
                                'array_number_grzdor',
                                'array_number_fisculture'
                            ));
                        }else{
                            return 400;
                        }
                        break;

                }
            }
        }

        $model_id->id = 1;
        $id_gruppa = \Yii::$app->request->cookies->getValue('save_last_group');
        if(empty($id_gruppa)){
            $id_gruppa = 1;
        }else{
            $model_group->name = $id_gruppa;
        }
        $year = date('Y');
        $array = $qu::show_grzdor_index($year, $id_gruppa);
        $array2 = $qu::show_grzdor_svodka($year, $id_gruppa);

        $array_year = [];
        for($i=4; $i>0; $i--){
            $array_year[date('Y')-$i] = strval(date('Y')-$i);
        }
        $array_year[date('Y')] = strval(date('Y'));

        $model_y->name = $array_year[date('Y')];

//        \Yii::error( $array );
        return $this->render('index',compact(
            'array_number_grzdor',
            'array_number_fisculture',
            'array_year',
            'model_grzdor',
            'array_gruppa',
            'model_group',
            'model_id',
            'model_y',
            'array',
            'array2'
        ));
    }

}
