<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 30.07.19
 * Time: 16:26
 */

namespace app\controllers\admin;

use yii\web\Controller;

class UserController extends Controller{

    public function actionIndex(){
        return $this->render('index');
    }

} 