<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 30.07.19
 * Time: 15:52
 */
namespace app\controllers;
use app\models\Crujki;
use app\models\Gruppa;
use app\models\Id;
use app\models\Item_date;
use app\models\Month;
use app\models\Reports;
use app\models\Settings;
use app\models\Sotrudniki_new;
use app\models\Year;
use DateTime;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use yii\db\Connection;
use app\models\Queries;

class ReportsController extends Controller{

    //public $layout = 'basic';


    public function actionRep(){
//        show_layout_light($this);
//        vul($this);
//
//        change_db_attr();

//        $model_d_date = new Disable_date();
        $model_group = new Gruppa();
        $model_rep = new Reports();
        $model_y = new Year();
        $model_m = new Month();
        $model_id = new Id();
        $model_id->id = 0;
        $qu = new Queries();

        if (verification_user()!=555){
            $array = [
                1 => 'Табель (дети)',
//                2 => 'Табель (сотрудники)',
//                3 => 'Отчет 5 дней',
            ];
        }else{
            $array = [
                1 => 'Табель (дети)',
                2 => 'Табель (сотрудники)',
//                3 => 'Отчет 5 дней',
            ];
        }



//        $array_year_tabel_deti = json_decode(Yii::$app->request->cookies->getValue('array_years_in_gogo'), true);
//
//        if(empty($array_year_tabel_deti)){
//            $array_year_tabel_deti = [
//                2019 => '2019',
//            ];
//            $key_add_year = date('Y');
//            if (!in_array($key_add_year,$array_year_tabel_deti)){
//                $array_year_tabel_deti[$key_add_year] = $key_add_year;
//            }
//        }else{
//            $key_add_year = date('Y');
//            if (!in_array($key_add_year,$array_year_tabel_deti)){
//                $array_year_tabel_deti[$key_add_year] = $key_add_year;
//            }
//        }
//
//
//        $json_array_dates = json_encode($array_year_tabel_deti);
//        $cookie = new \yii\web\Cookie([
//            'name' => 'array_years_in_gogo',
//            'value' => $json_array_dates,
//        ]);
//        Yii::$app->response->cookies->add($cookie);


        $array_year_tabel_deti = [
            2019 => '2019',
            2020 => '2020',
            2021 => '2021',
            2022 => '2022',
            2023 => '2023',
        ];



        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");

        $model_d_date = Yii::$app->cache->get('array_dates');

        if(empty($model_d_date) || !in_array(date('Y-01-01'),$model_d_date)){
            $model_d_date_query = "select date_hol from holidays";
            $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
            $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');

            $json_array_dates = json_encode($model_d_date);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_dates',
                'value' => $json_array_dates,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }





        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if($model_rep->load(Yii::$app->request->post()) && $model_rep->validate()){

                $cookie = new \yii\web\Cookie([
                    'name' => 'save_last_item_reports',
                    'value' => $model_rep->name,
                ]);
                Yii::$app->response->cookies->add($cookie);

                switch ($model_rep->name){
                    case 1:
                        /*$rep = (Yii::$app->request->post('old_rep'));
                        if ($rep!=1){
                            $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
                            $year = Yii::$app->request->cookies->getValue('save_last_y_deti_rep');
                            $mon = Yii::$app->request->cookies->getValue('save_last_m_deti_rep');
                        }else{
                            $model_group->load(Yii::$app->request->post());
                            $id_gruppa = $model_group->name;

                            $cookie2 = new \yii\web\Cookie([
                                'name' => 'save_last_group',
                                'value' => $id_gruppa,
                            ]);
                            Yii::$app->response->cookies->add($cookie2);

                            $model_y->load(Yii::$app->request->post());
                            $year = $model_y->name;
                            $cookie3 = new \yii\web\Cookie([
                                'name' => 'save_last_y_deti_rep',
                                'value' => $year,
                            ]);
                            Yii::$app->response->cookies->add($cookie3);

                            $model_m->load(Yii::$app->request->post());
                            $mon = $model_m->name;
                            $cookie4 = new \yii\web\Cookie([
                                'name' => 'save_last_m_deti_rep',
                                'value' => $mon,
                            ]);
                            Yii::$app->response->cookies->add($cookie4);
                        }*/


                        $model_group->load(Yii::$app->request->post());
                        $id_gruppa = $model_group->name;

                        if ((Int)$id_gruppa != 0){
                            $cookie2 = new \yii\web\Cookie([
                                'name' => 'save_last_group',
                                'value' => $id_gruppa,
                            ]);
                            Yii::$app->response->cookies->add($cookie2);
                        }elseif((Int)Yii::$app->request->cookies->getValue('save_last_group')==0){
                            $id_gruppa = 1;
                        }else{
                            $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
                        }





                        $model_y->load(Yii::$app->request->post());
                        $year = $model_y->name;
                        $cookie3 = new \yii\web\Cookie([
                            'name' => 'save_last_y_deti_rep',
                            'value' => $year,
                        ]);
                        Yii::$app->response->cookies->add($cookie3);

                        $model_m->load(Yii::$app->request->post());
                        $mon = $model_m->name;
                        $cookie4 = new \yii\web\Cookie([
                            'name' => 'save_last_m_deti_rep',
                            'value' => $mon,
                        ]);
                        Yii::$app->response->cookies->add($cookie4);

                        $id_gruppa == 0 ? $id_gruppa = 1 : null;

                        $model2 = $qu->show_tabel_deti($model_y->name,$model_m->name,$id_gruppa);

                        $number_reports = 1;
                        return $this->renderAjax('diseases\table',compact(
                            'number_reports',
                            'model2',
                            'dataProvider'
                        ));
                        break;
                    case 2:

                        $model_y->load(Yii::$app->request->post());
                        $year = $model_y->name;
                        $cookie3 = new \yii\web\Cookie([
                            'name' => 'save_last_y_so_rep',
                            'value' => $year,
                        ]);
                        Yii::$app->response->cookies->add($cookie3);

                        $model_m->load(Yii::$app->request->post());
                        $mon = $model_m->name;
                        $cookie4 = new \yii\web\Cookie([
                            'name' => 'save_last_m_so_rep',
                            'value' => $mon,
                        ]);
                        Yii::$app->response->cookies->add($cookie4);

                        $model2 = $qu->show_tabel_so($model_y->name,$model_m->name);
                        $number_reports = 2;
                        return $this->renderAjax('diseases\table',compact(
                            'number_reports',
                            'model2',
                            'dataProvider'
                        ));
                        break;
                    case 3:
                        $model_y->load(Yii::$app->request->post());
                        $year = $model_y->name;
                        $cookie3 = new \yii\web\Cookie([
                            'name' => 'save_last_y_deti_rep',
                            'value' => $year,
                        ]);
                        Yii::$app->response->cookies->add($cookie3);

                        $model_m->load(Yii::$app->request->post());
                        $mon = $model_m->name;
                        $cookie4 = new \yii\web\Cookie([
                            'name' => 'save_last_m_deti_rep',
                            'value' => $mon,
                        ]);
                        Yii::$app->response->cookies->add($cookie4);
//                        return $this->renderAjax('day5',compact(
//                            'model_rep',
//                            'model_id',
//                            'model_y',
//                            'model_m',
//                            '_monthsList',
//                            'array_year_tabel_deti',
//                            'model',
//                            'model2'
//                        ));
                        return $this->renderAjax('day5');
                        break;

                }
//                return $model_rep->name;
            }
            exit;
        }

//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);
//        $year = date('Y');
//        $mon = date('m');
//        $model_y->id = $year;
//        $model_m->name = $mon;
//        $model->name = 3;
//        $model_rep->name = json_decode(Yii::$app->request->cookies->getValue('save_last_item_reports'), true);


        $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);

        if (empty($array_gruppa)){
            $query_gruppa = "select * from gruppa order by id";
            $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
            $array_gruppa = ArrayHelper::index($array_gruppa,'id');
            $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');

            $json_array_group = json_encode($array_gruppa);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_group',
                'value' => $json_array_group,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }

        $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
        if(strlen($id_gruppa)<1 || (Int)$id_gruppa == 0){
            $id_gruppa = 1;
            $model_group->name = 1;
        }else{
            $model_group->name = $id_gruppa;
        }





        $model_rep->name = Yii::$app->request->cookies->getValue('save_last_item_reports');
//        $model_rep->name = 2;
        if(strlen($model_rep->name)<1){
            $model_rep->name = 1;
        }
        switch($model_rep->name){
            case 1://дети
                $model_y->name = Yii::$app->request->cookies->getValue('save_last_y_deti_rep');
                $model_m->name = Yii::$app->request->cookies->getValue('save_last_m_deti_rep');

                if (strlen($model_y->name)<1)$model_y->name = date('Y');
                if (strlen($model_m->name)<1)$model_m->name = date('n');

                $model2 = $qu->show_tabel_deti($model_y->name,$model_m->name,$id_gruppa);
//                $model2 = array();//////////////////////////////////////////////////////////////////////
                $number_reports = 1;

                return $this->render('rep',compact(
                    'number_reports',
                    'array_gruppa',
                    'array_year',
                    'model_rep',
                    'model_group',
                    'model_id',
                    'model_y',
                    'model_m',
                    '_monthsList',
                    'array_year_tabel_deti',
                    'model',
                    'model2',
                    'array',
                    'dataProvider'
                ));
                break;
            case 2://сотрудники
                $model_y->name = Yii::$app->request->cookies->getValue('save_last_y_so_rep');
                $model_m->name = Yii::$app->request->cookies->getValue('save_last_m_so_rep');

                if (empty($model_y->name))$model_y->name = date('Y');
                if (empty($model_m->name))$model_m->name = date('n');

                $model2 = $qu->show_tabel_so($model_y->name,$model_m->name);
                $number_reports = 2;

                return $this->render('rep',compact(
                    'model_group',
                    'array_gruppa',
                    'array_year_tabel_deti',
                    'number_reports',
                    'array_year',
                    'model_rep',
                    'model_id',
                    'model_y',
                    'model_m',
                    '_monthsList',
                    'model',
                    'model2',
                    'array',
                    'dataProvider'
                ));
                break;
            case 3://5 дневный отчет
                return $this->render('day5',compact(
                    'array_gruppa',
//                'item_gruppa',
                    'model_rep',
                    'model_group',
                    'model_id',
                    'model_y',
                    'model_m',
                    '_monthsList',
                    'array_year_tabel_deti',
                    'model',
                    'model2',
                    'array',
                    'dataProvider'
                ));
                break;

        }
        /*if ($model_rep->name == 3){
            return $this->render('day5',compact(
                'array_gruppa',
//                'item_gruppa',
                'model_rep',
                'model_group',
                'model_id',
                'model_y',
                'model_m',
                '_monthsList',
                'array_year_tabel_deti',
                'model',
                'model2',
                'array',
                'dataProvider'
            ));
        }else{
            return $this->render('rep',compact(
                'array_gruppa',
                'array_year',
                'model_rep',
                'model_group',
                'model_id',
                'model_y',
                'model_m',
                '_monthsList',
                'array_year_tabel_deti',
                'model',
                'model2',
                'array',
                'dataProvider'
            ));
        }*/
        exit;

    }










    public function actionDvij(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();
        $model_item_date = new Item_date();
        $model_d_date = Yii::$app->cache->get('array_dates');
        $model_d_antidate = Yii::$app->cache->get('array_antidates');
        $qu = new Queries();

        $nametable = 'deti';

        $array_disabled_dates = array();
        foreach($model_d_date as $date){
            $date = date('d.m.Y',  strtotime($date));
            array_push($array_disabled_dates,$date);
        }

        $percent = '9%';

        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model_item_date->load(Yii::$app->request->post());
            $data = Yii::$app->formatter->asTime($model_item_date->item_date);


            if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
                $dv = '_weekends_or_holidays';
            }else{
                $transaction = Yii::$app->db->beginTransaction();
                try{
                    $model_dvig = $qu->show_dvijenie($data);
                    $array = $qu->show_dvijenie_alergiki($data);
                    $transaction->commit();
                }catch (Exception $e){
                    $transaction->rollBack();
                }
                $dv = 'dvigenie_table';

                /*$dataProvider = new \yii\data\SqlDataProvider([
                    'sql' => 'SET NOCOUNT ON; EXEC Dvizh2222 @data=:data, @nametable=:nametable',
                    'params' => [':data' => $data,':nametable'  => $nametable],
                    'pagination' => false,
                    'sort' => false,
                ]);
                $model_dvig = $dataProvider->getModels();
                $model_dvig = ArrayHelper::index($model_dvig,'idid');*/


            }

            /*if (verification_user()==555){
                $dv = 'dvigenie_table';

                $dataProvider = new \yii\data\SqlDataProvider([
                    'sql' => 'SET NOCOUNT ON; EXEC Dvizh2222 @data=:data, @nametable=:nametable',
                    'params' => [':data' => $data,':nametable'  => $nametable],
                    'pagination' => false,
                    'sort' => false,
                ]);
                $model_dvig = $dataProvider->getModels();
                $model_dvig = ArrayHelper::index($model_dvig,'idid');

                $query = "set nocount on; exec Show_alergiki_ander_dvij @date=:date";
                $array = Yii::$app->db->createCommand($query,['date' => $data])->queryAll();
            }*/


            return $this->renderAjax($dv,compact(
                'array',
                'dataProvider',
                'model_dvig',
                'percent'));

            return null;
        }

        exit;
//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);
        /*$model_item_date->item_date = date("d.m.Y");
        $data = date("Y-m-d");

        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SET NOCOUNT ON; EXEC Dvizh2222 @data=:data, @nametable=:nametable',
            'params' => [':data' => $data,':nametable'  => $nametable],
            'pagination' => false,
            'sort' => false,
        ]);

        $model_dvig = $dataProvider->getModels();
        $model_dvig = ArrayHelper::index($model_dvig,'idid');



        if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
            $dv = '_weekends_or_holidays';
        }else{
            $dv = 'dvij_view';
        }

//        if (verification_user()==555)$dv = 'dvij_view';

        return $this->render($dv,compact(
            'model_dvig',
            'dataProvider',
            'model_d_date',
            'item_date',
            'percent',
            'model_item_date',
            'array_disabled_dates'
        ));*/
    }





    public function actionOrz(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();
        $qu = new Queries();

        $model_item_date = new Item_date();
        $model_d_date = Yii::$app->cache->get('array_dates');
        $model_d_antidate = Yii::$app->cache->get('array_antidates');

        $array_disabled_dates = array();
        foreach($model_d_date as $date){
            $date = date('d.m.Y',  strtotime($date));
            array_push($array_disabled_dates,$date);
        }

        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_item_date->load(Yii::$app->request->post());

            $data = Yii::$app->formatter->asTime($model_item_date->item_date);

//            $query = "SET NOCOUNT ON; EXEC Bosss @data=:data";
//            $array = Yii::$app->db->createCommand($query,['data' => $data])->queryAll();



            /*if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
                $dv = '_weekends_or_holidays';
            }else{
                $dv = 'dvigenie_table';
            }

            if (verification_user()==555)$dv = 'dvigenie_table';*/

            if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
                $dv = '_weekends_or_holidays';
            }else{
                $array = $qu->show_hot_button_orz($data);
                $dv = 'dvigenie_orz';
            }

            return $this->renderAjax($dv,compact('array','data'));
        }
        exit;
    }






    public function actionAlergia(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $query = "select deti.name as `name`,gruppa.name as `namegr`,deti.alergia from deti
            left outer join gruppa on deti.id_gruppa = gruppa.id where length(rtrim(alergia)) > 0 and allergy_is_active = 1 and `out` is null order by deti.name";
            $model = Yii::$app->db->createCommand($query)->queryAll();

            return $this->renderAjax('alergia_table',compact('model'));
        }
        exit;
    }

    public function actionSettings(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        $model = new Settings();
        $model_id = new Id();

        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model_id->load(Yii::$app->request->post()) && $model_id->validate()){
                switch($model_id->id){
                    case 0:
                    default:
//                        $day = Yii::$app->request->cookies->getValue('day_tabel');
                        $day = 0;
                        if ((Int)$day == 0){
                            $query = "select * from settings";
                            $array = Yii::$app->db->createCommand($query)->queryAll();
                            foreach($array as $a){
                                $a['id']==1 ? $model->day_tabel = $a['value0'] : null;
                                $a['id']==2 ? $model->city = $a['value0'] : null;
                                $a['id']==3 ? $model->boss = $a['value0'] : null;
                                $a['id']==4 ? $model->medsestra = $a['value0'] : null;
                                $a['id']==5 ? $model->lico_cru = $a['value0'] : null;
                                $a['id']==6 ? $model->lico_cru2 = $a['value0'] : null;
                                $a['id']==7 ? $model->boss_rod = $a['value0'] : null;
                                $a['id']==8 ? $model->flagActive = $a['value0'] : null;
                            }

                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'day_tabel','value' => $model->day_tabel,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'cityr','value' => $model->city,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'boss','value' => $model->boss,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'boss_rod','value' => $model->boss_rod,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'medsestra','value' => $model->medsestra,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'lico_cru','value' => $model->lico_cru,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'lico_cru2','value' => $model->lico_cru2,'expire' => time() + 3600 * 24 * 365]));
                            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'flagActive','value' => $model->flagActive,'expire' => time() + 3600 * 24 * 365]));

                        }else{
                            $model->day_tabel = Yii::$app->request->cookies->getValue('day_tabel');
                            $model->city = Yii::$app->request->cookies->getValue('cityr');
                            $model->boss = Yii::$app->request->cookies->getValue('boss');
                            $model->boss_rod = Yii::$app->request->cookies->getValue('boss_rod');
                            $model->medsestra = Yii::$app->request->cookies->getValue('medsestra');
                            $model->lico_cru = Yii::$app->request->cookies->getValue('lico_cru');
                            $model->lico_cru2 = Yii::$app->request->cookies->getValue('lico_cru2');
                            $model->flagActive = Yii::$app->request->cookies->getValue('flagActive');
                        }

                        return $this->renderAjax('settings_table',compact('model'));
                        break;
                    case 1:
                        if ($model->load(Yii::$app->request->post()) && $model->validate()){

                            //return $model->flagActive;

                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=1",['value0' => $model->day_tabel])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=2",['value0' => $model->city])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=3",['value0' => $model->boss])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=4",['value0' => $model->medsestra])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=5",['value0' => $model->lico_cru])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=6",['value0' => $model->lico_cru2])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=7",['value0' => $model->boss_rod])->execute();
                                Yii::$app->db->createCommand("update settings set value0=:value0 where id=8",['value0' => $model->flagActive])->execute();

                                $query = "select * from settings";
                                $array = Yii::$app->db->createCommand($query)->queryAll();

                                foreach($array as $a){
                                    $a['id']==1 ? $model->day_tabel = $a['value0'] : null;
                                    $a['id']==2 ? $model->city = $a['value0'] : null;
                                    $a['id']==3 ? $model->boss = $a['value0'] : null;
                                    $a['id']==4 ? $model->medsestra = $a['value0'] : null;
                                    $a['id']==5 ? $model->lico_cru = $a['value0'] : null;
                                    $a['id']==6 ? $model->lico_cru2 = $a['value0'] : null;
                                    $a['id']==7 ? $model->boss_rod = $a['value0'] : null;
                                    $a['id']==8 ? $model->flagActive = $a['value0'] : null;
                                }

                                $transaction->commit();
                            } catch (Exception $e) {
                                $transaction->rollback();
//                                print $e;
                                exit;
                            }
                        }

                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'day_tabel','value' => $model->day_tabel,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'cityr','value' => $model->city,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'boss','value' => $model->boss,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'boss_rod','value' => $model->boss_rod,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'medsestra','value' => $model->medsestra,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'lico_cru','value' => $model->lico_cru,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'lico_cru2','value' => $model->lico_cru2,'expire' => time() + 3600 * 24 * 365]));
                        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'flagActive','value' => $model->flagActive,'expire' => time() + 3600 * 24 * 365]));

                        return $this->renderAjax('settings_table',compact('model'));
                        break;
                }
            }

        }
        exit;
    }


    public function actionRbac(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        $model_id = new Id();

        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model_id->load(Yii::$app->request->post()) && $model_id->validate()){
                $column = Yii::$app->request->post('check');
                $column = preg_replace('/[^0-9]/','',$column);
                $id_so = Yii::$app->request->post('id_so');
                $id_so = preg_replace('/[^0-9]/','',$id_so);

                switch($column){
                    case 1://pitanie_deti
                        $column = 'pitanie_deti';
                        break;
                    case 2://pitanie_so
                        $column = 'pitanie_so';
                        break;
                    case 3://delo_deti
                        $column = 'delo_deti';
                        break;
                    case 4://delo_so
                        $column = 'delo_so';
                        break;
                    case 5://antro
                        $column = 'antro';
                        break;
                    case 6://tabel
                        $column = 'tabel';
                        break;
                    case 7://journal
                        $column = 'journal';
                        break;
                    case 8://search
                        $column = 'search';
                        break;
                    case 9://five
                        $column = 'five';
                        break;
                    case 10://correct
                        $column = 'correct';
                        break;
                    case 11://history
                        $column = 'history';
                        break;
                    case 12://injection
                        $column = 'injection';
                        break;
                    case 13://crujki
                        $column = 'crujki';
                        break;
                    case 14://zp
                        $column = 'zp';
                        break;

                }

                switch($model_id->id){
                    case 0:
                        $array = Yii::$app->db->createCommand("select sotrudniki.id[id],sotrudniki.name,sotrudniki.dol,rbac.* from rbac full outer join sotrudniki on rbac.id_so = sotrudniki.id where outS is null order by name")->queryAll();
                        return $this->renderAjax('rbac_table',compact('array'));
                        break;
                    case 1://delete
                        Yii::$app->db->createCommand("update rbac set $column = 0 where id_so=:id_so",['id_so' => $id_so])->execute();
                        $array = Yii::$app->db->createCommand("select sotrudniki.id[id],sotrudniki.name,sotrudniki.dol,rbac.* from rbac full outer join sotrudniki on rbac.id_so = sotrudniki.id where outS is null order by name")->queryAll();
                        return $this->renderAjax('rbac_table',compact('array'));
                        break;
                    case 2://insert
                        $transaction = Yii::$app->db->beginTransaction();
                        try {
                            $count = Yii::$app->db->createCommand("select isnull(count(*),0) from rbac where id_so=:id_so",['id_so' => $id_so])->queryScalar();
                            if ((Int)$count > 0){
                                $query = "update rbac set $column = 1 where id_so=:id_so";
                                Yii::$app->db->createCommand($query,['id_so' => $id_so])->execute();
                            }elseif((Int)$count == 0){
                                $query = "insert into rbac (id_so,$column)values(:id_so,1)";
                                Yii::$app->db->createCommand($query,['id_so' => $id_so])->execute();
                            }

                            $array = Yii::$app->db->createCommand("select sotrudniki.id[id],sotrudniki.name,sotrudniki.dol,rbac.* from rbac full outer join sotrudniki on rbac.id_so = sotrudniki.id where outS is null order by name")->queryAll();

                            $transaction->commit();
                        } catch (Exception $e) {
                            $transaction->rollback();
                            exit;
                        }

                        return $this->renderAjax('rbac_table',compact('array'));
                        break;
                    case 3:

                        break;

                }
            }
            exit;
        }
        exit;
    }

























    public function actionExcel($id,$year,$month,$id_group){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

//        $model_group = new Gruppa();
//        $model_rep = new Reports();
//        $model_y = new Year();
//        $model_m = new Month();
        $model_id = new Id();
        $model_id->id = 0;
        $qu = new Queries();

        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");

        $MonthNamesRus=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

        $id = preg_replace('/[^0-9]/','',$id);
        $year = preg_replace('/[^0-9]/','',$year);
        $month = preg_replace('/[^0-9]/','',$month);
        $id_group = preg_replace('/[^0-9]/','',$id_group);

        switch($id){
            case 1:

                $model = $qu->show_tabel_deti($year,$month,$id_group);
                $ped = Yii::$app->db->createCommand("select rtrim(`name`) as `name` from sotrudniki where id_gruppa = :group and dol like '%Воспитатель%' and outS is null",['group' => $id_group])->queryOne();
                $buh = 1;
                return $this->render('excel/excel_deti_buh_test3.php',compact(
                    'ped',
                    'buh',
                    'year',
                    'month',
                    'id_group',
                    '_monthsList',
                    'MonthNamesRus',
                    'model'
                ));
                break;
            case 2:
                $model = $qu->show_tabel_deti($year,$month,$id_group);
                $ped = Yii::$app->db->createCommand("select rtrim(`name`) as `name` from sotrudniki where id_gruppa = :group and dol like '%Воспитатель%' and outS is null",['group' => $id_group])->queryOne();
                $buh = 0;
                return $this->render('excel/excel_deti_buh_test3.php',compact(
                    'ped',
                    'buh',
                    'year',
                    'month',
                    'id_group',
                    '_monthsList',
                    'MonthNamesRus',
                    'model'
                ));
                break;
            case 3:
                $model = $qu->show_tabel_so($year,$month);
                return $this->render('excel/excel_deti_buh_so.php',compact(
                    'year',
                    'month',
                    '_monthsList',
                    'MonthNamesRus',
                    'model'
                ));
                break;
            case 4://spravka deti word
                $array = $qu->show_spravka($year,$month);
                return $this->render('excel/word_cru_spravka_deti.php',compact(
                    'month',
                    'array',
                    'year',
                    'month'
                ));
                break;
            case 5:
                $buh = 1;
                return $this->render('excel/excel_deti_buh_all_group.php',compact(
                    'buh',
                    'year',
                    'month',
                    '_monthsList',
                    'MonthNamesRus'
                ));
                break;
            case 6:
                $buh = 0;
                return $this->render('excel/excel_deti_buh_all_group.php',compact(
                    'buh',
                    'year',
                    'month',
                    '_monthsList',
                    'MonthNamesRus'
                ));
                break;


        }
        exit;
    }

    public function actionExcel2($id,$year,$month,$id_crujok){
//        change_db_attr();
        $id = preg_replace('/[^0-9]/','',$id);
        $year = preg_replace('/[^0-9]/','',$year);
        $month = preg_replace('/[^0-9]/','',$month);
        $id_crujok = preg_replace('/[^0-9]/','',$id_crujok);

        $qu = new Queries();
        $crujki_model = new Crujki();

        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");

        $MonthNamesRus=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

        switch($id){
            case 1:


                $name_ped = $crujki_model->getNamePed($id_crujok);

                $model_cru = new Crujki();

                $transaction = Yii::$app->db->beginTransaction();
                try{
                    $array_cru = $model_cru->show_crujki_edit_modal();
                    $model = $qu->show_tabel_cru($year,$month,$id_crujok);
                    $transaction->commit();
                }catch (Exception $e){
                    $transaction->rollBack();
                }

                return $this->render('excel/excel_cru_tabel.php',compact(
                    'name_ped',
                    'id_crujok',
                    'array_cru',
                    'year',
                    'month',
                    '_monthsList',
                    'MonthNamesRus',
                    'model'
                ));
                break;
            case 2:
                $_monthsList_rod = array(
                    1=>"январе",2=>"феврале",3=>"марте",
                    4=>"апреле",5=>"мае", 6=>"июне",
                    7=>"июле",8=>"августе",9=>"сентябре",
                    10=>"октябре",11=>"ноябре",12=>"декабре");

                $array = $qu->show_cru_akt($year,$month);
                return $this->render('excel/word_cru_akt.php',compact(
                    'array',
                    '_monthsList_rod',
                    'month',
                    'year',
                    '_monthsList'
//                    'MonthNamesRus',
//                    'model'
                ));

                break;
            case 3:
                $array = $qu->show_cru_akt($year,$month);
                return $this->render('excel/word_cru_spravka.php',compact(
                    'array',
                    '_monthsList_rod',
                    'month',
                    'year',
                    '_monthsList'
//                    'MonthNamesRus',
//                    'model'
                ));
                break;
            case 4:
                /*$array = Yii::$app->db->createCommand('set nocount on; exec Cru_dogovora @year=:year,@mon=:month',[
                    'year' => $year,
                    'month' => $month
                ])->queryAll();*/

                $array = $qu->show_cru_dogovora($year,$month);

                return $this->render('excel/word_cru_dogovora.php',compact(
                    'array',
                    'month',
                    'year',
                    '_monthsList'
                ));
                break;
            case 5:
                $array = $qu->show_cru_dogovora($year,$month);
                return $this->render('excel/word_cru_dogovora_akti.php',compact(
                    'array',
                    'month',
                    'year',
                    '_monthsList'
                ));
                break;
            case 6:
                $array = $qu->show_cru_dogovora($year,$month);
                return $this->render('excel/excel_cru_reestr.php',compact(
                    'array',
                    'month',
                    'year',
                    '_monthsList'
                ));
                break;
            case 7:

                return $this->render('excel/excel_cru_tabel_all.php',compact(
                    'id_crujok',
                    'year',
                    'month',
                    '_monthsList',
                    'MonthNamesRus'
                ));
                break;




        }

        exit;
    }


    public function actionSave_all_doc($id,$year = 2020,$month = 1,$id_gruppa = 1,$vozrast = 0,$month1 = 0,$month2 = 0){
//        change_db_attr();
        $qu = new Queries();
        $id = preg_replace('/[^0-9]/','',$id);
        $year = preg_replace('/[^0-9]/','',$year);
        $month = preg_replace('/[^0-9]/','',$month);
        $id_gruppa = preg_replace('/[^0-9]/','',$id_gruppa);
        $vozrast = preg_replace('/[^0-9]/','',$vozrast);

        $month1 = preg_replace('/[^0-9]/','',$month1);
        $month2 = preg_replace('/[^0-9]/','',$month2);

        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");
        $MonthNamesRus=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

        switch($id){
            case 1:
                $group = Yii::$app->db->createCommand("SELECT gruppa.*,0 AS `count` from gruppa")->queryAll();

                $array = Yii::$app->db->createCommand("SELECT gogo.id_child,gogo.datenotgo,deti.id_gruppa, deti.`name` FROM gogo LEFT OUTER JOIN deti ON gogo.id_child = deti.id WHERE (gogo.id = 4 OR sovsem = 4) AND MONTH(datenotgo) = :month AND YEAR(datenotgo) = :year ORDER BY deti.id_gruppa,deti.`name`,gogo.datenotgo",[
                    'month' => $month,
                    'year' => $year,
                ])->queryAll();

                $array_friday = [];
                $count = 0;
                $date1 = 0;
                foreach ($array as $key =>  $item){
                    if ($count == 0){
                        $count = $item['id_child'];
                        $date1 = date('N',strtotime($item['datenotgo']));
                        $array_friday[] = [
                            'id_child' => $item['id_child'],
                            'datenotgo' => $item['datenotgo'],
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }elseif($count == $item['id_child']){
                        if ($date1 == 5 && date('N',strtotime($item['datenotgo'])) == 1){
                            $subbota = date('Y-m-d',strtotime($item['datenotgo'].'- 2 day'));
                            $voskr = date('Y-m-d',strtotime($item['datenotgo'].'- 1 day'));
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $subbota,
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $voskr,
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $item['datenotgo'],
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];

                        }else{
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $item['datenotgo'],
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];
                        }
                        $date1 = date('N',strtotime($item['datenotgo']));
                    }else{
                        $count = $item['id_child'];
                        $date1 = date('N',strtotime($item['datenotgo']));
                        $array_friday[] = [
                            'id_child' => $item['id_child'],
                            'datenotgo' => $item['datenotgo'],
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }

                }


                $id_child = 0;
                $ddd = 0;
                $count = 0;
                $array2 = [];
                foreach ($array_friday as $key =>  $item){
                    if ($key == 0){
                        $id_child = $item['id_child'];
                        $count = $key;
                        $array2[$key] = [
                            'id_child' => $item['id_child'],
                            'date_start' => $item['datenotgo'],
                            'date_end' => '',
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }elseif($id_child == $item['id_child'] && date('Y-m-d',strtotime($item['datenotgo'])) == $ddd){
                        $array2[$count]['date_end'] = $item['datenotgo'];
                    }else{
                        $id_child = $item['id_child'];
                        $count = $key;
                        $array2[$count] = [
                            'id_child' => $item['id_child'],
                            'date_start' => $item['datenotgo'],
                            'date_end' => '',
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }
                    $ddd = date('Y-m-d',strtotime(($item['datenotgo']).'+1 day'));
                }

                foreach ($array2 as $key => $item){
                    if (empty($item['date_end'])){
                        continue;
                    }else{
                        $date_end = new DateTime($item['date_end']);
                        $date_start = new DateTime($item['date_start']);
                        $interval = date_diff($date_end, $date_start);
                        $array2[$key]['date_diff'] = ($interval->d)+1;
                    }
                }

                foreach($group as $key => $item){
                    foreach ($array2 as $value){
	                    if (!isset($value['date_diff']) || $value['date_diff'] < 3){
		                    continue;
	                    }
                        if ($value['id_gruppa'] == $item['id'] && isset($value['date_diff'])){
                            $group[$key]['count'] += $value['date_diff'];
                        }
                    }
                }


                /*$array = Yii::$app->db->createCommand('set nocount on; exec Journal_orz @year=:year,@month=:month',[
                    'year' => $year,
                    'month' => $month
                ])->queryAll();*/
                return $this->render('excel/excel_save_all_doc_orz.php',compact(
                    'array2',
                    'month',
                    'year',
                    '_monthsList',
                    'group'
                ));
                break;
            case 2:
	            $group = Yii::$app->db->createCommand("SELECT gruppa.*,0 AS `count` from gruppa")->queryAll();

	            $array = Yii::$app->db->createCommand("SELECT b.*,reason.`name` AS reason FROM (SELECT (SELECT case when a.sovsem > 0 then a.sovsem ELSE a.id end) AS id,a.id_child,a.datenotgo,a.id_gruppa,a.`name` FROM (SELECT gogo.id,gogo.sovsem,gogo.id_child,gogo.datenotgo,deti.id_gruppa, deti.`name` FROM gogo LEFT OUTER JOIN deti ON gogo.id_child = deti.id  WHERE (gogo.id in (select id from reason where id = 2 or id_parent in (2,21,22,23,24,25,26,27)) OR sovsem in (select id from reason where id = 2 or id_parent in (2,21,22,23,24,25,26,27))) AND MONTH(datenotgo) = :month AND YEAR(datenotgo) = :year ORDER BY deti.id_gruppa,deti.`name`,gogo.datenotgo)a)b LEFT OUTER JOIN reason ON b.id = reason.id",[
		            'month' => $month,
		            'year' => $year,
	            ])->queryAll();

	            $array_friday = [];
	            $count = 0;
	            $date1 = 0;
	            foreach ($array as $key =>  $item){
		            if ($count == 0){
			            $count = $item['id_child'];
			            $date1 = date('N',strtotime($item['datenotgo']));
			            $array_friday[] = [
				            'id_child' => $item['id_child'],
				            'datenotgo' => $item['datenotgo'],
				            'id_gruppa' => $item['id_gruppa'],
				            'name' => $item['name'],
				            'reason' => $item['reason']
			            ];
		            }elseif($count == $item['id_child']){
			            if ($date1 == 5 && date('N',strtotime($item['datenotgo'])) == 1){
				            $subbota = date('Y-m-d',strtotime($item['datenotgo'].'- 2 day'));
				            $voskr = date('Y-m-d',strtotime($item['datenotgo'].'- 1 day'));
				            $array_friday[] = [
					            'id_child' => $item['id_child'],
					            'datenotgo' => $subbota,
					            'id_gruppa' => $item['id_gruppa'],
					            'name' => $item['name'],
					            'reason' => $item['reason']
				            ];
				            $array_friday[] = [
					            'id_child' => $item['id_child'],
					            'datenotgo' => $voskr,
					            'id_gruppa' => $item['id_gruppa'],
					            'name' => $item['name'],
					            'reason' => $item['reason']
				            ];
				            $array_friday[] = [
					            'id_child' => $item['id_child'],
					            'datenotgo' => $item['datenotgo'],
					            'id_gruppa' => $item['id_gruppa'],
					            'name' => $item['name'],
					            'reason' => $item['reason']
				            ];

			            }else{
				            $array_friday[] = [
					            'id_child' => $item['id_child'],
					            'datenotgo' => $item['datenotgo'],
					            'id_gruppa' => $item['id_gruppa'],
					            'name' => $item['name'],
					            'reason' => $item['reason']
				            ];
			            }
			            $date1 = date('N',strtotime($item['datenotgo']));
		            }else{
			            $count = $item['id_child'];
			            $date1 = date('N',strtotime($item['datenotgo']));
			            $array_friday[] = [
				            'id_child' => $item['id_child'],
				            'datenotgo' => $item['datenotgo'],
				            'id_gruppa' => $item['id_gruppa'],
				            'name' => $item['name'],
				            'reason' => $item['reason']
			            ];
		            }

	            }


	            $id_child = 0;
	            $ddd = 0;
	            $count = 0;
	            $array2 = [];
	            foreach ($array_friday as $key =>  $item){
		            if ($key == 0){
			            $id_child = $item['id_child'];
			            $count = $key;
			            $array2[$key] = [
				            'id_child' => $item['id_child'],
				            'date_start' => $item['datenotgo'],
				            'date_end' => '',
				            'id_gruppa' => $item['id_gruppa'],
				            'name' => $item['name'],
				            'reason' => $item['reason']
			            ];
		            }elseif($id_child == $item['id_child'] && date('Y-m-d',strtotime($item['datenotgo'])) == $ddd){
			            $array2[$count]['date_end'] = $item['datenotgo'];
		            }else{
			            $id_child = $item['id_child'];
			            $count = $key;
			            $array2[$count] = [
				            'id_child' => $item['id_child'],
				            'date_start' => $item['datenotgo'],
				            'date_end' => '',
				            'id_gruppa' => $item['id_gruppa'],
				            'name' => $item['name'],
				            'reason' => $item['reason']
			            ];
		            }
		            $ddd = date('Y-m-d',strtotime(($item['datenotgo']).'+1 day'));
	            }

	            foreach ($array2 as $key => $item){
		            if (empty($item['date_end'])){
			            continue;
		            }else{
			            $date_end = new DateTime($item['date_end']);
			            $date_start = new DateTime($item['date_start']);
			            $interval = date_diff($date_end, $date_start);
			            $array2[$key]['date_diff'] = ($interval->d)+1;
		            }
	            }

	            foreach($group as $key => $item){
		            foreach ($array2 as $value){
			            if (!isset($value['date_diff']) || $value['date_diff'] < 3){
				            continue;
			            }
			            if ($value['id_gruppa'] == $item['id'] && isset($value['date_diff'])){
				            $group[$key]['count'] += $value['date_diff'];
			            }
		            }
	            }

	            $nameJournal = 'Журнал учета соматических заболеваний';

	            return $this->render('excel/excel_save_all_doc_somatic.php',compact(
		            'array2',
		            'month',
		            'year',
		            '_monthsList',
		            'nameJournal',
		            'group'
	            ));
                break;
            case 3:

                if($id_gruppa == 0){
                    if ($vozrast == 0){
                        $query = "select id_gruppa, rtrim(deti.name) as `name`, rtrim(deti.otche) as `otche`, date_format(rozd, '%d.%m.%Y') as `rozd`, adress, polis, snils, '' as coment from deti left outer join gruppa on deti.id_gruppa = gruppa.id where deti.out is null order by deti.id_gruppa,deti.name";
                        $array = Yii::$app->db->createCommand($query)->queryAll();
                    }else{
                        $query = "select id_gruppa, rtrim(deti.name) as `name`, rtrim(deti.otche) as `otche`, date_format(rozd, '%d.%m.%Y') as `rozd`, adress, polis, snils, '' as coment from deti left outer join gruppa on deti.id_gruppa = gruppa.id where deti.out is null and (year(now())-year(rozd)) = :vozrast order by deti.id_gruppa,deti.name";
                        $array = Yii::$app->db->createCommand($query,['vozrast' => $vozrast])->queryAll();
                    }


                }else{
                    if ($vozrast == 0){
                        $query = "select id_gruppa, rtrim(deti.name) as `name`, rtrim(deti.otche) as `otche`, date_format(rozd, '%d.%m.%Y') as `rozd`, adress, polis, snils, '' as coment from deti left outer join gruppa on deti.id_gruppa = gruppa.id where deti.out is null and gruppa.id = :id_gruppa order by deti.name";
                        $array = Yii::$app->db->createCommand($query,['id_gruppa' => $id_gruppa])->queryAll();
                    }else{
                        $query = "select id_gruppa, rtrim(deti.name) as `name`, rtrim(deti.otche) as `otche`, date_format(rozd, '%d.%m.%Y') as `rozd`, adress, polis, snils, '' as coment from deti left outer join gruppa on deti.id_gruppa = gruppa.id where deti.out is null and (year(now())-year(rozd)) = :vozrast and gruppa.id = :id_gruppa order by deti.name";
                        $array = Yii::$app->db->createCommand($query,['id_gruppa' => $id_gruppa,'vozrast' => $vozrast])->queryAll();
                    }

                }

                /*if (intval(co$group = Yii::$app->db->createCommand("SELECT gruppa.*,0 AS `count` from gruppa")->queryAll();

                $array = Yii::$app->db->createCommand("SELECT gogo.id_child,gogo.datenotgo,deti.id_gruppa, deti.`name` FROM gogo LEFT OUTER JOIN deti ON gogo.id_child = deti.id WHERE (gogo.id = 4 OR sovsem = 4) AND MONTH(datenotgo) = :month AND YEAR(datenotgo) = :year ORDER BY deti.id_gruppa,deti.`name`,gogo.datenotgo",[
                    'month' => $month,
                    'year' => $year,
                ])->queryAll();

                $array_friday = [];
                $count = 0;
                $date1 = 0;
                foreach ($array as $key =>  $item){
                    if ($count == 0){
                        $count = $item['id_child'];
                        $date1 = date('N',strtotime($item['datenotgo']));
                        $array_friday[] = [
                            'id_child' => $item['id_child'],
                            'datenotgo' => $item['datenotgo'],
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }elseif($count == $item['id_child']){
                        if ($date1 == 5 && date('N',strtotime($item['datenotgo'])) == 1){
                            $subbota = date('Y-m-d',strtotime($item['datenotgo'].'- 2 day'));
                            $voskr = date('Y-m-d',strtotime($item['datenotgo'].'- 1 day'));
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $subbota,
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $voskr,
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $item['datenotgo'],
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];

                        }else{
                            $array_friday[] = [
                                'id_child' => $item['id_child'],
                                'datenotgo' => $item['datenotgo'],
                                'id_gruppa' => $item['id_gruppa'],
                                'name' => $item['name']
                            ];
                        }
                        $date1 = date('N',strtotime($item['datenotgo']));
                    }else{
                        $count = $item['id_child'];
                        $date1 = date('N',strtotime($item['datenotgo']));
                        $array_friday[] = [
                            'id_child' => $item['id_child'],
                            'datenotgo' => $item['datenotgo'],
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }

                }


                $id_child = 0;
                $ddd = 0;
                $count = 0;
                $array2 = [];
                foreach ($array_friday as $key =>  $item){
                    if ($key == 0){
                        $id_child = $item['id_child'];
                        $count = $key;
                        $array2[$key] = [
                            'id_child' => $item['id_child'],
                            'date_start' => $item['datenotgo'],
                            'date_end' => '',
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }elseif($id_child == $item['id_child'] && date('Y-m-d',strtotime($item['datenotgo'])) == $ddd){
                        $array2[$count]['date_end'] = $item['datenotgo'];
                    }else{
                        $id_child = $item['id_child'];
                        $count = $key;
                        $array2[$count] = [
                            'id_child' => $item['id_child'],
                            'date_start' => $item['datenotgo'],
                            'date_end' => '',
                            'id_gruppa' => $item['id_gruppa'],
                            'name' => $item['name']
                        ];
                    }
                    $ddd = date('Y-m-d',strtotime(($item['datenotgo']).'+1 day'));
                }

                foreach ($array2 as $key => $item){
                    if (empty($item['date_end'])){
                        continue;
                    }else{
                        $date_end = new DateTime($item['date_end']);
                        $date_start = new DateTime($item['date_start']);
                        $interval = date_diff($date_end, $date_start);
                        $array2[$key]['date_diff'] = ($interval->d)+1;
                    }
                }

                foreach($group as $key => $item){
                    foreach ($array2 as $value){
                        if ($value['id_gruppa'] == $item['id'] && isset($value['date_diff'])){
                            $group[$key]['count'] += $value['date_diff'];
                        }
                    }
                }unt($array))==0){
                    return $this->render('excel/script.php');
//                    return $this->renderPartial('alert_message', array(), true);
//                    Yii::app()->clientScript->registerScript('sc1','$().ready(function(){alert("444444444444");});');
//                    return $this->render('excel/excel_save_all_doc_spiski.php');
//                    exit;
                }*/

                return $this->render('excel/excel_save_all_doc_spiski.php',compact(
                    'vozrast',
                    'id_gruppa',
                    'array',
//                    'month',
//                    'year',
                    '_monthsList'
                ));
                break;
            case 4://
                $array = $qu->show_zabolevaemost($year,$month);
//                Yii::error($array);return;
                return $this->render('excel/excel_zabol_month_period.php',compact('array','year','month'));
                break;
            case 5:
                $id_gruppa == 0 ? $id_gruppa = 1 : $id_gruppa = $id_gruppa;
                $array = $qu->show_mantu($id_gruppa);
                return $this->render('excel/excel_save_all_doc_journal_mantu.php',compact('array','id_gruppa'));
                break;
            case 6:
                $id_gruppa == 0 ? $id_gruppa = 1 : $id_gruppa = $id_gruppa;
                $array = $qu->show_sp_antro_excel($year,$month,$id_gruppa);
                if ($month >= 1 && $month < 7){//spring
                    $period = 1;
                }elseif($month >= 7 && $month < 12){//ourtemn
                    $period = 2;
                }
                return $this->render('excel/excel_save_all_doc_journal_antro.php',compact('array','id_gruppa','period','year'));
                break;
            case 7:
                $array = $qu->show_sp_antro_excel_all_years($id_gruppa);
                return $this->render('excel/excel_save_all_doc_journal_antro_all_years_all_group.php',compact('array','id_gruppa'));
                break;
            case 8:
                return $this->render('excel/word_blank1.php',compact('id_gruppa'));
                break;
            case 9:
                $query = "SELECT * FROM (SELECT
a.rozd2,
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'М' AND YEAR(rozd) = a.rozd2) AS `m`,
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'Ж' AND YEAR(rozd) = a.rozd2) AS `g`,
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND YEAR(rozd) = a.rozd2) AS `summa`
FROM (SELECT YEAR(rozd) AS rozd2 FROM deti WHERE `out` IS null GROUP BY rozd2) a) b
UNION ALL
SELECT 'итого',
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'М'),
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'Ж'),
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL)";

                $array = \Yii::$app->db->createCommand($query)->queryAll();
                return $this->render('excel/excel_save_all_doc_year_born_m_g.php',compact('array'));
                break;


            case 10:
                $query = "SELECT * FROM (SELECT
a.id, a.`name`,
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'М' AND id_gruppa = a.id) AS `m`,
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'Ж' AND id_gruppa = a.id) AS `g`,
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND id_gruppa = a.id) AS `summa`
FROM (SELECT id, `name` FROM gruppa) a) b
UNION ALL
SELECT '','итого',
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'М'),
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL AND pol = 'Ж'),
(SELECT COUNT(*) FROM deti WHERE `out` IS NULL)";
                $array = \Yii::$app->db->createCommand($query)->queryAll();
                return $this->render('excel/excel_save_all_doc_gruppa_m_g.php',compact('array'));
                break;

            case 11:
                $array = Sotrudniki_new::find()->where(['outs' => null])->orderBy(['name' => SORT_ASC])->all();
                return $this->render('excel/excel_save_all_doc_spisok_sotrudniki.php',compact('array'));
                break;
	        case 12:
		        $group = Yii::$app->db->createCommand("SELECT gruppa.*,0 AS `count` from gruppa")->queryAll();

		        $array = Yii::$app->db->createCommand("SELECT b.*,reason.`name` AS reason FROM (SELECT (SELECT case when a.sovsem > 0 then a.sovsem ELSE a.id end) AS id,a.id_child,a.datenotgo,a.id_gruppa,a.`name` FROM (SELECT gogo.id,gogo.sovsem,gogo.id_child,gogo.datenotgo,deti.id_gruppa, deti.`name` FROM gogo LEFT OUTER JOIN deti ON gogo.id_child = deti.id  WHERE (gogo.id in (select id from reason where id_parent = 1 and id != 4) OR sovsem in (select id from reason where id_parent = 1 and id != 4)) AND MONTH(datenotgo) = :month AND YEAR(datenotgo) = :year ORDER BY deti.id_gruppa,deti.`name`,gogo.datenotgo)a)b LEFT OUTER JOIN reason ON b.id = reason.id",[
			        'month' => $month,
			        'year' => $year,
		        ])->queryAll();

		        $array_friday = [];
		        $count = 0;
		        $date1 = 0;
		        foreach ($array as $key =>  $item){
			        if ($count == 0){
				        $count = $item['id_child'];
				        $date1 = date('N',strtotime($item['datenotgo']));
				        $array_friday[] = [
					        'id_child' => $item['id_child'],
					        'datenotgo' => $item['datenotgo'],
					        'id_gruppa' => $item['id_gruppa'],
					        'name' => $item['name'],
					        'reason' => $item['reason']
				        ];
			        }elseif($count == $item['id_child']){
				        if ($date1 == 5 && date('N',strtotime($item['datenotgo'])) == 1){
					        $subbota = date('Y-m-d',strtotime($item['datenotgo'].'- 2 day'));
					        $voskr = date('Y-m-d',strtotime($item['datenotgo'].'- 1 day'));
					        $array_friday[] = [
						        'id_child' => $item['id_child'],
						        'datenotgo' => $subbota,
						        'id_gruppa' => $item['id_gruppa'],
						        'name' => $item['name'],
						        'reason' => $item['reason']
					        ];
					        $array_friday[] = [
						        'id_child' => $item['id_child'],
						        'datenotgo' => $voskr,
						        'id_gruppa' => $item['id_gruppa'],
						        'name' => $item['name'],
						        'reason' => $item['reason']
					        ];
					        $array_friday[] = [
						        'id_child' => $item['id_child'],
						        'datenotgo' => $item['datenotgo'],
						        'id_gruppa' => $item['id_gruppa'],
						        'name' => $item['name'],
						        'reason' => $item['reason']
					        ];

				        }else{
					        $array_friday[] = [
						        'id_child' => $item['id_child'],
						        'datenotgo' => $item['datenotgo'],
						        'id_gruppa' => $item['id_gruppa'],
						        'name' => $item['name'],
						        'reason' => $item['reason']
					        ];
				        }
				        $date1 = date('N',strtotime($item['datenotgo']));
			        }else{
				        $count = $item['id_child'];
				        $date1 = date('N',strtotime($item['datenotgo']));
				        $array_friday[] = [
					        'id_child' => $item['id_child'],
					        'datenotgo' => $item['datenotgo'],
					        'id_gruppa' => $item['id_gruppa'],
					        'name' => $item['name'],
					        'reason' => $item['reason']
				        ];
			        }

		        }


		        $id_child = 0;
		        $ddd = 0;
		        $count = 0;
		        $array2 = [];
		        foreach ($array_friday as $key =>  $item){
			        if ($key == 0){
				        $id_child = $item['id_child'];
				        $count = $key;
				        $array2[$key] = [
					        'id_child' => $item['id_child'],
					        'date_start' => $item['datenotgo'],
					        'date_end' => '',
					        'id_gruppa' => $item['id_gruppa'],
					        'name' => $item['name'],
					        'reason' => $item['reason']
				        ];
			        }elseif($id_child == $item['id_child'] && date('Y-m-d',strtotime($item['datenotgo'])) == $ddd){
				        $array2[$count]['date_end'] = $item['datenotgo'];
			        }else{
				        $id_child = $item['id_child'];
				        $count = $key;
				        $array2[$count] = [
					        'id_child' => $item['id_child'],
					        'date_start' => $item['datenotgo'],
					        'date_end' => '',
					        'id_gruppa' => $item['id_gruppa'],
					        'name' => $item['name'],
					        'reason' => $item['reason']
				        ];
			        }
			        $ddd = date('Y-m-d',strtotime(($item['datenotgo']).'+1 day'));
		        }

		        foreach ($array2 as $key => $item){
			        if (empty($item['date_end'])){
				        continue;
			        }else{
				        $date_end = new DateTime($item['date_end']);
				        $date_start = new DateTime($item['date_start']);
				        $interval = date_diff($date_end, $date_start);
				        $array2[$key]['date_diff'] = ($interval->d)+1;
			        }
		        }

		        foreach($group as $key => $item){
			        foreach ($array2 as $value){
				        if (!isset($value['date_diff']) || $value['date_diff'] < 3){
					        continue;
				        }
				        if ($value['id_gruppa'] == $item['id'] && isset($value['date_diff'])){
					        $group[$key]['count'] += $value['date_diff'];
				        }
			        }
		        }

		        $nameJournal = 'Журнал учета инфекционных заболеваний';

		        return $this->render('excel/excel_save_all_doc_somatic.php',compact(
			        'array2',
			        'month',
			        'year',
			        '_monthsList',
			        'nameJournal',
			        'group'
		        ));
		        break;
        }
        exit;
    }

}