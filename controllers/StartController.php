<?php

namespace app\controllers;

use app\components\NumericCaptcha;
use app\models\Antro;
use app\models\Gruppa;
use app\models\Id;
use app\models\Month;
use app\models\Presentation;
use app\models\Year;
use Yii;
use yii\web\Controller;

class StartController extends Controller
{
    public $layout = 'main_start';

    public function actionTest(){
        return $this->render('test');
    }

    public function actionSystem(){

        $model = new Presentation();
        $model_id = new Id();



        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model_id->load(Yii::$app->request->post()) && $model_id->validate()){
                switch($model_id->id){
                    case 0:
//                        insert_data('send_form');
                        return $this->renderAjax('system_form',compact('model','model_id'));
                        break;
                    case 1:
                        if ($model->load(Yii::$app->request->post()) && $model->validate()){

                            Yii::$app->mailer->compose()
                                ->setFrom(['pochtovi2007@yandex.ru' => 'Заявка на подключение'])
                                ->setTo('babyonline3000@gmail.com')
                                ->setSubject('Новая заявка')
                                ->setTextBody('Новая заявка')
                                ->setHtmlBody('
                                <br/>Регион: '.$model->region.'</p>
                                <br/>Город: '.$model->city.'</p>
                                <br/>Имя: '.$model->name.'</p>
                                <br/>Название организации: '.$model->name_org.'</p>
                                <br/>Почта: '.$model->email.'</p>
                                <br/>Тлф: '.$model->tel.'</p>
                                <br/>IP address: '.Yii::$app->request->userIP.'</p>
                                ')
                                ->send();

                            Yii::$app->db->close();
                            Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=u0601128_baby";
                            Yii::$app->db->username='u0601128_baby';

                            $query = "insert into connect_table (region,city,name,name_org,email,tel) values (:region,:city,:name,:name_org,:email,:tel);";
                            Yii::$app->db->createCommand($query,[
                                    'region' => $model->region,
                                    'city' => $model->city,
                                    'name' => $model->name,
                                    'name_org' => $model->name_org,
                                    'email' => $model->email,
                                    'tel' => $model->tel,
                                ]
                            )->execute();

                            $name = $model->name;
//                            insert_data('send_form_ok');
                            return $this->renderAjax('system_form_after_send_message_ok',compact('name'));
                        }else{
//                            insert_data('send_form_error');
                            return $this->renderAjax('system_form_after_send_message_error',compact('name'));
                        }
                        break;
                    case 2:
//                        insert_data('presentation');
                        return $this->renderAjax('system5',compact('model','model_id'));
                        break;
                    case 3:
//                        insert_data('contakts');
                        return $this->renderAjax('system_contakts');
                        break;
                    case 4:
//                        insert_data('send_form');
                        return $this->renderAjax('system_form_demo',compact('model','model_id'));
                        break;
                    case 5:
                        if ($model->load(Yii::$app->request->post()) && $model->validate()){

                            Yii::$app->mailer->compose()
                                ->setFrom(['pochtovi2007@yandex.ru' => 'Заявка на демо доступ'])
                                ->setTo([
                                    'babyonline3000@gmail.com',
                                    'pochtovi2007@yandex.ru',
                                    'martin-kirov@yandex.ru'
                                ])
                                ->setSubject('Новая заявка демо')
                                ->setTextBody('Новая заявка демо')
                                ->setHtmlBody('
                                <br/>Регион: '.$model->region.'</p>
                                <br/>Город: '.$model->city.'</p>
                                <br/>Имя: '.$model->name.'</p>
                                <br/>Название организации: '.$model->name_org.'</p>
                                <br/>Почта: '.$model->email.'</p>
                                <br/>Тлф: '.$model->tel.'</p>
                                <br/>IP address: '.Yii::$app->request->userIP.'</p>
                                ')
                                ->send();

                            Yii::$app->db->close();
                            Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=u0601128_baby";
                            Yii::$app->db->username='u0601128_baby';

                            $query = "insert into connect_table_demo (region,city,name,name_org,email,tel) values (:region,:city,:name,:name_org,:email,:tel);";
                            Yii::$app->db->createCommand($query,[
                                    'region' => $model->region,
                                    'city' => $model->city,
                                    'name' => $model->name,
                                    'name_org' => $model->name_org,
                                    'email' => $model->email,
                                    'tel' => $model->tel,
                                ]
                            )->execute();

                            $name = $model->name;
                            return $this->renderAjax('system_form_after_send_message_ok',compact('name'));
                        }else{
                            return $this->renderAjax('system_form_after_send_message_error',compact('name'));
                        }
                        break;



                }
            }
            exit;
        }

        /*if (Yii::$app->request->userIP != '127.0.0.1'){
            $c = counter_peoples();
            if($c != 0){
                Yii::$app->mailer->compose()
                    ->setFrom(['pochtovi2007@yandex.ru' => 'Вошел новый клиент'])
                    ->setTo([
                        'babyonline3000@gmail.com',
                        'pochtovi2007@yandex.ru'
                    ])
                    ->setSubject('новый клиент')
                    ->setTextBody('новый клиент')
                    ->setHtmlBody('Вошел <b>'.$c.'</b> клиент')
                    ->send();
            }
        }*/

        insert_data('presentation');

        return $this->render('system2',compact('model','model_id'));
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => NumericCaptcha::className(),
                'transparent' => true,
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

}
