<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\db\Connection;
use yii\helpers\ArrayHelper;

class SendfiveController extends Controller
{
    public function actionRun(){



        $MonthNamesRus=array(
            1 => "января",
            2 => "февраля",
            3 => "марта",
            4 => "апреля",
            5 => "мая",
            6 => "июня",
            7 => "июля",
            8 => "августа",
            9 => "сентября",
            10 => "октября",
            11 => "ноября",
            12 => "декабря"
        );

        $transaction = Yii::$app->db->beginTransaction();
        try{
            $on_off = Yii::$app->db->createCommand('select value0 from settings where id=8')->queryScalar();
            if ($on_off==0){
                $transaction->rollBack();
                exit;
            }


            $today = date('Y-m-d');

            $array_date_to = Yii::$app->db->createCommand('SELECT date_format(date_to, "%Y-%m-%d") as `date_to` FROM send_five;')->queryAll();
            $array_date_to = ArrayHelper::getColumn($array_date_to,'date_to');

            if (in_array($today,$array_date_to)){
                $transaction->rollBack();
                exit;
            }

            $model_d_date_query = "select * from holidays,works_day";
            $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
            $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');
            $model_d_date_anti = ArrayHelper::getColumn($model_d_date_result,'date_work');
            $model_d_antidate = array_unique($model_d_date_anti);
            if (empty($model_d_date))$model_d_date=array();
            if (empty($model_d_antidate))$model_d_antidate=array();

            $first = date('Y-m-'.'01');
            $last_day = date('Y-m-'.date('t'));
            $iter_date = $first;
            $array_date = [];
            $i = 0;
            $workdayiter = 0;
            $count_works_day_before_today = 0;
            while($iter_date <= $last_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $workdayiter++;
                    if ($workdayiter==5){
                        $array_date[] = $iter_date;
                        $i++;
                        $workdayiter = 0;
                    }

                    if ($iter_date <= $today){
                        $count_works_day_before_today++;
                    }
                }
                $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
            }

//            if (true){
            if (in_array($today,$array_date)){
                $query = 'select (select count(*) as `count` from deti where ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`)) and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))*:c-(select COUNT(*) from gogo left outer join deti on gogo.id_child=deti.id where datenotgo between :date1 AND :date2 and gogo.id not in (100,200) and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`)) and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL)) as `count`;';
                $value = Yii::$app->db->createCommand($query,[
                    'c' => $count_works_day_before_today,
                    'mon' => date('n'),
                    'yea' => date('Y'),
                    'date1' => date('Y-m-'.'01'),
                    'date2' => date('Y-m-d'),
                ])->queryScalar();

                Yii::$app->db->createCommand('insert into send_five (val) values (:val);',['val' => $value])->execute();
            }else{
                $transaction->rollBack();
                exit;
            }

            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }



        $results = date('j').' '.$MonthNamesRus[date('n')].' '.date('Y').' г.:  '.$value;

        Yii::$app->mailer->compose()
            ->setFrom(['pochtovi2007@yandex.ru' => 'МКДОУ №35'])
//            ->setFrom(['dou35@kirovedu.ru' => 'МКДОУ №35'])
            ->setTo('brevnopilov@mail.ru')
//            ->setTo('cb2uo2-prod@kirovedu.ru')//нужный ящик
//            ->setBcc('dou35@kirovedu.ru')//копия себе
            ->setBcc('pochtovi2007@yandex.ru')//копия себе
            ->setSubject('Детодни')
//            ->setTextBody('Новая заявка')
            ->setHtmlBody('<p>Информация о фактической посещаемости детей c 1 по '.$results.'</p><br/>
            <p>С уважением МКДОУ №35 г. Кирова</p>')
            ->send();

//        $results = 1000;
//        $results = print_r($array_date_to, true);


        Console::output($results);
    }
}
//строка подключения крон опен сервер
//%sitedir%\inet\yii sendfive/run