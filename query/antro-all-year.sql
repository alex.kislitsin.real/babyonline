
SELECT d.*
FROM (
SELECT a.id,rtrim(a.`name`) AS `name`,a.rozd,a.id_gruppa,



if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','06-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','06-01'))),
'') AS rost_v,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','06-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','06-01'))),
'') AS ves_v,

if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','12-01'))),
'') AS rost_o,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW()),'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW()),'-','12-01'))),
'') AS ves_o,




if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','06-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','06-01'))),
'') AS rost_v1,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','06-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','06-01'))),
'') AS ves_v1,

if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','12-01'))),
'') AS rost_o1,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-1,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-1,'-','12-01'))),
'') AS ves_o1,






if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','06-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','06-01'))),
'') AS rost_v2,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','06-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','06-01'))),
'') AS ves_v2,

if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','12-01'))),
'') AS rost_o2,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-2,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-2,'-','12-01'))),
'') AS ves_o2,





if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','06-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','06-01'))),
'') AS rost_v3,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','06-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','06-01'))),
'') AS ves_v3,

if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','12-01'))),
'') AS rost_o3,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-3,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-3,'-','12-01'))),
'') AS ves_o3,





if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','06-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','06-01'))),
'') AS rost_v4,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','06-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','01-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','06-01'))),
'') AS ves_v4,

if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','12-01'))),
'') AS rost_o4,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(YEAR(NOW())-4,'-','07-01') AND LAST_DAY(CONCAT(YEAR(NOW())-4,'-','12-01'))),
'') AS ves_o4






FROM (SELECT id,`name`,id_gruppa,date_format(rozd,'%d.%m.%Y') AS rozd from deti WHERE `out` is NULL AND id_gruppa = :id_group)a ORDER BY a.`name`)d