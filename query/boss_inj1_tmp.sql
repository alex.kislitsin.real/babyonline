


DROP TABLE if EXISTS boss_inj1_tmp;
CREATE TEMPORARY TABLE boss_inj1_tmp
(select deti.id as id_child,deti.id_gruppa,deti.name,i1.otkaz,i1.v,i1.r,i1.date_inj,i1.id_inj,i1.medotvod,
(select TIMESTAMPDIFF(month,rozd,now()) from deti where id = i1.id_child) as vozrast_month,
(select ROUND((datediff(now(),rozd))/365) from deti where id = i1.id_child) as vozrast_year
FROM (
select id_child,max(date_inj)as date_inj,id_inj from injection group by id_child,id_inj) A
left outer join injection i1 ON
(A.id_child=i1.id_child and A.date_inj=i1.date_inj and A.id_inj=i1.id_inj) right outer join deti on i1.id_child = deti.id where deti.out is NULL
order by A.id_child,A.id_inj);


CALL Show_plan_injection ();





DROP TABLE if EXISTS boss_inj2_tmp;
CREATE TEMPORARY TABLE boss_inj2_tmp

select *,(select 
		case

		
		when id_inj in (2,3,8) and (vozrast_month>=6 and v in (1,2)) and now() >= (select DATE_ADD(date_inj, INTERVAL 45 day)) then now()
		when id_inj in (2,3,8) and (vozrast_month>=6 and v in (1,2)) and now() < (select DATE_ADD(date_inj, INTERVAL 45 day)) then (select DATE_ADD(date_inj, INTERVAL 45 day))
		when id_inj in (2,3,8) and (vozrast_month>=18 and v = 3) and now() >= (select DATE_ADD(date_inj, INTERVAL 12 month)) then now()
		when id_inj in (2,3,8) and (vozrast_month>=18 and v = 3) and now() < (select DATE_ADD(date_inj, INTERVAL 12 month)) then (select DATE_ADD(date_inj, INTERVAL 12 month))

		
		when id_inj = 3 and (vozrast_year>=6 and r = 1) and now() >= (select DATE_ADD(date_inj, INTERVAL 5 year)) then now()
		when id_inj = 3 and (vozrast_year>=6 and r = 1) and now() < (select DATE_ADD(date_inj, INTERVAL 5 year)) then (select DATE_ADD(date_inj, INTERVAL 5 year))

		
		when id_inj = 2 and (vozrast_month>=18 and r = 1) and now() >= (select DATE_ADD(date_inj, INTERVAL 2 month)) then now()
		when id_inj = 2 and (vozrast_month>=18 and r = 1) and now() < (select DATE_ADD(date_inj, INTERVAL 2 month)) then (select DATE_ADD(date_inj, INTERVAL 2 month))

		
		when id_inj in (5,6,7) and (vozrast_year>=6 and v = 1) and now() >= (select DATE_ADD(date_inj, INTERVAL 6 month)) then now()
		when id_inj in (5,6,7) and (vozrast_year>=6 and v = 1) and now() < (select DATE_ADD(date_inj, INTERVAL 6 month)) then (select DATE_ADD(date_inj, INTERVAL 6 month))

		
		when id_inj = 9 and (vozrast_month>=5 and v = 1) and now() >= (select DATE_ADD(date_inj, INTERVAL 75 day)) then now()
		when id_inj = 9 and (vozrast_month>=5 and v = 1) and now() < (select DATE_ADD(date_inj, INTERVAL 75 day)) then (select DATE_ADD(date_inj, INTERVAL 75 day))
		when id_inj = 9 and (vozrast_month>=15 and v = 2) and now() >= (select DATE_ADD(date_inj, INTERVAL 315 day)) then now()
		when id_inj = 9 and (vozrast_month>=15 and v = 2) and now() < (select DATE_ADD(date_inj, INTERVAL 315 day)) then (select DATE_ADD(date_inj, INTERVAL 315 day))

		
		when id_inj = 1 and (vozrast_month>=6 and v = 1) and (now() >= (select DATE_ADD(date_inj, INTERVAL 6 year)) and 
		(select el2 from mantu where mantu.id_child = boss_inj1_tmp.id_child and year(dat) = (year(now())))=0 and
		(select el2 from mantu where mantu.id_child = boss_inj1_tmp.id_child and year(dat) = (year(now())-1))=0 and
		(select el2 from mantu where mantu.id_child = boss_inj1_tmp.id_child and year(dat) = (year(now())-2))=0) and 
		((select DATE_ADD((select max(dat) from mantu where mantu.id_child = boss_inj1_tmp.id_child),INTERVAL 14 day)) > now()) then now()
		when id_inj = 1 and (vozrast_month>=6 and v = 1) and (now() < (select DATE_ADD(date_inj, INTERVAL 6 year)) and 
		(select el2 from mantu where mantu.id_child = boss_inj1_tmp.id_child and year(dat) = (year(now())))=0 and
		(select el2 from mantu where mantu.id_child = boss_inj1_tmp.id_child and year(dat) = (year(now())-1))=0 and
		(select el2 from mantu where mantu.id_child = boss_inj1_tmp.id_child and year(dat) = (year(now())-2))=0) and 
		((select DATE_ADD((select max(dat) from mantu where mantu.id_child = boss_inj1_tmp.id_child),INTERVAL 14 day)) > now()) then (select DATE_ADD(date_inj, INTERVAL 6 year))

		when (vozrast_month>=5 and v = 0 and r = 0) then now()

		else null end
		
		) as next_date,
	
		'' AS coment,
		(select 
		case
		when v = 0 and r = 0 then 'V1'
		when id_inj in (1,2,3,4) and v = 1 and r = 0 then 'V2'
		when id_inj in (1,2,3,4) and v = 2 and r = 0 then 'V3'		

		when id_inj in (5,6,7) and v = 1 and r = 0 then 'R1'
		when id_inj in (1,2,3,4) and v = 3 and r = 0 then 'R1'
		when id_inj in (1,2,3,4) and r = 1 and v = 0 then 'R2'
		when id_inj in (1,2,3,4) and r = 2 and v = 0 then 'R3'
		else '' end
		) as RV,
		'' AS medotvod_date
		from boss_inj1_tmp;
		
		


DROP TABLE if EXISTS boss_inj3_tmp;
CREATE TABLE boss_inj3_tmp
SELECT * from boss_inj2_tmp left outer join code_injection on boss_inj2_tmp.id_inj = code_injection.id_code 
where year(next_date) <= year(now()) and month(next_date) <= month(now()) and id_inj not in (7,8,9) order by id_gruppa,`name`;


alter table boss_inj2_tmp modify column medotvod_date char(15);

alter table boss_inj3_tmp modify column medotvod_date CHAR(30);
alter table boss_inj3_tmp modify column coment varCHAR(900);





CALL Show_plan_injection2 ();
CALL Show_plan_injection3 ();

select boss_inj3_tmp.id_child,boss_inj3_tmp.id_gruppa,rtrim(boss_inj3_tmp.`name`) AS `name`,otkaz,v,r,date_inj,id_inj,medotvod,vozrast_month,vozrast_year,date_format(next_date,'%Y-%m-%d') AS `next_date`,boss_inj3_tmp.coment,RV,medotvod_date,rtrim(name_code) AS `name_code`,deti.rozd from boss_inj3_tmp left outer join deti on boss_inj3_tmp.id_child = deti.id order by boss_inj3_tmp.id_gruppa,boss_inj3_tmp.`name`;


		
