
SELECT u.*,
(if(`1`=0,1,0)+if(`2`=0,1,0)+if(`3`=0,1,0)+if(`4`=0,1,0)+if(`5`=0,1,0)+if(`6`=0,1,0)+if(`7`=0,1,0)
+if(`8`=0,1,0)+if(`9`=0,1,0)+if(`10`=0,1,0)+if(`11`=0,1,0)+if(`12`=0,1,0)+if(`13`=0,1,0)+if(`14`=0,1,0)
+if(`15`=0,1,0)+if(`16`=0,1,0)+if(`17`=0,1,0)+if(`18`=0,1,0)+if(`19`=0,1,0)+if(`20`=0,1,0)+if(`21`=0,1,0)
+if(`22`=0,1,0)+if(`23`=0,1,0)+if(`24`=0,1,0)+if(`25`=0,1,0)+if(`26`=0,1,0)+if(`27`=0,1,0)+if(`28`=0,1,0)
+if(`29`=0,1,0)+if(`30`=0,1,0)+if(`31`=0,1,0)) AS `пришли`,
(if(`1`=1,1,0)+if(`2`=1,1,0)+if(`3`=1,1,0)+if(`4`=1,1,0)+if(`5`=1,1,0)+if(`6`=1,1,0)+if(`7`=1,1,0)
+if(`8`=1,1,0)+if(`9`=1,1,0)+if(`10`=1,1,0)+if(`11`=1,1,0)+if(`12`=1,1,0)+if(`13`=1,1,0)+if(`14`=1,1,0)
+if(`15`=1,1,0)+if(`16`=1,1,0)+if(`17`=1,1,0)+if(`18`=1,1,0)+if(`19`=1,1,0)+if(`20`=1,1,0)+if(`21`=1,1,0)
+if(`22`=1,1,0)+if(`23`=1,1,0)+if(`24`=1,1,0)+if(`25`=1,1,0)+if(`26`=1,1,0)+if(`27`=1,1,0)+if(`28`=1,1,0)
+if(`29`=1,1,0)+if(`30`=1,1,0)+if(`31`=1,1,0)) AS `пропуски`,

(if(`1`=0 OR `1`=1,1,0)+if(`2`=0 OR `2`=1,1,0)+if(`3`=0 OR `3`=1,1,0)+if(`4`=0 OR `4`=1,1,0)+if(`5`=0 OR `5`=1,1,0)+if(`6`=0 OR `6`=1,1,0)+if(`7`=0 OR `7`=1,1,0)
+if(`8`=0 OR `8`=1,1,0)+if(`9`=0 OR `9`=1,1,0)+if(`10`=0 OR `10`=1,1,0)+if(`11`=0 OR `11`=1,1,0)+if(`12`=0 OR `12`=1,1,0)+if(`13`=0 OR `13`=1,1,0)+if(`14`=0 OR `14`=1,1,0)
+if(`15`=0 OR `15`=1,1,0)+if(`16`=0 OR `16`=1,1,0)+if(`17`=0 OR `17`=1,1,0)+if(`18`=0 OR `18`=1,1,0)+if(`19`=0 OR `19`=1,1,0)+if(`20`=0 OR `20`=1,1,0)+if(`21`=0 OR `21`=1,1,0)
+if(`22`=0 OR `22`=1,1,0)+if(`23`=0 OR `23`=1,1,0)+if(`24`=0 OR `24`=1,1,0)+if(`25`=0 OR `25`=1,1,0)+if(`26`=0 OR `26`=1,1,0)+if(`27`=0 OR `27`=1,1,0)+if(`28`=0 OR `28`=1,1,0)
+if(`29`=0 OR `29`=1,1,0)+if(`30`=0 OR `30`=1,1,0)+if(`31`=0 OR `31`=1,1,0))*(select price from crujki where id = :id_crujok) AS `stavka`
 FROM (
SELECT a.*,rtrim(deti.`name`) AS `name`,deti.number_schet as `number`,
case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-01')) then 1/*пропуск*/

when
(weekday(CONCAT(:yea,'-',:mon,'-01')) > 4 or CONCAT(:yea,'-',:mon,'-01') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-01')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-01')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-01')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-01')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-01')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-01') not in (select date_work from works_day)

then 5 ELSE 0 END AS `1`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-02')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-02')) > 4 or CONCAT(:yea,'-',:mon,'-02') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-02')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-02')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-02')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-02')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-02')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-02') not in (select date_work from works_day)
then 5 ELSE 0 END AS `2`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-3')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-03')) > 4 or CONCAT(:yea,'-',:mon,'-03') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-03')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-03')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-03')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-03')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-03')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-03') not in (select date_work from works_day)
then 5 ELSE 0 END AS `3`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-4')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-04')) > 4 or CONCAT(:yea,'-',:mon,'-04') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-04')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-04')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-04')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-04')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-04')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-04') not in (select date_work from works_day)
then 5 ELSE 0 END AS `4`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-5')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-05')) > 4 or CONCAT(:yea,'-',:mon,'-05') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-05')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-05')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-05')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-05')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-05')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-05') not in (select date_work from works_day)
then 5 ELSE 0 END AS `5`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-6')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-06')) > 4 or CONCAT(:yea,'-',:mon,'-06') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-06')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-06')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-06')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-06')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-06')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-06') not in (select date_work from works_day)
then 5 ELSE 0 END AS `6`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-7')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-07')) > 4 or CONCAT(:yea,'-',:mon,'-07') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-07')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-07')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-07')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-07')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-07')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-07') not in (select date_work from works_day)
then 5 ELSE 0 END AS `7`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-8')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-08')) > 4 or CONCAT(:yea,'-',:mon,'-08') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-08')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-08')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-08')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-08')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-08')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-08') not in (select date_work from works_day)
then 5 ELSE 0 END AS `8`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-9')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-09')) > 4 or CONCAT(:yea,'-',:mon,'-09') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-09')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-09')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-09')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-09')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-09')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-09') not in (select date_work from works_day)
then 5 ELSE 0 END AS `9`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-10')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-10')) > 4 or CONCAT(:yea,'-',:mon,'-10') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-10')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-10')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-10')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-10')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-10')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-10') not in (select date_work from works_day)
then 5 ELSE 0 END AS `10`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-11')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-11')) > 4 or CONCAT(:yea,'-',:mon,'-11') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-11')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-11')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-11')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-11')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-11')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-11') not in (select date_work from works_day)
then 5 ELSE 0 END AS `11`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-12')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-12')) > 4 or CONCAT(:yea,'-',:mon,'-12') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-12')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-12')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-12')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-12')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-12')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-12') not in (select date_work from works_day)
then 5 ELSE 0 END AS `12`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-13')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-13')) > 4 or CONCAT(:yea,'-',:mon,'-13') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-13')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-13')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-13')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-13')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-13')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-13') not in (select date_work from works_day)
then 5 ELSE 0 END AS `13`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-14')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-14')) > 4 or CONCAT(:yea,'-',:mon,'-14') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-14')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-14')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-14')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-14')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-14')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-14') not in (select date_work from works_day)
then 5 ELSE 0 END AS `14`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-15')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-15')) > 4 or CONCAT(:yea,'-',:mon,'-15') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-15')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-15')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-15')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-15')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-15')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-15') not in (select date_work from works_day)
then 5 ELSE 0 END AS `15`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-16')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-16')) > 4 or CONCAT(:yea,'-',:mon,'-16') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-16')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-16')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-16')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-16')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-16')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-16') not in (select date_work from works_day)
then 5 ELSE 0 END AS `16`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-17')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-17')) > 4 or CONCAT(:yea,'-',:mon,'-17') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-17')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-17')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-17')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-17')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-17')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-17') not in (select date_work from works_day)
then 5 ELSE 0 END AS `17`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-18')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-18')) > 4 or CONCAT(:yea,'-',:mon,'-18') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-18')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-18')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-18')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-18')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-18')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-18') not in (select date_work from works_day)
then 5 ELSE 0 END AS `18`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-19')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-19')) > 4 or CONCAT(:yea,'-',:mon,'-19') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-19')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-19')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-19')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-19')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-19')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-19') not in (select date_work from works_day)
then 5 ELSE 0 END AS `19`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-20')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-20')) > 4 or CONCAT(:yea,'-',:mon,'-20') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-20')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-20')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-20')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-20')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-20')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-20') not in (select date_work from works_day)
then 5 ELSE 0 END AS `20`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-21')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-21')) > 4 or CONCAT(:yea,'-',:mon,'-21') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-21')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-21')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-21')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-21')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-21')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-21') not in (select date_work from works_day)
then 5 ELSE 0 END AS `21`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-22')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-22')) > 4 or CONCAT(:yea,'-',:mon,'-22') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-22')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-22')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-22')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-22')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-22')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-22') not in (select date_work from works_day)
then 5 ELSE 0 END AS `22`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-23')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-23')) > 4 or CONCAT(:yea,'-',:mon,'-23') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-23')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-23')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-23')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-23')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-23')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-23') not in (select date_work from works_day)
then 5 ELSE 0 END AS `23`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-24')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-24')) > 4 or CONCAT(:yea,'-',:mon,'-24') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-24')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-24')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-24')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-24')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-24')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-24') not in (select date_work from works_day)
then 5 ELSE 0 END AS `24`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-25')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-25')) > 4 or CONCAT(:yea,'-',:mon,'-25') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-25')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-25')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-25')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-25')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-25')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-25') not in (select date_work from works_day)
then 5 ELSE 0 END AS `25`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-26')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-26')) > 4 or CONCAT(:yea,'-',:mon,'-26') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-26')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-26')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-26')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-26')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-26')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-26') not in (select date_work from works_day)
then 5 ELSE 0 END AS `26`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-27')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-27')) > 4 or CONCAT(:yea,'-',:mon,'-27') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-27')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-27')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-27')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-27')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-27')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-27') not in (select date_work from works_day)
then 5 ELSE 0 END AS `27`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-28')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-28')) > 4 or CONCAT(:yea,'-',:mon,'-28') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-28')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-28')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-28')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-28')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-28')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-28') not in (select date_work from works_day)
then 5 ELSE 0 END AS `28`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-29')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-29')) > 4 or CONCAT(:yea,'-',:mon,'-29') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-29')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-29')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-29')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-29')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-29')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-29') not in (select date_work from works_day)
then 5 ELSE 0 END AS `29`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-30')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-30')) > 4 or CONCAT(:yea,'-',:mon,'-30') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-30')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-30')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-30')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-30')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-30')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-30') not in (select date_work from works_day)
then 5 ELSE 0 END AS `30`,

case 
when a.id_child IN (select crujki_gogo.id_child from crujki_gogo where id_crujok = :id_crujok and datenotgo = CONCAT(:yea,'-',:mon,'-31')) then 1/*пропуск*/
when
(weekday(CONCAT(:yea,'-',:mon,'-31')) > 4 or CONCAT(:yea,'-',:mon,'-31') in (select date_hol from holidays) or
	(weekday(CONCAT(:yea,'-',:mon,'-31')) = 0 and (select pn from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-31')) = 1 and (select vt from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-31')) = 2 and (select sr from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-31')) = 3 and (select ch from crujki where id = :id_crujok) = 0) or
	(weekday(CONCAT(:yea,'-',:mon,'-31')) = 4 and (select pt from crujki where id = :id_crujok) = 0)) and 
	CONCAT(:yea,'-',:mon,'-31') not in (select date_work from works_day)
then 5 ELSE 0 END AS `31`






FROM (
select A.id_child,A.id_crujok,A.in_cru,B.out_cru from (select id_child,id_crujok,max(in_cru) AS `in_cru` from crujki_add_deti group by id_child,id_crujok)A left outer join 
(select * from crujki_add_deti)B on A.id_child=B.id_child and A.in_cru=B.in_cru and A.id_crujok=B.id_crujok where A.id_crujok = :id_crujok and
((:mon>=month(A.in_cru) and :yea>=year(A.in_cru)) or :yea>year(A.in_cru))
and (((:mon<=month(B.out_cru) and :yea<=year(B.out_cru)) or :yea<year(B.out_cru)) or B.out_cru is NULL))a LEFT OUTER JOIN deti ON a.id_child=deti.id)u ORDER BY u.`name`