SELECT a.id AS id_gruppa,a.`name` AS namegr,
(SELECT COUNT(*) FROM (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a0 WHERE a0.id_gruppa = a.id) AS countgr,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a1 ON grzdor.child_id = a1.id 
WHERE `year` = :year AND a1.id_gruppa = a.id) AS vsego,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a2 ON grzdor.child_id = a2.id
WHERE `year` = :year AND a2.id_gruppa = a.id and grzdor.number_grzdor = 1) as `1`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a3 ON grzdor.child_id = a3.id
WHERE `year` = :year AND a3.id_gruppa = a.id and grzdor.number_grzdor = 2) as `2`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a4 ON grzdor.child_id = a4.id
WHERE `year` = :year AND a4.id_gruppa = a.id and grzdor.number_grzdor = 3) as `3`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a5 ON grzdor.child_id = a5.id
WHERE `year` = :year AND a5.id_gruppa = a.id and grzdor.number_grzdor = 4) as `4`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a6 ON grzdor.child_id = a6.id
WHERE `year` = :year AND a6.id_gruppa = a.id and grzdor.number_grzdor = 5) as `5`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a7 ON grzdor.child_id = a7.id
WHERE `year` = :year AND a7.id_gruppa = a.id and grzdor.number_fisculture = 1) as `1f`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a8 ON grzdor.child_id = a8.id
WHERE `year` = :year AND a8.id_gruppa = a.id and grzdor.number_fisculture = 2) as `2f`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a9 ON grzdor.child_id = a9.id
WHERE `year` = :year AND a9.id_gruppa = a.id and grzdor.number_fisculture = 3) as `3f`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a10 ON grzdor.child_id = a10.id
WHERE `year` = :year AND a10.id_gruppa = a.id and grzdor.number_fisculture = 4) as `4f`
FROM (SELECT * FROM gruppa)a
UNION
SELECT '','Итого',SUM(countgr),SUM(vsego),SUM(`1`),SUM(`2`),SUM(`3`),SUM(`4`),SUM(`5`),SUM(`1f`),SUM(`2f`),SUM(`3f`),SUM(`4f`) FROM
(SELECT b.id AS id_gruppa,b.`name` AS namegr,
(SELECT COUNT(*) FROM (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))b0 WHERE b0.id_gruppa = b.id) AS countgr,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a1_1 ON grzdor.child_id = a1_1.id
WHERE `year` = :year AND a1_1.id_gruppa = b.id) AS vsego,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a2_1 ON grzdor.child_id = a2_1.id
WHERE `year` = :year AND a2_1.id_gruppa = b.id and grzdor.number_grzdor = 1) as `1`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a3_1 ON grzdor.child_id = a3_1.id
WHERE `year` = :year AND a3_1.id_gruppa = b.id and grzdor.number_grzdor = 2) as `2`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a4_1 ON grzdor.child_id = a4_1.id
WHERE `year` = :year AND a4_1.id_gruppa = b.id and grzdor.number_grzdor = 3) as `3`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a5_1 ON grzdor.child_id = a5_1.id
WHERE `year` = :year AND a5_1.id_gruppa = b.id and grzdor.number_grzdor = 4) as `4`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a6_1 ON grzdor.child_id = a6_1.id
WHERE `year` = :year AND a6_1.id_gruppa = b.id and grzdor.number_grzdor = 5) as `5`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a7_1 ON grzdor.child_id = a7_1.id
WHERE `year` = :year AND a7_1.id_gruppa = b.id and grzdor.number_fisculture = 1) as `1f`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a8_1 ON grzdor.child_id = a8_1.id
WHERE `year` = :year AND a8_1.id_gruppa = b.id and grzdor.number_fisculture = 2) as `2f`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a9_1 ON grzdor.child_id = a9_1.id
WHERE `year` = :year AND a9_1.id_gruppa = b.id and grzdor.number_fisculture = 3) as `3f`,
(SELECT COUNT(*) FROM grzdor LEFT OUTER JOIN (SELECT id, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a10_1 ON grzdor.child_id = a10_1.id
WHERE `year` = :year AND a10_1.id_gruppa = b.id and grzdor.number_fisculture = 4) as `4f`
FROM (SELECT * FROM gruppa)b)m