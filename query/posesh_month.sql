
SELECT k.id,k.`name`,k.kolvse,k.worksday,
k.hodili,round(k.hodili/k.kolvse*100,1) AS `hodili_percent`,
k.nehodili,round(k.nehodili/k.kolvse*100,1) AS `nehodili_percent`,
k.nehodili_boleli,round(k.nehodili_boleli/k.nehodili*(k.nehodili/k.kolvse*100),1) AS `nehodili_boleli_percent`,
k.nehodili_prochee,round(k.nehodili_prochee/k.nehodili*(k.nehodili/k.kolvse*100),1) AS `nehodili_prochee_percent`,
k.nirazu,k.`z-index`
 FROM (
SELECT t.*,:c AS `worksday`,
round(((kolvse*:c)-
(select count(*) from gogo left outer JOIN (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))u on gogo.id_child=u.id where u.id_gruppa = t.id and (gogo.id not in (100,200) or sovsem != 0) and (month(datenotgo)=:mon and YEAR(datenotgo)=:yea)))/:c,2)
 AS `hodili`,
round((
(select count(*) from gogo left outer JOIN (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))u on gogo.id_child=u.id where u.id_gruppa = t.id and (gogo.id not in (100,200) or sovsem != 0) and (month(datenotgo)=:mon and YEAR(datenotgo)=:yea)))/:c,2)
 AS `nehodili`,
 round((
(select count(*) from gogo left outer JOIN (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))u on gogo.id_child=u.id where u.id_gruppa = t.id and (gogo.id not IN (0,100,200) or sovsem != 0) and (month(datenotgo)=:mon and YEAR(datenotgo)=:yea)))/:c,2)
 AS `nehodili_boleli`,
 round((
(select count(*) from gogo left outer JOIN (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))u on gogo.id_child=u.id where u.id_gruppa = t.id AND gogo.id = 0 and (month(datenotgo)=:mon and YEAR(datenotgo)=:yea)))/:c,2)
 AS `nehodili_prochee`,
(kolvse-(select count(distinct(id_child)) from gogo left outer JOIN (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))u on gogo.id_child=u.id where u.id_gruppa = t.id and (gogo.id not IN (0,100,200) or sovsem != 0) and (month(datenotgo)=:mon and YEAR(datenotgo)=:yea))) AS `nirazu`,
ROUND((kolvse-(select count(distinct(id_child)) from gogo left outer JOIN (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))u on gogo.id_child=u.id where u.id_gruppa = t.id and (gogo.id not IN (0,100,200) or sovsem != 0) and (month(datenotgo)=:mon and YEAR(datenotgo)=:yea)))/kolvse,2) AS `z-index`
FROM (
SELECT g.*, 
(SELECT COUNT(*) FROM (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id) AS `kolvse`
FROM (SELECT id,RTRIM(`name`) AS `name` FROM gruppa)g)t)k

