SELECT
  a.id AS `id_all`,
  a.`name` AS `name_all`,
  IFNULL(MONTH(a.`inS`),0) AS `inS`,
  IFNULL(MONTH(a.`outS`),0) AS `outS`,
  IFNULL(year(a.`inS`),0) AS `yinS`,
  IFNULL(year(a.`outS`),0) AS `youtS`,
  case when a.id not IN (select gogoS.id from gogos where dateS = :date_in and propuskS ='H') then 1 ELSE 0 END AS `st`,
  case when a.id IN (select gogos.id from gogos where dateS = :date_in and propuskS ='H') then 1 ELSE 0 END AS `sn`,
  case when a.id IN (select gogos.id from gogos where dateS = :date_in) then (select gogos.propuskS from gogos where dateS = :date_in AND id = a.id) ELSE '' END AS `propuskS`,
  case when a.id IN (select gogos.id from gogos where dateS = :date_in and propuskS ='H' AND surpriseS = '+') then 1 ELSE 0 END AS `surprise`,
  case when a.id IN (select gogos.id from gogos where dateS = :date_in and propuskS LIKE '%не приду%') then 1 ELSE 0 END AS `today`
FROM (
       SELECT
         id,`name`,
         `inS`,
         `outS`
       from sotrudniki where
         ((month(:date_in)>=MONTH(`inS`) and year(:date_in)=year(`inS`)) or year(:date_in)>year(`inS`))
         and (((month(:date_in)<=month(`outS`) and year(:date_in)<=year(`outS`)) or year(:date_in)<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%')a ORDER BY a.`name`