SELECT
  a.id AS `id_child`,
  rtrim(a.`name`) AS `name`,date_format(`rozd`,'%d.%m.%Y') AS `rozd`,
  (SELECT `number_grzdor` FROM grzdor WHERE child_id = a.id AND `year` = :year) as `number_grzdor`,
  (SELECT `comment` FROM grzdor WHERE child_id = a.id AND `year` = :year) as `comment`,
  (SELECT `number_fisculture` FROM grzdor WHERE child_id = a.id AND `year` = :year) as `number_fisculture`

FROM (SELECT
        id,`name`,
        case
        when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa
        end as id_gruppa,
        old_id_gruppa,
        perevod,`start`,
        `in`,
        `out`,
        `rozd`
      from deti where
        year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a WHERE a.id_gruppa = :id_group ORDER BY a.`name`