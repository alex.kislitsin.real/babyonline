SELECT 1 AS `id`,
(select COUNT(*) from gogo left outer join deti on gogo.id_child=deti.id 
where month(datenotgo)=:mon AND year(datenotgo)=:yea and gogo.id not in (100,200) and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL)) AS `znachenie`,'всего пропуски' AS `opisanie`
UNION ALL
SELECT 2, (select COUNT(*) from gogo left outer join deti on gogo.id_child=deti.id 
where month(datenotgo)=:mon AND year(datenotgo)=:yea and gogo.id = 0 and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL)),'пропуски прочее'
UNION ALL
SELECT 3, (select COUNT(*) from gogo left outer join deti on gogo.id_child=deti.id 
where month(datenotgo)=:mon AND year(datenotgo)=:yea and gogo.id not in (0,100,200) and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL)),'пропуски болезни'
UNION ALL
SELECT 4, ((select count(*) from deti where ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))*:c-
(select COUNT(*) from gogo left outer join deti on gogo.id_child=deti.id 
where month(datenotgo)=:mon AND year(datenotgo)=:yea and gogo.id not in (100,200) and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))),'детодни'
UNION ALL
SELECT 5, (select count(*) from deti where ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL)),'всего детей'
UNION ALL
SELECT 6, :c,'количество рабочих дней'
UNION ALL
SELECT 7,(select count(*) from gruppa),'количество групп'