
SELECT category_id,
(SELECT `name` FROM category_cru WHERE id = finish.category_id) AS `name`
,SUM(`summa`) AS `summa`,SUM(`detodays`) AS `detodays`,
price,zarplata AS zp,id_so,rtrim(ped) AS nameso,rozdso,address,snils,seria,
num_passport,kem_vidan,date_vidacha,inn,max(pn) AS `pn`,MAX(vt) AS `vt`,MAX(sr) AS `sr`,MAX(ch) AS `ch`,MAX(pt) AS `pt`
FROM (
SELECT c.*,
CAST(((c.detodays * c.price)/100 * zarplata) AS DECIMAL (8,2)) AS `summa`
FROM (
SELECT b.*, 
((b.count_child * b.count_days)-
(select count(*) from crujki_gogo where month(datenotgo)=:mon and year(datenotgo)=:yea and id_crujok = b.id
AND id_child IN (SELECT id_child FROM crujki_add_deti WHERE (((:mon>=MONTH(`in_cru`) and :yea=year(`in_cru`)) or :yea>year(`in_cru`))
and (((:mon<=month(`out_cru`) and :yea<=year(`out_cru`)) or :yea<year(`out_cru`)) or `out_cru` IS NULL)) and id_crujok = b.id))) AS `detodays`
FROM (
SELECT a.*,
(SELECT count_works_day_cru(:yea,:mon,a.id)) AS `count_days`,
(select count(*) from crujki_add_deti where (((:mon>=MONTH(`in_cru`) and :yea=year(`in_cru`)) or :yea>year(`in_cru`))
and (((:mon<=month(`out_cru`) and :yea<=year(`out_cru`)) or :yea<year(`out_cru`)) or `out_cru` IS NULL)) and id_crujok = a.id) AS `count_child`
FROM (
select crujki.*,rtrim(ifnull(sotrudniki.NAME,'')) as `ped`,rtrim(sotrudniki.address) AS `address`,sotrudniki.rozdso,rtrim(sotrudniki.snils) AS `snils`,sotrudniki.seria,sotrudniki.num_passport,rtrim(sotrudniki.kem_vidan) AS `kem_vidan`,sotrudniki.date_vidacha,sotrudniki.inn from crujki left outer join sotrudniki on crujki.id_so = sotrudniki.id)a WHERE
(pn=1 or vt=1 or sr=1 or ch=1 or pt=1) and (select count(*) from crujki_add_deti where (((:mon>=MONTH(`in_cru`) and :yea=year(`in_cru`)) or :yea>year(`in_cru`))
and (((:mon<=month(`out_cru`) and :yea<=year(`out_cru`)) or :yea<year(`out_cru`)) or `out_cru` IS NULL)) and id_crujok = a.id)>0)b)c)finish 
GROUP BY category_id,price,zarplata,id_so,ped,rozdso,address,snils,seria,num_passport,kem_vidan,date_vidacha,inn



