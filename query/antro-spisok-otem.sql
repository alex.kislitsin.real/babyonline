
SELECT d.*, 
case
when rost between 1 and 85 then '00-А'
when rost between 85.01 and 100 then '0-Б'
when rost between 100.01 and 115 then '1-В'
when rost between 115.01 and 130 then '2-Г'
when rost between 130.01 and 145 then '3-Д'
when rost between 145.01 and 160 then '4'
end mebel,
case
when rost between 1 and 85 then 1
when rost between 85.01 and 100 then 2
when rost between 100.01 and 115 then 3
when rost between 115.01 and 130 then 4
when rost between 130.01 and 145 then 5
when rost between 145.01 and 160 then 6
ELSE 0 end code_mebel,
case
when rost between 1 and 85 then 'до 850'
when rost between 85.01 and 100 then '850-1000'
when rost between 100.01 and 115 then '1000-1150'
when rost between 115.01 and 130 then '1150-1300'
when rost between 130.01 and 145 then '1300-1450'
when rost between 145.01 and 160 then '1400-1600'
end rost_standart,
case
when rost between 1 and 85 then '340'
when rost between 85.01 and 100 then '400'
when rost between 100.01 and 115 then '460'
when rost between 115.01 and 130 then '520'
when rost between 130.01 and 145 then '580'
when rost between 145.01 and 160 then '640'
end visota_stola,
case
when rost between 1 and 85 then '180'
when rost between 85.01 and 100 then '220'
when rost between 100.01 and 115 then '260'
when rost between 115.01 and 130 then '300'
when rost between 130.01 and 145 then '340'
when rost between 145.01 and 160 then '380'
end visota_stula
FROM (
SELECT a.id,a.`name`,a.rozd,
if(
(SELECT max(dat) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:yea,'-','07-01') AND LAST_DAY(CONCAT(:yea,'-','12-01'))),
date_format((SELECT max(dat) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:yea,'-','07-01') AND LAST_DAY(CONCAT(:yea,'-','12-01'))),'%d.%m.%Y'),
'') AS dat,
if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:yea,'-','07-01') AND LAST_DAY(CONCAT(:yea,'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:yea,'-','07-01') AND LAST_DAY(CONCAT(:yea,'-','12-01'))),
'') AS rost,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:yea,'-','07-01') AND LAST_DAY(CONCAT(:yea,'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:yea,'-','07-01') AND LAST_DAY(CONCAT(:yea,'-','12-01'))),
'') AS ves
FROM (SELECT id,`name`,date_format(rozd,'%d.%m.%Y') AS rozd from deti WHERE `out` is NULL AND id_gruppa = :id_group)a ORDER BY a.`name`)d