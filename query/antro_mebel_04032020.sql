SELECT a.id AS id_gruppa,a.`name` AS namegr,
(SELECT COUNT(*) FROM (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a0 WHERE a0.id_gruppa = a.id) AS countgr,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a1 ON rostves.id_child = a1.id 
WHERE rost > 1 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a1.id_gruppa = a.id) AS zzz,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a2 ON rostves.id_child = a2.id 
WHERE rost between 1 AND 85 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a2.id_gruppa = a.id) AS y1,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a3 ON rostves.id_child = a3.id 
WHERE rost between 85.01 and 100 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a3.id_gruppa = a.id) AS y2,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a4 ON rostves.id_child = a4.id 
WHERE rost between 100.01 and 115 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a4.id_gruppa = a.id) AS y3,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a5 ON rostves.id_child = a5.id 
WHERE rost between 115.01 and 130 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a5.id_gruppa = a.id) AS y4,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a6 ON rostves.id_child = a6.id 
WHERE rost between 130.01 and 145 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a6.id_gruppa = a.id) AS y5,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a7 ON rostves.id_child = a7.id 
WHERE rost between 145.01 and 160 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a7.id_gruppa = a.id) AS y6
FROM (SELECT * FROM gruppa)a
UNION
SELECT '','Итого',SUM(countgr),SUM(zzz),SUM(y1),SUM(y2),SUM(y3),SUM(y4),SUM(y5),SUM(y6) FROM
(SELECT b.id AS id_gruppa,b.`name` AS namegr,
   (SELECT COUNT(*) FROM (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a0_1 WHERE a0_1.id_gruppa = b.id) AS countgr,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a1_1 ON rostves.id_child = a1_1.id
   WHERE rost > 1 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a1_1.id_gruppa = b.id) AS zzz,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a2_1 ON rostves.id_child = a2_1.id
   WHERE rost between 1 AND 85 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a2_1.id_gruppa = b.id) AS y1,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a3_1 ON rostves.id_child = a3_1.id
   WHERE rost between 85.01 and 100 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a3_1.id_gruppa = b.id) AS y2,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a4_1 ON rostves.id_child = a4_1.id
   WHERE rost between 100.01 and 115 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a4_1.id_gruppa = b.id) AS y3,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a5_1 ON rostves.id_child = a5_1.id
   WHERE rost between 115.01 and 130 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a5_1.id_gruppa = b.id) AS y4,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a6_1 ON rostves.id_child = a6_1.id
   WHERE rost between 130.01 and 145 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a6_1.id_gruppa = b.id) AS y5,
   (SELECT COUNT(*) FROM rostves LEFT OUTER JOIN (SELECT id, case when :yea < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :yea and (:yea<year(`out`) or `out` is NULL))a7_1 ON rostves.id_child = a7_1.id
   WHERE rost between 145.01 and 160 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND a7_1.id_gruppa = b.id) AS y6
FROM (SELECT * FROM gruppa)b)m