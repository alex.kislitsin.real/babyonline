
	create proc Table_4_group @year int, @month1 int, @month2 int as

	if @year > year(getdate()) or (@year = year(getdate()) and (@month1 > month(getdate()) or @month2 > month(getdate()))) or (@year = year(getdate()) and @month1 > @month2)
	begin
	print 'early'
	end
	else
	begin
	
	
	declare @nametable char(20)
	--declare @year int = 2020
	--declare @year2 int = 2019
	--declare @month1 int = 1
	--declare @month2 int = 1
	declare @period_month int = (@month2 - @month1) + 1
	declare @month_iterator int
	declare @summa_sr_spisok numeric(9,2)


	declare @sr_propuski_bolezni numeric(9,2)
	declare @sr_hodili_opt numeric(9,2)
	declare @sr_propuski_vsego_minus numeric(9,2)
	declare @sr_hodili_percent numeric(9,0)
	declare @sr_propuski_vsego_minus_percent numeric(9,0)
	declare @sr_propuski_bolezni_percent_all numeric(9,0)
	declare @sr_propuski_bolezni_percent_ot_propuskov numeric(9,0)
	declare @count_child_never_bolezn numeric(9,0)
	declare @index_z numeric(9,2)

	declare @min_number_gr int
	declare @max_number_gr int
	--declare @sr_hodili numeric(9,2)
	--declare @sr_propuski numeric(9,2)
	declare @counter int
	declare @count_works_days numeric(9,2) = 0
	
	--declare @count_child_never_bolezn int

	declare @date_start date = concat(@year,'-',@month1,'-01')
	declare @date_begin date = @date_start
	DECLARE @count_days int
	declare @date_end date = concat(@year,'-',@month2,'-01')
	set @count_days = DAY(DATEADD(Month, 1, cast(@date_end as datetime)) - DAY(DATEADD(Month, 1, cast(@date_end as datetime))))
	set @date_end = concat(@year,'-',@month2,'-',@count_days) 
	print @date_begin
	print @date_end

	while @date_begin <= @date_end
		begin
		if (@date_begin not in (select * from holidays)) and (datepart(WEEKDAY,@date_begin) not in (6,7))
			begin
			
			set @count_works_days = @count_works_days + 1
			end
		set @date_begin = dateadd(day,1,@date_begin)
		end
	print @count_works_days
	
	set @date_begin = @date_start

	IF OBJECT_ID(N'tempdb..#tab_4_1',N'U') IS NOT NULL
	begin
	drop table #tab_4_1
	print 'del'
	end;
	print 'create'
	select * into #tab_4_1 from gruppa

	--select * from #tab_4_1

	alter table #tab_4_1 add sr_spisok  numeric(9,0) default 0 not null
	--alter table #tab_4_1 add sr_hodili  numeric(9,2) default 0 not null
	alter table #tab_4_1 add sr_hodili_opt  numeric(9,2) default 0 not null
	alter table #tab_4_1 add sr_hodili_percent  numeric(9,0) default 0 not null
	alter table #tab_4_1 add sr_propuski_vsego_minus  numeric(9,2) default 0 not null
	alter table #tab_4_1 add sr_propuski_vsego_minus_percent  numeric(9,0) default 0 not null
	
	alter table #tab_4_1 add sr_propuski_bolezni  numeric(9,2) default 0 not null
	alter table #tab_4_1 add sr_propuski_bolezni_percent_all  numeric(9,0) default 0 not null
	alter table #tab_4_1 add sr_propuski_bolezni_percent_ot_propuskov  numeric(9,0) default 0 not null
	--alter table #tab_4_1 add sr_spisok int default 0 not null

	alter table #tab_4_1 add count_child_never_bolezn numeric(9,0) default 0 not null

	alter table #tab_4_1 add index_z numeric(9,2) default 0 not null

	set @month_iterator = @month1
	set @min_number_gr = (select min(id) from #tab_4_1)
	set @max_number_gr = (select max(id) from #tab_4_1)

	while @min_number_gr <= @max_number_gr
		begin
		set @summa_sr_spisok = 0
		set @sr_propuski_bolezni = 0
		set @sr_hodili_opt = 0
		--set @sr_propuski_vsego_minus = 0
		--set @sr_hodili_percent = 0
		--set @sr_propuski_vsego_minus_percent = 0
		--set @sr_propuski_bolezni_percent_all = 0
		--set @sr_propuski_bolezni_percent_ot_propuskov = 0
		set @count_child_never_bolezn = 0
		--set @index_z = 0

		set @month_iterator = @month1

		while @month_iterator <= @month2
			begin
			if @month_iterator >= month(getdate())
				begin
				set @nametable = 'deti'
				end
				else begin
				set @nametable = concat('deti',@month_iterator,@year)
				end
			print @nametable
			create table #temp1
			(id int,id_gruppa int, name char(50), old_id_gruppa int, perevod date)
			insert into #temp1
			exec('select id,id_gruppa,name,old_id_gruppa,perevod from '+@nametable+' where 
			((('+@month_iterator+'>=month([in]) and '+@year+'>=year([in])) or '+@year+'>year([in]))
			and ((('+@month_iterator+'<=month([out]) and '+@year+'<=year([out])) or '+@year+'<year([out])) or [out] is null)) ')
			--select * from #temp1 order by id_gruppa,name
			update #temp1 set id_gruppa = old_id_gruppa where (@month_iterator < month(perevod) and @year = year(perevod)) or ( @year < year(perevod))

			set @summa_sr_spisok = @summa_sr_spisok + (select count(*) from #temp1 where id_gruppa = @min_number_gr)

			set @sr_propuski_bolezni = @sr_propuski_bolezni+ (((select count(*) from gogo left outer join #temp1 on gogo.id_child=#temp1.id where #temp1.id_gruppa = @min_number_gr and 
			(gogo.id not in (0,100,200) or sovsem != 0) and (datenotgo between @date_begin and @date_end)))/@count_works_days)

			set @sr_hodili_opt = @sr_hodili_opt + ((((select count(*) from #temp1 where id_gruppa = @min_number_gr) *@count_works_days)-(select count(*) from gogo left outer join #temp1 on gogo.id_child=#temp1.id 
			where #temp1.id_gruppa = @min_number_gr and (gogo.id not in (100,200) or sovsem != 0) and (datenotgo between @date_begin and @date_end)))/@count_works_days)

			set @count_child_never_bolezn = @count_child_never_bolezn + (((select count(*) from #temp1 where id_gruppa = @min_number_gr) - 
			(select count(distinct(id_child)) from gogo left outer join #temp1 on gogo.id_child=#temp1.id where #temp1.id_gruppa = @min_number_gr and 
			(gogo.id not in (0,100,200) or sovsem != 0) and (datenotgo between @date_begin and @date_end))))

			drop table #temp1
			set @month_iterator = @month_iterator + 1
			end

		set @summa_sr_spisok = @summa_sr_spisok/@period_month

		set @sr_propuski_bolezni = @sr_propuski_bolezni/@period_month
		set @sr_hodili_opt = @sr_hodili_opt/@period_month
		set @count_child_never_bolezn = @count_child_never_bolezn/@period_month
		update #tab_4_1 set 
		sr_spisok = isnull(@summa_sr_spisok,0),
		sr_propuski_bolezni = isnull(@sr_propuski_bolezni,0),
		sr_hodili_opt = isnull(@sr_hodili_opt,0),
		count_child_never_bolezn = isnull(@count_child_never_bolezn,0)
		where id = @min_number_gr
		set @min_number_gr = @min_number_gr + 1
		end

	set @date_begin = @date_start

	--select * from #tab_4_1

	--update #tab_4_1 set sr_propuski_bolezni = ((select count(*) from gogo where id_gruppa = #tab_4_1.id and 
	--(gogo.id not in (0,100,200) or sovsem != 0) and (datenotgo between @date_begin and @date_end)))/@count_works_days
	--update #tab_4_1 set sr_hodili_opt = ((sr_spisok *@count_works_days)-(select count(*) from gogo where id_gruppa = #tab_4_1.id and 
	--(gogo.id not in (100,200) or sovsem != 0) and (datenotgo between @date_begin and @date_end)))/@count_works_days
	update #tab_4_1 set sr_propuski_vsego_minus = sr_spisok - sr_hodili_opt
	update #tab_4_1 set sr_hodili_percent = (sr_hodili_opt/sr_spisok)*100
	update #tab_4_1 set sr_propuski_vsego_minus_percent = (sr_propuski_vsego_minus/sr_spisok)*100
	update #tab_4_1 set sr_propuski_bolezni_percent_all = (sr_propuski_bolezni/sr_spisok)*100
	update #tab_4_1 set sr_propuski_bolezni_percent_ot_propuskov = (sr_propuski_bolezni/sr_propuski_vsego_minus)*100

	--update #tab_4_1 set count_child_never_bolezn = (sr_spisok - (select count(distinct(id_child)) from gogo where id_gruppa = #tab_4_1.id and 
	--(gogo.id not in (0,100,200) or sovsem != 0) and (datenotgo between @date_begin and @date_end)))

	update #tab_4_1 set index_z = count_child_never_bolezn / sr_spisok
		
	select * from #tab_4_1
	union
	select 
	id = (select max(id) from #tab_4_1)+1,'В среднем по садику:',
	sr_spisok = convert(numeric (9,0),(sum(sr_spisok))),
	sr_hodili_opt = convert(numeric (9,2),(sum(sr_hodili_opt))),
	sr_hodili_percent = convert(numeric (9,0),(avg(sr_hodili_percent))),
	sr_propuski_vsego_minus = convert(numeric (9,2),(avg(sr_propuski_vsego_minus))),
	--sr_propuski_vsego_minus_percent = convert(numeric (9,0),(avg(sr_propuski_vsego_minus_percent))),

	sr_propuski_vsego_minus_percent = convert(numeric (9,0),(100 - (convert(numeric (9,0),(avg(sr_hodili_percent)))))),

	sr_propuski_bolezni = convert(numeric (9,2),(sum(sr_propuski_bolezni))),
	sr_propuski_bolezni_percent_all = convert(numeric (9,0),(avg(sr_propuski_bolezni_percent_all))),
	sr_propuski_bolezni_percent_ot_propuskov = convert(numeric (9,0),(avg(sr_propuski_bolezni_percent_ot_propuskov))),
	count_child_never_bolezn = convert(numeric (9,0),(avg(count_child_never_bolezn))),
	index_z = convert(numeric (9,2),(avg(index_z)))
	from #tab_4_1

	drop table #tab_4_1
	end
	
	--exec Table_4_group 2020,1,1

	--drop proc Table_4_group

	