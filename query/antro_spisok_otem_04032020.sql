SELECT a.id,a.`name`,a.rozd,
if(
(SELECT max(dat) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','07-01') AND LAST_DAY(CONCAT(:year,'-','12-01'))),
date_format((SELECT max(dat) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','07-01') AND LAST_DAY(CONCAT(:year,'-','12-01'))),'%d.%m.%Y'),
'') AS dat,
if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','07-01') AND LAST_DAY(CONCAT(:year,'-','12-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','07-01') AND LAST_DAY(CONCAT(:year,'-','12-01'))),
0) AS rost,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','07-01') AND LAST_DAY(CONCAT(:year,'-','12-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','07-01') AND LAST_DAY(CONCAT(:year,'-','12-01'))),
0) AS ves
FROM (select * from (SELECT id,`name`,date_format(rozd,'%d.%m.%Y') AS rozd, case when :year < year(perevod) AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti WHERE year(`in`) <= :year and (:year<year(`out`) or `out` is NULL))a0 where id_gruppa = :id_group)a ORDER BY a.`name`