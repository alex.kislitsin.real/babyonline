SELECT 'Отрицательная' AS `name`, 
COUNT(*) AS `q1` FROM mantu LEFT OUTER JOIN deti ON mantu.id_child = deti.id WHERE `out` IS NULL AND YEAR(dat) = YEAR(NOW()) AND (el2 = 0 and el2 != '')
UNION
SELECT 'Сомнительная' AS `name`, 
COUNT(*) AS `q1` FROM mantu LEFT OUTER JOIN deti ON mantu.id_child = deti.id WHERE `out` IS NULL AND YEAR(dat) = YEAR(NOW()) AND ((el2 > 0 and el2 < 5) or el1 like '%ar%')
UNION
SELECT 'Положительная' AS `name`, 
COUNT(*) AS `q1` FROM mantu LEFT OUTER JOIN deti ON mantu.id_child = deti.id WHERE `out` IS NULL AND YEAR(dat) = YEAR(NOW()) AND (el2 > 4 and el2 < 17 and el1 LIKE 'p%')
UNION
SELECT 'Гиперергическая' AS `name`, 
COUNT(*) AS `q1` FROM mantu LEFT OUTER JOIN deti ON mantu.id_child = deti.id WHERE `out` IS NULL AND YEAR(dat) = YEAR(NOW()) AND (el2 > 16 and el1 LIKE 'p%')
UNION
SELECT 'Вираж' AS `name`, 
COUNT(*) AS `q1` FROM (SELECT k.*, 
if((select MAX(dat) FROM mantu WHERE id_child = k.id_child AND YEAR(dat) = YEAR(NOW()) AND (el2 > 4 and el2 < 17 and el1 LIKE 'p%')),1,0) AS `pro2`
FROM (SELECT id_child, MAX(dat) AS `dat555` FROM mantu WHERE YEAR(dat) = (YEAR(NOW())-1) AND (el2 = 0 and el2 != '') 
AND id_child IN (SELECT id FROM deti WHERE `out` IS NULL) GROUP BY id_child,dat)k)t WHERE pro2 = 1
UNION
SELECT 'Списочный состав' AS `name`,(select count(*) from deti WHERE `out` is null) AS `q1`
UNION
SELECT 'Сделано' AS `name`,
COUNT(*) AS `q1` FROM mantu LEFT OUTER JOIN deti ON mantu.id_child = deti.id WHERE `out` IS NULL AND YEAR(dat) = YEAR(NOW()) AND el2 != ''
UNION
SELECT '% Выполнения плана' AS `name`,
ROUND((SELECT COUNT(*) FROM mantu LEFT OUTER JOIN deti ON mantu.id_child = deti.id WHERE `out` IS NULL AND YEAR(dat) = YEAR(NOW()) AND el2 != '')/
(SELECT COUNT(*) from deti WHERE `out` is NULL)*100)