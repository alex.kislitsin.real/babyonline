<div class="my_table_optd my_table my_table2 not_selected_text_on_block" id="table_journal">
    <table class="table-bordered" id="t_mantu" data-all="<?= $all_deti ?>" data-notall="<?= $not_all_deti ?>">
        <?php
        $id_child_save = null;
        $i = 1;
        foreach($model as $q){

            if (strpos($q['date_to'],'1900-01-01') !== false){
                $date_to = '';
            }else{
                $date_to = Yii::$app->formatter->asDate(trim($q['date_to']));
            }
            if (strpos($q['date_osmotr_ptd'],'1900-01-01') !== false){
                $date_osmotr_ptd = '';
            }else{
                $date_osmotr_ptd = Yii::$app->formatter->asDate(trim($q['date_osmotr_ptd']));
            }
            if (strpos($q['date_sled_yavki'],'1900-01-01') !== false){
                $date_sled_yavki = '';
                $class = '';
            }else{
                $date_sled_yavki = Yii::$app->formatter->asDate(trim($q['date_sled_yavki']));
                $class = 'class_uchet_optd';///css -> journal.css
            }
            if (strpos($q['medotvod'],'1900-01-01') !== false){
                $medotvod = '';
            }else{
                $medotvod = Yii::$app->formatter->asDate(trim($q['medotvod']));
            }
            if (strpos($q['date_vidacha'],'1900-01-01') !== false){
                $date_vidacha = '';
            }else{
                $date_vidacha = Yii::$app->formatter->asDate(trim($q['date_vidacha']));
            }

            switch($q['status_ptd']){
                case 0:
                    $status = '';
//                    $status = 'не выбрано';
                    break;
                case 1:
                    $status = 'на учёте';
                    break;
                case 2:
                    $status = 'снят с учёта';
                    break;
                case 5:
                    $status = 'не подлежит учёту';
                    break;
            }

            strpos($q['rozd'],'1900-01-01') !== false ? $rozd = '' : $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.(Yii::$app->formatter->asDate(trim($q['rozd']))).'</span>';


            if ($id_child_save != null){
                if ($id_child_save == $q['id_child']){
                    $name = '';
                    $rozd = '';
                    $ii = '';
                    $group = '';
                    $class = 'class_uchet_optd';
                }else{
                    $id_child_save = $q['id_child'];
                    $name = trim($q['name']);
                    $ii = $i;
                    $group = $q['id_gruppa'];
                }
            }else{
                $id_child_save = $q['id_child'];
                $name = trim($q['name']);
                $ii = $i;
                $group = $q['id_gruppa'];
            }


            echo '<tr data-id="'.$q['id'].'" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'"
            data-rozd="'.(Yii::$app->formatter->asDate(trim($q['rozd']))).'" data-id_gruppa="'.$q['id_gruppa'].'"
            data-date_to="'.$date_to.'" data-pred_diagnoz="'.trim($q['pred_diagnoz']).'" data-date_osmotr_ptd="'.$date_osmotr_ptd.'"
            data-finish_diagnoz_ptd="'.trim($q['finish_diagnoz_ptd']).'" data-status_ptd="'.$q['status_ptd'].'"
            data-date_sled_yavki="'.$date_sled_yavki.'" data-medotvod="'.$medotvod.'" data-date_vidacha="'.$date_vidacha.'" data-coment="'.trim($q['coment']).'">
                <td class="not_hover_td '.$class.'" style="width: 3%">'.$ii.'</td>
                <td class="idstart '.$class.'" id="id_n" style="width: 22%">'.$name.$rozd.'</td>
                <td class="not_hover_td '.$class.'" style="width: 3%">'.$group.'</td>
                <td class="idstart '.$class.'" style="width: 9%">'.$date_to.'</td>
                <td class="idstart '.$class.'" style="width: 9%">'.$date_osmotr_ptd.'</td>
                <td class="idstart '.$class.'" style="width: 22%">'.$q['finish_diagnoz_ptd'].'</td>
                <td class="idstart '.$class.'" style="width: 14%">'.$status.'</td>
                <td class="idstart '.$class.'" style="width: 9%">'.$date_sled_yavki.'</td>
                <td class="idstart '.$class.'" style="width: 9%">'.$medotvod.'</td>
            </tr>';


            if (strlen($name)>0)$i++;
        }
        ?>
    </table>
</div>

<?php
$script = <<<JS

$(function(){

    /*$('#id_but_close_modal_control_yavka').on('ckick',function(){
        $('.modal').modal('hide');
    });*/

    var id_child,id;

    $('#but_id_go_to_optd').on('click',function(){

        /*$('#date_vidacha,#pred_diagnoz,#date_osmotr_ptd,#finish_diagnoz_ptd,#drop_status_optd,#date_sled_yavki,#medotvod,#coment,#date_vidacha_edit,#pred_diagnoz_edit,#date_osmotr_ptd_edit,#finish_diagnoz_ptd_edit,#date_sled_yavki_edit,#medotvod_edit,#coment_edit').val('');*/

//        $('#form_optd_new_child > input').val('');

        $('#form_optd_new_child').trigger('reset');

        $('#drop_group_optd option:selected').each(function(){
            this.selected=false;
        });
        $('#drop_name_optd option:selected').each(function(){
            this.selected=false;
        });
        $('#drop_status_optd option:selected').each(function(){
            this.selected=false;
        });
        $('#drop_name_optd').prop('disabled',true);

        $('#modal_journal').modal('show');

        return false;
    });

    $('#drop_group_optd').on('change',function(){
        console.log($(this).val());
        var arr = $('#form_optd_new_child');
        $('#hidden_pole_id_j').val(15);
        var test = $('#form_optd_new_child').serializeArray();
        console.log(test);//return;
        $('#drop_name_optd').empty().append($('<option value="">Выберите имя</option>'));
        $('#drop_name_optd option:selected').each(function(){
            this.selected=false;
        });
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : test
        }).done(function(response) {
        $("*").LoadingOverlay("hide");
            var array_deti_in_item_group = $.parseJSON(response);
            console.log(array_deti_in_item_group);
            $.each(array_deti_in_item_group, function(index, value) {
                var name = value.name;
                $('#drop_name_optd').append($('<option value="'+value.id+'">'+name.trim()+'</option>'));
            });
            $('#drop_name_optd').prop('disabled',false);

        }).fail(function() {
        $("*").LoadingOverlay("hide");
            console.log('not1');
        });
    });

    /*$('#drop_name_optd').on('change',function(){
        $('#hidden_id_child').val($('#drop_name_optd').val());
    });*/



    $('.my_table_optd table tr:not(.not_hover_td)').on('click',function(){

        $('#form_optd_edit_child').trigger('reset');

        $('#drop_status_optd_edit option:selected').each(function(){
            this.selected=false;
        });

        id_child = $(this).data('id_child');
        id = $(this).data('id');
        var rozd = $(this).data('rozd');
        var name = $(this).data('name');
        var id_gruppa = $(this).data('id_gruppa');
        var date_to = $(this).data('date_to');
        var pred_diagnoz = $(this).data('pred_diagnoz');
        var date_osmotr_ptd = $(this).data('date_osmotr_ptd');
        var finish_diagnoz_ptd = $(this).data('finish_diagnoz_ptd');
        var status_ptd = $(this).data('status_ptd');
        var date_sled_yavki = $(this).data('date_sled_yavki');
        var date_vidacha = $(this).data('date_vidacha');
        var medotvod = $(this).data('medotvod');
        var coment = $(this).data('coment');

        /*console.log(id_child+'*id_child');
        console.log(rozd+'*rozd');
        console.log(name+'*name');
        console.log(id_gruppa+'*id_gruppa');
        console.log(date_to+'*date_to');
        console.log(pred_diagnoz+'*pred_diagnoz');
        console.log(date_osmotr_ptd+'*date_osmotr_ptd');
        console.log(finish_diagnoz_ptd+'*finish_diagnoz_ptd');
        console.log(status_ptd+'*status_ptd');
        console.log(date_sled_yavki+'*date_sled_yavki');
        console.log(medotvod+'*medotvod');
        console.log(coment+'*coment');//return;*/

        $('#name_modal_optd_edit').val(name.trim());
        $('#date_to_edit').val(date_to);
        $('#date_vidacha_edit').val(date_vidacha);
        $('#pred_diagnoz_edit').val(pred_diagnoz.trim());
        $('#date_osmotr_ptd_edit').val(date_osmotr_ptd);
        $('#finish_diagnoz_ptd_edit').val(finish_diagnoz_ptd.trim());
        $('#date_sled_yavki_edit').val(date_sled_yavki);
        $('#medotvod_edit').val(medotvod);
        $('#coment_edit').val(coment.trim());

        console.log(status_ptd+' status_ptd');

        $("#drop_status_optd_edit option").each(function() {
                var f = $(this).val();
                console.log(f);
                if(status_ptd==f){
                    $(this).prop("selected", true);
                }
            });

        $('#modal_optd_edit').modal('show');

        return false;
    });


    $('#but_save_optd').click(function(){

        var id_child1 = $('#drop_name_optd').val();
//        console.log(id_child);return;

        $('#hidden_pole_id_j').val(16);
        var arr = $('#form_optd_new_child');
        var test = $('#form_optd_new_child').serializeArray();
        test.push({name:'Optd[id_child]',value:id_child1});
        console.log(test);//return;
        $('.modal').modal('hide');
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : test
        }).done(function(response) {
        $("*").LoadingOverlay("hide");
            console.log(response);
            $('#table_sotrudniki').html(response);
        }).fail(function() {
        $("*").LoadingOverlay("hide");
            console.log('not1');
        });

        return false;
    });

    $('#but_save_optd_edit').click(function(){
        var status = $('#drop_status_optd_edit').val();
        $('#drop_status_optd').val(status);
        console.log(status);
        $('#hidden_pole_id_j_edit').val(17);
        var arr = $('#form_optd_edit_child');
        var test = $('#form_optd_edit_child').serializeArray();
        test.push({name:'Optd[id_child]',value:id_child});
        test.push({name:'Optd[id]',value:id});
        console.log(test);//return;
        $('.modal').modal('hide');
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : test
        }).done(function(response) {
        $("*").LoadingOverlay("hide");
            console.log(response);
            $('#table_sotrudniki').html(response);
        }).fail(function() {
        $("*").LoadingOverlay("hide");
            console.log('not1');
        });

        return false;
    });

    $('#but_del_optd').click(function(){
        $('#hidden_pole_id_j_edit').val(18);
        var arr = $('#form_optd_edit_child');
        var test = $('#form_optd_edit_child').serializeArray();
        test.push({name:'Optd[id_child]',value:id_child});
        test.push({name:'Optd[id]',value:id});
        console.log(test);//return;
        $('.modal').modal('hide');
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : test
        }).done(function(response) {
        $("*").LoadingOverlay("hide");
            console.log(response);
            $('#table_sotrudniki').html(response);
        }).fail(function() {
        $("*").LoadingOverlay("hide");
            console.log('not1');
        });

        return false;
    });




})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>