<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
//use yii\grid\GridView;
//use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);


Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

?>





<?php

/*switch($status){
    case 0:
        $status = 0;
        break;
    case 1:
        $status = 1;
        $model5 = array();
        foreach($model as $key  => $value){
            if(strpos($value['dat'],'1900') !== false){
                array_push($model5,$value);
            }
        }
        $model = $model5;
        break;
}*/


?>


<div id="id_table_to_table_optd">
    <?= $this->render('table_to_table_mantu_n',compact(
        'all_deti',
        'not_all_deti',
        'model',
        'all_deti',
        'all_deti'
    )) ?>
</div>





<!--echo '<tr data-id_child="'.$q['id_child'].'" data-name="'.$q['name'].'" data-rozd="'.$rozd.'" data-id_gruppa="'.$q['id_gruppa'].'" data-date_to="'.$date_to.'" data-pred_diagnoz="'.$q['pred_diagnoz'].'" data-date_osmotr_ptd="'.$date_osmotr_ptd.'" data-finish_diagnoz_ptd="'.$q['finish_diagnoz_ptd'].'" data-status_ptd="'.$status.'" data-medotvod="'.$medotvod.'" data-coment="'.$q['coment'].'">


-->



<?php /*$form = ActiveForm::begin(); */?><!--

<?/*= $form->field($model_optd,'id_child')->hiddenInput([
    'id' => 'hidden_id_child',
])->label(false); */?>

--><?php /*ActiveForm::end(); */?>

<?php
 Modal::begin([
     'id' => 'modal_journal',
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="#">Создать запись в таблице направленных в ОПТД</h4>',
     'size' => Modal::SIZE_LARGE,
     'footer' => '<button type="submit" class="btn btn-success btn-md" id="but_save_optd">Создать запись</button>',
     'clientOptions' => [
         'backdrop' => 'static',
         'keyboard' => false,
     ],
 ]);
?>



<?php $form = ActiveForm::begin([
    'id' => 'form_optd_new_child',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => ['journal/journal'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-5',
            'offset' => 'col-lg-offset-3',
            'wrapper' => 'col-lg-7',
        ],
    ],
]); ?>

<div class="col-sm-6">

    <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
        'id' => 'drop_group_optd',
        'prompt' => 'Выберите группу',
    ])->label('Группа') ?>

    <?= $form->field($model_optd,'name')->dropDownList($array_all_deti_optd,[
        'id' => 'drop_name_optd',
        'prompt' => 'Выберите ребёнка',
        'disabled' => 'disabled'
    ])->label('Фамилия Имя') ?>

    <?= $form->field($model_optd, 'date_to')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_to',
            'placeholder' => 'Дата направления',
//            'value' => date('d.m.Y'),
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'date_vidacha')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_vidacha',
            'placeholder' => 'Дата выдачи направления',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'pred_diagnoz')->textarea([
        'id' => 'pred_diagnoz',
        'rows' => 2,
        'placeholder' => 'Предварительный диагноз'
    ]) ?>

</div>
<div class="col-sm-6">

    <?= $form->field($model_optd, 'date_osmotr_ptd')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_osmotr_ptd',
            'placeholder' => 'Дата осмотра в ОПТД',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'finish_diagnoz_ptd')->textarea([
        'id' => 'finish_diagnoz_ptd',
        'rows' => 2,
        'placeholder' => 'Окончательный диагноз ОПТД'
    ]) ?>

    <?= $form->field($model_optd,'status_ptd')->dropDownList($array_status,[
        'id' => 'drop_status_optd',
        'prompt' => 'Выберите статус',
    ])->label('Статус') ?>

    <?= $form->field($model_optd, 'date_sled_yavki')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_sled_yavki',
            'placeholder' => 'Дата следующей явки',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?= $form->field($model_optd, 'medotvod')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'medotvod',
            'placeholder' => 'Медотвод',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'coment')->textarea([
        'id' => 'coment',
        'rows' => 2,
        'placeholder' => 'Примечание'
    ]) ?>


</div>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_pole_id_j',
])->label(false); ?>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>

<!--второе модальное окно для редактирования-->

<?php
 Modal::begin([
     'id' => 'modal_optd_edit',
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="id_header_optd_edit"></h4>',
     'size' => Modal::SIZE_LARGE,
     'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_optd">Удалить запись</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_optd_edit">Сохранить данные</button>',
     'clientOptions' => [
         'backdrop' => 'static',
         'keyboard' => false,
     ],
 ]);
?>



<?php $form = ActiveForm::begin([
    'id' => 'form_optd_edit_child',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => ['journal/journal'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-5',
            'offset' => 'col-lg-offset-3',
            'wrapper' => 'col-lg-7',
        ],
    ],
]); ?>

<div class="col-sm-6">

    <?= $form->field($model_optd, 'name_edit')->textInput([
        'id' => 'name_modal_optd_edit',
        'placeholder' => 'Фамилия Имя',
        'readonly'=> true
    ]) ?>

    <?= $form->field($model_optd, 'date_to')->widget('yii\widgets\MaskedInput', [

        'options' => [
            'id' => 'date_to_edit',
            'placeholder' => 'Дата направления',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'date_vidacha')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_vidacha_edit',
            'placeholder' => 'Дата выдачи направления',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'pred_diagnoz')->textarea([
        'id' => 'pred_diagnoz_edit',
        'rows' => 2,
        'placeholder' => 'Предварительный диагноз'
    ]) ?>

</div>
<div class="col-sm-6">

    <?= $form->field($model_optd, 'date_osmotr_ptd')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_osmotr_ptd_edit',
            'placeholder' => 'Дата осмотра в ОПТД',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'finish_diagnoz_ptd')->textarea([
        'id' => 'finish_diagnoz_ptd_edit',
        'rows' => 2,
        'placeholder' => 'Окончательный диагноз ОПТД'
    ]) ?>

    <?= $form->field($model_optd,'status_ptd')->dropDownList($array_status,[
        'id' => 'drop_status_optd_edit',
        'prompt' => 'Выберите статус',
    ])->label('Статус') ?>

    <?= $form->field($model_optd, 'date_sled_yavki')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_sled_yavki_edit',
            'placeholder' => 'Дата следующей явки',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?= $form->field($model_optd, 'medotvod')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'medotvod_edit',
            'placeholder' => 'Медотвод',
        ],
        'mask' => '99.99.9999',
    ]) ?>

    <?= $form->field($model_optd, 'coment')->textarea([
        'id' => 'coment_edit',
        'rows' => 2,
        'placeholder' => 'Примечание'
    ]) ?>


</div>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_pole_id_j_edit',
])->label(false); ?>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>



<?php Modal::begin([
    'id' => 'modal_control_yavka',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">Контрольная явка в ПТД на текущий месяц</h4>',
    'footer' => '<button type="submit" class="btn btn-success btn-md" id="id_but_close_modal_control_yavka">Закрыть</button>',

//    'footer' => Html::a('<span class="glyphicon glyphicon-save"></span> Сохранить отчёт в Excel', ['sp/exceltest','id' => 2], ['class'=>'btn btn-md btn-success']),

    'size' => Modal::SIZE_LARGE
]);?>
<div id="id_div_table_plan_control_yavka_optd"><?= $this->render('modal_control_yavka',compact('array_control_yavka')) ?></div>

<?php Modal::end();?>




<?php
$sss = <<<JS
$(function(){
    $('#select_optd_status').change(function(e){
        e.preventDefault();
        var c = $(this).val();
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
                var form = $('#form_item_group_all_journal,#form_rep_item_journal').serializeArray();
                var testform = $('#form_rep_item_journal');
                form.push({name:'Id[id]',value:20});
                form.push({name:'id7',value:0});
                form.push({name:'ccc',value:c});console.log(form);//return;
                $.ajax({
                    type : testform.attr('method'),
                    url : testform.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    if(response==400){
                            $('#modal_error').modal('show');
                        }else{
                            $('#id_table_to_table_optd').html(response);
                        }
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    console.log('not');
                });

        return false;

    });
});
JS;
$this->registerJs($sss,yii\web\View::POS_END);
?>

