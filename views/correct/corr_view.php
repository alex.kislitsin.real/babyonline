<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
?>




<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">

            <div class="date_picker_so">
                <?php $form = ActiveForm::begin([
                    'id' => 'id_form_corr444',
                    'type' => ActiveForm::TYPE_VERTICAL,
                ])?>
                <?= $form->field($model, 'medotvod')->textInput([
                    'id' => 'medotvod',
                    'placeholder' => 'Медотвод (кол-во дней)',
                    'type' => 'number'
                ])->label(false) ?>
                <?php $form = ActiveForm::end()?>
            </div>

        </div>
        <div class="rep_boss_up_panel_1" data-id7="0">
            <div class="date_picker_so">
                <?php $form = ActiveForm::begin([
                    'id' => 'id_form_corr',
                    'action' => ['correct/corr'],
                    'method' => 'POST',
                    'type' => ActiveForm::TYPE_INLINE,
                    'enableAjaxValidation' => false,//
                ])?>

            <div class="btn-group btn-group" role="group">
                <div class="btn-group pull_right" role="group">
                    <?= $form->field($model, 'datbegin')->widget('yii\widgets\MaskedInput', [
                        'options' => [
                            'id' => 'datbegin',
//                            'autofocus' => true,
//                            'value' => '20.09.2019',
                            'placeholder' => 'начальная дата',
                            'style' => 'color:black;border-radius:3px 0 0 3px'
                        ],
                        'mask' => '99.99.9999',
                    ])->label(false) ?>
                </div>
                <div class="btn-group" role="group">
                    <?= Html::button('⟶', ['class'=>'btn btn-md btn-default disabled','id' => '#']); ?>
<!--                    --><?//= Html::button('→', ['class'=>'btn btn-md btn-default disabled','id' => '#']); ?>
                </div>
                <div class="btn-group" role="group">
                    <?= $form->field($model, 'datend')->widget('yii\widgets\MaskedInput', [
                        'options' => [
                            'id' => 'datend',
//                            'autofocus' => true,
//                            'value' => '01.10.2019',
                            'placeholder' => 'конечная дата',
                            'style' => 'border-radius:0 3px 3px 0',
                        ],
                        'mask' => '99.99.9999',
                    ])->label(false) ?>
                </div>

            </div>
            <?= $form->field($model, 'search')->textInput([
                'id' => 'search_corr',//main.css//search.js
                'placeholder' => 'Введите данные для поиска'
            ])->label(false) ?>
            <?= Html::button('X', ['class'=>'btn btn-md btn-danger','id' => 'reset_corr']); ?>

            <?= $form->field($model_id,'id')->hiddenInput([
                'id' => 'id_hidden_corr'
            ])->label(false); ?>
            <?= $form->field($model,'id_child')->hiddenInput([
                'id' => 'id_child_corr'
            ])->label(false); ?>
            <?= $form->field($model,'reasonnew')->hiddenInput([
                'id' => 'id_reasonnew'
            ])->label(false); ?>
            <?php $form = ActiveForm::end()?>
            </div>
        </div>
    </div>


    <div class="rep_boss_down" id="table_sotrudniki">
        <?= $this->render($table,compact(
            'model',
            'array'
            )) ?>
    </div>


</div>

<?php
$script = <<<JS
$(function(){

$('#reset_corr').on('click',function(){
    $('#search_corr,#datbegin,#datend,#medotvod').val('').keyup();
        return false;
});

})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>