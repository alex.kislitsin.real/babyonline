<?php
use yii\bootstrap\Modal;

Modal::begin([
    'id' => 'modal_table2_corr',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">55555555555555555</div>
</div>';
Modal::end();
?>

<div class="my_table my_table2 not_selected_text_on_block" id="table_corr2">
    <table class="table-striped table-bordered table-hover" id="mytable">
        <tbody>
        <?php
        $i = 1;
        foreach($array as $q){
            echo '<tr>
                    <td style="width: 3%">'.$i.'</td>
                    <td id="id_n">'.trim($q['name']).'</td>
                    <td>'.trim($q['datenotgo']).'</td>
                    <td>'.trim($q['prichina']).'</td>
                </tr>';
            $i++;
        }
        ?>
        </tbody>
    </table>
</div>
