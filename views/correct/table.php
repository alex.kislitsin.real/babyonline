<?php
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);

Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, проверьте правильность введенных дат и попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

Modal::begin([
    'id' => 'modal_info_corr',
    'header' => '<h4 style="padding-left: 10px" id="name_corr_not_otmetka"></h3>',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert" id="message_not_otmetka_corr"></div>
</div>';
Modal::end();
?>


<?php
Modal::begin([
    'id' => 'modal_get_reason_corr',
    'header' => '<h4 style="padding-left: 10px" id="name_corr_not_otmetka1"></h3>
    <div id="meesage_success_change_correct" hidden="hidden" class="alert alert-success not_selected_text_on_block" role="alert">Изменения прошли успешно!</div>',
    'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_cancel_corr">Отмена</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_reason_corr">Изменить причину</button>',
//    'size' => Modal::SIZE_SMALL,
]);
?>
    <div id="id_modal_corr">
        <?= $this->render('modal_corr',compact(
            'array'
        )) ?>
    </div>
<?php Modal::end(); ?>


<div class="my_table my_table2 not_selected_text_on_block" id="table_corr">
    <table class="table-striped table-bordered table-hover" id="mytable">
        <tbody>

        <?php
        $i = 1;
        foreach($array as $q){

            echo '<tr data-id_child="'.$q['id'].'" data-name="'.trim($q['name']).'">
                <td style="width: 3%">'.$i.'</td>
                <td style="width: 2%" id="tr_start_deti">'.trim($q['start']).'</td>
                <td id="id_n">'.trim($q['name']).'</td>
            </tr>';
            $i++;
        }

        ?>

        </tbody>
    </table>
</div>

<?php
$script = <<<JS
$(function(){

    $('#but_save_reason_corr').on('click',function(){
        console.log('but_save_reason_corr');
        var vr = $(".rrrrr li").data('key');
        $('#id_reasonnew').val(vr);
        $('#id_hidden_corr').val(5);
//        console.log(vr+' vr');
//        console.log(id_child+' id_child');
        var testform = $('#id_form_corr');
        var form = $('#id_form_corr444,#id_form_corr').serializeArray();
//        console.log(form);return;
        $("#id_modal_corr").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
//        $('.modal').modal('hide');
        $.ajax({
            type : testform.attr('method'),
            url : testform.attr('action'),
            data : form
        }).done(function(response) {
        $("*").LoadingOverlay("hide");
            if(response==400){
                $('#modal_error').modal('show');
            }/*else if(response==700){
                $('.modal').modal('hide');
            }*/else{


                $('#id_modal_corr').html(response);
                $('#meesage_success_change_correct').fadeIn(300).show();

                $('#but_save_reason_corr').fadeOut(300).hide();
                $("*").LoadingOverlay("hide");
//                $('.rrrrr0 li').addClass('disabled');
//                $('#modal_get_reason_corr').modal('show');
//                console.log(response);
            }

        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
        return false;
    });

/*$('#reset_corr').on('click',function(){
    $('#search_corr').empty();
    return false;
//        alert('555');
});*/

})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>