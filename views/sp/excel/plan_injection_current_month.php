<?php
use yii\helpers\ArrayHelper;



$height_row_st = 32;
$style_border_bottom = array(
    'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
//    'fill' => array(
//        'type' => PHPExcel_Style_Fill::FILL_SOLID,
//        'startcolor' => array(
//            'argb' => 'FFFF0000',
//        ),
//    ),
);
$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);
$allign_center_all = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);
$allign_up_right = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_center_left = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);
$allign_up_center = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(14);
$sheet->getDefaultStyle()->getFont()->setName('Arial');
$sheet->getSheetView()->setZoomScale(70);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

$sheet->getPageSetup()->setFitToWidth(1);



//$sheet->setBreak('J1',PHPExcel_Worksheet::BREAK_COLUMN);
//$sheet->setBreak('B100',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.5);
$sheet->getPageMargins()->setBottom(0.5);
$sheet->getPageMargins()->setLeft(0.7);
$sheet->getPageMargins()->setRight(0.5);

$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3,3);

//$sheet->getColumnDimensionByColumn("A")->setAutoSize(true);


$sheet->getColumnDimension('A')->setWidth(5);//пусто
$sheet->getColumnDimension('B')->setWidth(4);//п/п
$sheet->getColumnDimension('C')->setWidth(25);//фио
$sheet->getColumnDimension('D')->setWidth(11);//др
$sheet->getColumnDimension('E')->setWidth(8);//возраст
$sheet->getColumnDimension('F')->setWidth(8);//группа
$sheet->getColumnDimension('G')->setWidth(12);//прививка
$sheet->getColumnDimension('H')->setWidth(11);//дата прививки
$sheet->getColumnDimension('I')->setWidth(30);//комент

$sheet->getRowDimension("1")->setRowHeight(50);
$sheet->getRowDimension("2")->setRowHeight($height_row_st);
$sheet->getRowDimension("3")->setRowHeight(45);

$sheet->getStyle("B{$line}:I{$line}")->applyFromArray($allign_center_all);
$month = $_monthsList[(date('n'))].' '.date('Y').' г.';

$line = 1;

$sheet->setCellValue("B{$line}", 'План прививок на '.$_monthsList[(date('n'))].' '.date('Y').' г.');
$sheet->mergeCells("B{$line}:I{$line}");
$sheet->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("B{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("B{$line}")->getFont()->setBold(true);
$sheet->getStyle("B{$line}")->getFont()->setSize(20);
$line++;
$line++;
$sheet->getStyle("B{$line}:I{$line}")->getFont()->setBold(true);
$sheet->getStyle("B{$line}:I{$line}")->applyFromArray($style_border_all);
$sheet->setCellValue("B{$line}", 'п/п');
$sheet->setCellValue("C{$line}", 'Фамилия Имя');
$sheet->setCellValue("D{$line}", 'Дата рождения');
$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", 'Возраст');
$sheet->setCellValue("F{$line}", '№ группы');
$sheet->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", 'Прививка по плану');
$sheet->getStyle("G{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("H{$line}", 'Дата по плану');
$sheet->getStyle("H{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("I{$line}", 'Примечание');

$iii = 1;
$number_row = 1;
$id_child_save = null;
    foreach($array as $abc){
        $po_planu = trim($abc['RV']).' '.trim($abc['name_code']);
        strpos($abc['next_date'],'1900-01-01') !== false ? $next_date = '' : $next_date = Yii::$app->formatter->asDate($abc['next_date']);
        $rozd = Yii::$app->formatter->asDate($abc['rozd']);
        if (strlen(trim($abc['medotvod_date']))>0){
            $med_date = Yii::$app->formatter->asDate(trim($abc['medotvod_date']));
        }else{
            $med_date = '';
        }
        if (strlen($med_date)>0){
            $text_med = 'медотвод до ';
        }else{
            $text_med = '';
        }
        $coment = $text_med.' '.$med_date.' '.trim($abc['coment']).' '.trim($abc['medotvod']);

        if ($id_child_save != null){
            if ($id_child_save == $abc['id_child']){
                $name = '';
                $rozd = '';
                $ii = '';
                $group = '';
                $vozrast_year = '';
            }else{
                $id_child_save = $abc['id_child'];
                $name = trim($abc['name']);
                $ii = $iii;
                $group = $abc['id_gruppa'];
                $vozrast_year = $abc['vozrast_year'];
            }
        }else{
            $id_child_save = $abc['id_child'];
            $name = trim($abc['name']);
            $ii = $iii;
            $group = $abc['id_gruppa'];
            $vozrast_year = $abc['vozrast_year'];
        }

        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:I{$line}")->applyFromArray($style_border_all);
        $sheet->getStyle("C{$line}")->applyFromArray($allign_center_left);

        $sheet->setCellValue("B{$line}", $ii);
        $sheet->setCellValue("C{$line}", $name);
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->getStyle("I{$line}")->getAlignment()->setWrapText(true);
        $sheet->setCellValue("D{$line}", $rozd);
        $sheet->setCellValue("E{$line}", $vozrast_year);
        $sheet->setCellValue("F{$line}", $group);
        $sheet->setCellValue("G{$line}", $po_planu);
        $sheet->setCellValue("H{$line}", $next_date);
        $sheet->setCellValue("I{$line}", $coment);
//        $iii++;
        if (strlen($name)>0)$iii++;
        $number_row++;
}

$sheet->getPageSetup()->setPrintArea("B1:I{$line}");
$sheet->getPageSetup()->setFitToHeight(0);

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=План прививок на ".$month.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;