<?php
//$model_id = new \app\models\Id();

use yii\bootstrap\Modal;

$_monthsList = array(
    "1"=>"Январь","2"=>"Февраль","3"=>"Март",
    "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
    "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
    "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");

//debug($data);
$item_month = $_monthsList[date("n", strtotime($data))];//текущий месяц
$item_year_panel = date("Y", strtotime($data));//текущий год

$w1 = date_create($data);
date_modify($w1, '-1 month');
$w1 = date_format($w1,'Y-m-d');
$before_item_month = $_monthsList[date("n", strtotime($w1))];//прошлый месяц

$w2 = date_create($data);
date_modify($w2, '1 month');
//debug($w2);
$w2 = date_format($w2,'Y-m-d');
//debug($w2);
$after_item_month = $_monthsList[date("n", strtotime($w2))];//след месяц
//debug($after_item_month);

//$data = date("Y-m-d");
//$data = '2019-04-23';
//$item_gruppa = 'Звёздочки';
//$item_gruppa = 'Затейники';
//$item_gruppa = 'Фантазеры';

//$iteration = 1;
//while($iteration<=37){
//    ${'id'.$iteration} = '';
////    debug(${'id'.$iteration});
//    $iteration++;
//}

Modal::begin([
    'id' => 'modal_cal_not_zachislen',
//    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert" style="text-align: center"><h4 class="not_selected_text_on_block">Вы не зачислены в след. месяце !</h4></div>
</div>';
Modal::end();

$item_date = date("Y-m-01");
$last_day_of_item_date = date('t', strtotime($item_date));
?>

    <div class="panel_sp_info_dropdown_group">
        <div class="conteiner_for_dropdown_and_name_child">
            <div class="conteiner_for_dropdown_and_name_child1">
                <?= $this->render('form_item_date_on_calendar',compact(
                    'model_d_antidate',
                    'model_item_date',
                    'array_disabled_dates',
                    'model_id')) ?>
            </div>
            <div class="conteiner_for_dropdown_and_name_child2">
                <?= $this->render('form_item_group_on_calendar_sp',compact(
                    'model_sp_begin',
                    'model_group',
                    'array_gruppa',
                    'model_id')) ?>
            </div>
        </div>

    </div>
    <div class="block_calendar_sp_content2">
        <div class="block_calendar_sp_content">
            <div class="container_calendar">
                <div class="container_calendar1">

                    <div class="container_calendar1_1 item_child_sp"></div>
                    <div class="container_calendar1_3">
                        <!--                    <div class="container_calendar1_3_1"></div>-->
                        <div class="container_calendar1_3_1 button_next_month" data-clickmonth="0" data-idd=""></div>
                        <div class="container_calendar1_3_2" data-in="" data-out="" data-yin="" data-yout="">
                            <?= $item_month.' '.$item_year_panel ?>
                        </div>
                        <!--                    <div class="container_calendar1_3_3"></div>-->
                        <div class="container_calendar1_3_3 button_next_month" data-clickmonth="1" data-idd=""></div>
                    </div>
                    <div class="container_calendar1_2" id="table_cal_sp">
                        <?= $this->render('_only_cal',compact(
                            'model_d_antidate',
                            'data',
                            'data_item','model_d_date')) ?>
                    </div>
                    <!--                <div class="container_calendar1_3">333</div>-->
                    <div class="container_calendar1_4">
                        <div class="conteiner_for_dropdown_and_name_child">
                            <div class="conteiner_for_dropdown_and_name_child1">
                                <div title="снять весь месяц с 1 числа" id="sp_button_gogo_one_child_all_month" style="height: 110px;width: 100px;background-color: transparent;cursor: pointer"></div>
                            </div>
                            <div class="conteiner_for_dropdown_and_name_child2">
                                <div title="поставить весь месяц с 1 числа" id="sp_button_gogo_one_child_all_month_cancel" style="height: 110px;width: 100px;background-color: transparent;cursor: pointer;margin-left: 40px"></div>
                            </div>
                            <div class="conteiner_for_dropdown_and_name_child2">
                                <div title="снять остаток месяца" id="sp_button_gogo_one_child_ostatok_month" style="height: 110px;width: 100px;background-color: transparent;cursor: pointer;margin-left: 60px"></div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

<?php
$script = <<<JS

$(function(){


    $('.button_next_month').on('click',function(){

        var val_in = $('.container_calendar1_3_2').attr('data-in');
        var val_out = $('.container_calendar1_3_2').attr('data-out');
        var yin = $('.container_calendar1_3_2').attr('data-yin');
        var yout = $('.container_calendar1_3_2').attr('data-yout');

//        console.log('in: '+val_in+', yin: '+yin+', out: '+val_out+', yout: '+yout);return

        var fio = $('.item_child_sp').text();
        if(fio.length > 0){
        var leftright = $(this).data('clickmonth');//console.log(leftright);return
        var datamonth = $('.table_sp').data('datamonth');

        var idd = $(this).data('idd');
        var id = $("#id_form_datepicker").val('12');
        var form = $('#form_sp_item_date').serializeArray();
        form.push({name:'id_child',value:idd});
        form.push({name:'leftright',value:leftright});
        form.push({name:'datamonth',value:datamonth});

        var year_item = new Date(datamonth).getFullYear();
        var month_item = (new Date(datamonth).getMonth())+1;

        if(leftright == 0){//left
            if(year_item == yin && val_in == month_item){
                $('#modal_cal_not_zachislen').modal('show');
                return;
            }
        }
        if(leftright == 1){//right
            if(year_item == yout && val_out == month_item){
                $('#modal_cal_not_zachislen').modal('show');
                return;
            }
        }

//        console.log(year_item);
//        console.log(month_item);
        console.log(form);
//        return;
        var arr = $('#form_sp_item_date');
        $("#table_cal_sp").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
//        return;
        $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
//                console.log(response);

                $('#table_cal_sp').html(response);
                var item_month = $('.table_sp').data('item_month');
                var item_year = $('.table_sp').data('item_year');
                $('.container_calendar1_3_2').text(item_month+' '+item_year);

            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not leftright');
//                alert("Ошибка");
            });

        return false;
        }else{
            alert('Чтобы переключить месяц - выберите ребёнка, нажав на иконку календарика')
        }
    });

    $('#sp_button_gogo_one_child_all_month').click(function(){
        if(confirm("снять с питания на весь месяц ?")){
            var id_ch = $('#id_table_sp_veruyu').data('id_childer');
        if(id_ch > 0){
                var datamonth = $('.table_sp').data('datamonth');
//                console.log(datamonth);return;

                var arr = $("#form_sp_item_date,#form_sp_item_group");
                var form = $('#form_sp_item_group').serializeArray();
                form.push({name:'id_child',value:id_ch});
                form.push({name:'Item_date[item_date]',value:datamonth});
                form.push({name:'Id[id]',value:14});
//                console.log(form);return;
                $(".block_content").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                $.ajax({
                    type : arr.attr("method"),
                    url : arr.attr("action"),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");

                        $("#id_form_datepicker").val("11");
                        var form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                        form.push({name:'id_child',value:id_ch});
                        $("#only_spiski").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
                        $.ajax({
                            type : arr.attr('method'),
                            url : arr.attr('action'),
                            data : form
                        }).done(function(response2) {
                            $("*").LoadingOverlay("hide");
                            console.log('yes');
                            $('#only_spiski').html(response2);
                            $('#table_cal_sp').html(response);
                        }).fail(function() {
                            $("*").LoadingOverlay("hide");
                            console.log('not');
                            alert("Ошибка");
                        });


                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    console.log("not");
                    alert("Ошибка");
                });
        }
        }

        return false;
    });






    $('#sp_button_gogo_one_child_all_month_cancel').click(function(){
        if(confirm("поставить на питание на весь месяц ?")){
            var id_ch = $('#id_table_sp_veruyu').data('id_childer');
        if(id_ch > 0){
                var datamonth = $('.table_sp').data('datamonth');
//                console.log(datamonth);return;

                var arr = $("#form_sp_item_date,#form_sp_item_group");
                var form = $('#form_sp_item_group').serializeArray();
                form.push({name:'id_child',value:id_ch});
                form.push({name:'Item_date[item_date]',value:datamonth});
                form.push({name:'Id[id]',value:15});
//                console.log(form);return;
                $(".block_content").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                $.ajax({
                    type : arr.attr("method"),
                    url : arr.attr("action"),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");

                        $("#id_form_datepicker").val("11");
                        var form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                        form.push({name:'id_child',value:id_ch});
                        $("#only_spiski").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
                        $.ajax({
                            type : arr.attr('method'),
                            url : arr.attr('action'),
                            data : form
                        }).done(function(response2) {
                            $("*").LoadingOverlay("hide");
                            console.log('yes');
                            $('#only_spiski').html(response2);
                            $('#table_cal_sp').html(response);
                        }).fail(function() {
                            $("*").LoadingOverlay("hide");
                            console.log('not');
                            alert("Ошибка");
                        });


                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    console.log("not");
                    alert("Ошибка");
                });
        }
        }

        return false;
    });








    $('#sp_button_gogo_one_child_ostatok_month').click(function(){
        if(confirm("снять с питания остаток месяца ?")){
            var id_ch = $('#id_table_sp_veruyu').data('id_childer');
        if(id_ch > 0){
                var arr = $("#form_sp_item_date,#form_sp_item_group");
                var form = $('#form_sp_item_group').serializeArray();
                form.push({name:'id_child',value:id_ch});
                form.push({name:'Item_date[item_date]',value:$("#dp_sp").val()});
                form.push({name:'Id[id]',value:16});
//                console.log(form);return;
                $(".block_content").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                $.ajax({
                    type : arr.attr("method"),
                    url : arr.attr("action"),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");

                        $("#id_form_datepicker").val("11");
                        var form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                        form.push({name:'id_child',value:id_ch});
                        $("#only_spiski").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
                        $.ajax({
                            type : arr.attr('method'),
                            url : arr.attr('action'),
                            data : form
                        }).done(function(response2) {
                            $("*").LoadingOverlay("hide");
                            console.log('yes');
                            $('#only_spiski').html(response2);
                            $('#table_cal_sp').html(response);
                        }).fail(function() {
                            $("*").LoadingOverlay("hide");
                            console.log('not');
                            alert("Ошибка");
                        });


                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    console.log("not");
                    alert("Ошибка");
                });
        }
        }

        return false;
    });
})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>