<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
$model = array();
$model_id = new \app\models\Id();
?>
<?php $form = ActiveForm::begin([
    'id' => 'hot_form_settings',
    'action' => ['reports/settings'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_INLINE,
    'enableAjaxValidation' => false,
])?>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'id_settings_ajax',
])->label(false); ?>
<?php ActiveForm::end(); ?>

<?php Modal::begin([
    'id' => 'modal_hot_settings',
    'header' => '<h4 style="padding-left: 10px">Настройки</h4>',
    'size' => Modal::SIZE_LARGE,
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'footer' => '<button class="btn btn-default btn-md" id="but_message_save_settings" style="color: #008000;border: 0px;font-size: 16px;font-weight: bold" disabled="disabled">Данные успешно сохранены !</button>
    <button type="submit" class="btn btn-success btn-md" id="but_save_settings">Сохранить изменения</button>',
]);?>

<div id="id_render_modal_settings">
<?= $this->render('settings_table',compact('model')) ?>
</div>

<?php Modal::end();?>