<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAssetRod;
use yii\helpers\Html;

AppAssetRod::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="rod_boss">

    <div class="rod_left_block"></div>
    <div class="rod_center_block">
        <div class="rod_center_block_1">
            <?= Html::a('Выйти', ['site/logout'], [
//                'href' => 'site/logout',
                'onclick' => 'return confirm("Вы хотите выйти из системы ?");',
                'class'=>'btn btn-light btn-md',
                'id'=>'exit_off',
                'style' => [
                    'height' => '100%',
                    'color' => 'red'
//                    'border' => '1px solid rgb(255, 219, 210)'
                ]
            ]) ?>
        </div>
        <div class="rod_center_block_2"></div>
    </div>
    <div class="rod_right_block"></div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

