<?php

use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <?php /*Html::csrfMetaTags() */?>
    <title><?= $this->title ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="login_boss">
    <div class="login_bok">
        <?/*= Html::a('На главную', ['start/system'],[
            'class'=>'btn btn-default btn-md',
//            'id'=>'but_start_main',
//            'style' => 'height:100%'
        ]) */?>
        babyonline3000@gmail.com
    </div>
    <div class="login_center">
        <div class="login_center_1 not_selected_text_on_block">
            <h1 id="id_h1_label">
                <span style="color: #cd0a0a">B</span><span style="color: #0073ea">a</span><span style="color: #00aa00">b</span><span style="color: #6f42c1">y</span> <span style="color: #00aa00">О</span><span style="color: #fece2f">н</span><span style="color: #0074c7">л</span><span style="color: #cd0a0a">а</span><span style="color: #a1abff">й</span><span style="color: #e83e8c">н</span>  </h1>
        </div>
        <div class="login_center_2">
            <?=$content?>
        </div>
        <div class="login_center_3">
        </div>
    </div>
    <div class="login_bok">

    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


