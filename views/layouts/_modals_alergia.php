<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
$model = array();
?>
<?php $form = ActiveForm::begin([
    'id' => 'hot_form_alergia',
    'action' => ['reports/alergia'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_INLINE,
    'enableAjaxValidation' => false,
])?>
<?php ActiveForm::end(); ?>

<?php Modal::begin([
    'id' => 'modal_hot_alergia',
    'header' => '<h4 style="padding-left: 10px">Алергики</h4>',
    'size' => Modal::SIZE_LARGE,
]);?>

<div id="id_render_modal_alergia">
<?= $this->render('alergia_table',compact('model')) ?>
</div>

<?php Modal::end();?>