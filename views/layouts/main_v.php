<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('_modals') ?>

<div class="wrap">

    <div class="block_shapka">

        <div class="block_shapka_left">
            <div style="width: 250px;margin-right: 10px" id="baby_label1"><a href="index.php?r=start/system"><span style="color: #cd0a0a">B</span><span style="color: #0073ea">a</span><span style="color: #00aa00">b</span><span style="color: #6f42c1">y</span> <span style="color: #00aa00">О</span><span style="color: #fece2f">н</span><span style="color: #0074c7">л</span><span style="color: #cd0a0a">а</span><span style="color: #a1abff">й</span><span style="color: #e83e8c">н</span></a></div>
        </div>
        <div class="block_shapka_center">

        </div>
        <div class="block_shapka_right_l">
            <div id="anim_loader" style="height: 100%; width: 40px"></div>
        </div>
        <div class="block_shapka_right_r">
            <?= Html::a('Выйти', ['site/logout'], [
//                'href' => 'site/logout',
                'onclick' => 'return confirm("Вы хотите выйти из системы ?");',
                'class'=>'btn btn-light btn-md',
                'id'=>'exit_off',
                'style' => [
                    'height' => '100%',
                    'color' => 'red'
//                    'border' => '1px solid rgb(255, 219, 210)'
                ]
            ]) ?>
        </div>

    </div>
    <div class="block2">
        <div class="block_sidebar">
            <?= $this->render('_sidebar') ?>
        </div>
        <div class="block_content">
            <?= $content ?>
        </div>
    </div>
<!--    <div class="block block_footer"></div>-->

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>



