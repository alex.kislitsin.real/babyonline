<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.01.20
 * Time: 12:24
 */
//debug($array);
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
$width = '8%';
//$year = $model_y->name;
//$month = $model_m->name;
//$array = array(1,2,3,4,5,6,7,8,9);
?>
<div class="my_table not_selected_text_on_block" style="height: 5vh;background-color: #eeeeee" id="antro_table2up">
    <table>
        <tr>
            <td class="not_item" style="width: 5%;text-align: center">№</td>
            <td class="not_item">Название группы</td>
            <td class="not_item" style="width: <?= $width ?>">список</td>
            <td class="not_item" style="width: <?= $width ?>">факт</td>
            <td id="td_antro_mebel1" class="not_item" style="width: <?= $width ?>">00-A</td>
            <td id="td_antro_mebel2" class="not_item" style="width: <?= $width ?>">0-Б</td>
            <td id="td_antro_mebel3" class="not_item" style="width: <?= $width ?>">1-В</td>
            <td id="td_antro_mebel4" class="not_item" style="width: <?= $width ?>">2-Г</td>
            <td id="td_antro_mebel5" class="not_item" style="width: <?= $width ?>">3-Д</td>
            <td id="td_antro_mebel6" class="not_item" style="width: <?= $width ?>">4</td>
        </tr>
    </table>
</div>
<div class="my_table my_table2 not_selected_text_on_block" id="antro_table2">
    <table class="table-striped table-bordered">

        <?php

        foreach($array2 as $q){
            trim($q['namegr'])=='Итого' ? $ii = '' : $ii = $q['id_gruppa'];
            if((int)$q['countgr'] != (int)$q['zzz']){
                $style = '; color: #EE100F';
            }else{
                $style = '';
            }

            intval($q['y1'])==0 && intval($q['id_gruppa'])>0 ? $y1 = '' : $y1 = $q['y1'];
            intval($q['y2'])==0 && intval($q['id_gruppa'])>0 ? $y2 = '' : $y2 = $q['y2'];
            intval($q['y3'])==0 && intval($q['id_gruppa'])>0 ? $y3 = '' : $y3 = $q['y3'];
            intval($q['y4'])==0 && intval($q['id_gruppa'])>0 ? $y4 = '' : $y4 = $q['y4'];
            intval($q['y5'])==0 && intval($q['id_gruppa'])>0 ? $y5 = '' : $y5 = $q['y5'];
            intval($q['y6'])==0 && intval($q['id_gruppa'])>0 ? $y6 = '' : $y6 = $q['y6'];

                echo '<tr data-id_gruppa="'.$ii.'">
                <td style="width: 5%">'.$ii.'</td>
                <td id="id_n">'.trim($q['namegr']).'</td>
                <td style="width: '.$width.'">'.$q['countgr'].'</td>
                <td style="width: '.$width.$style.'">'.$q['zzz'].'</td>
                <td style="width: '.$width.'">'.$y1.'</td>
                <td style="width: '.$width.'">'.$y2.'</td>
                <td style="width: '.$width.'">'.$y3.'</td>
                <td style="width: '.$width.'">'.$y4.'</td>
                <td style="width: '.$width.'">'.$y5.'</td>
                <td style="width: '.$width.'">'.$y6.'</td>
            </tr>';
        }

        ?>

    </table>
</div>
<hr style="margin-top: 4px;margin-bottom: 4px"/>
<div class="my_table my_table2 not_selected_text_on_block">
    <table class="table-bordered" style="background-color: #faffc5">
        <tbody>
        <tr>
            <td style="width: 20%">00-A</td>
            <td style="width: 80%;text-align: left;padding-left: 30px">рост: до 850 мм; высота стола: 340 мм; высота стула: 180 мм.</td>
        </tr>
        <tr>
            <td style="width: 20%">0-Б</td>
            <td style="width: 80%;text-align: left;padding-left: 30px">рост: 850-1000 мм; высота стола: 400 мм; высота стула: 220 мм.</td>
        </tr>
        <tr>
            <td style="width: 20%">1-В</td>
            <td style="width: 80%;text-align: left;padding-left: 30px">рост: 1000-1150 мм; высота стола: 460 мм; высота стула: 260 мм.</td>
        </tr>
        <tr>
            <td style="width: 20%">2-Г</td>
            <td style="width: 80%;text-align: left;padding-left: 30px">рост: 1150-1300 мм; высота стола: 520 мм; высота стула: 300 мм.</td>
        </tr>
        <tr>
            <td style="width: 20%">3-Д</td>
            <td style="width: 80%;text-align: left;padding-left: 30px">рост: 1300-1450 мм; высота стола: 580 мм; высота стула: 340 мм.</td>
        </tr>
        <tr>
            <td style="width: 20%">4</td>
            <td style="width: 80%;text-align: left;padding-left: 30px">рост: 1450-1600 мм; высота стола: 640 мм; высота стула: 380 мм.</td>
        </tr>

        </tbody>
    </table>
</div>