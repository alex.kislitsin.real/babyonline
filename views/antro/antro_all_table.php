<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 07.01.20
 * Time: 20:58
 */
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

?>
<div class="block_listview_sp_boss">
    <div class="block_listview_sp" id="antro_left_table">
        <?= $this->render('antro_table',compact('array','model_antro','model_id')) ?>
    </div>
    <div class="block_listview_sp" style="overflow: auto">
        <?= $this->render('antro_table_mebel',compact('array2')) ?>
    </div>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'form_modal_antro_mebel2',
//    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<?= $form->field($model_id, 'id')->hiddenInput([
    'id' => 'id_hidden_modal_antro',
])->label(false); ?>
<?= $form->field($model_antro, 'id_child')->hiddenInput([
    'id' => 'id_child_modal_antro',
])->label(false); ?>
<?= $form->field($model_antro, 'dat')->hiddenInput([
    'id' => 'dat_modal_antro',
])->label(false); ?>

<?php ActiveForm::end(); ?>
<?php
Modal::begin([
    'id' => 'modal_antro_mebel',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="header_modal_antro_mebel"></h4>',
    'size' => Modal::SIZE_DEFAULT,
    'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_antro_mebel">Удалить замеры</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_antro_mebel">Сохранить изменения</button>',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form_modal_antro_mebel',
    'method' => 'POST',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'action' => ['antro/antro'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-4',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-8',
        ],
    ],
]); ?>

<?= $form->field($model_antro, 'name')->textInput([
    'id' => 'name_modal_antro',
    'placeholder' => 'Фамилия Имя',
    'readonly'=> true
]) ?>

<?= $form->field($model_antro,'rost')->textInput([
    'id' => 'rost_modal_antro',
    'type' => 'number',
    'placeholder' => 'Рост'
]) ?>
<?= $form->field($model_antro, 'ves')->textInput([
    'id' => 'ves_modal_antro',
    'type' => 'number',
    'placeholder' => 'Вес'
]) ?>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>

<?php
$script = <<<JS
$(function(){
    $('#but_save_antro_mebel').on('click',function(){

    var s = $("#antro_table1").scrollTop();

    $('.modal').modal('hide');
    var form = $('#form_modal_antro_mebel,#form_modal_antro_mebel2,#form_antro_panel_button').serializeArray();
    var arr = $('#form_modal_antro_mebel');
    console.log(form);
//    return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#renderAjax_antro_table_boss').html(response);
                $("#antro_table1").scrollTop(s);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
    });

    $('#but_del_antro_mebel').on('click',function(){

    var s = $("#antro_table1").scrollTop();

    $('#id_hidden_modal_antro').val(4);
    $('.modal').modal('hide');
    var form = $('#form_modal_antro_mebel,#form_modal_antro_mebel2,#form_antro_panel_button').serializeArray();
//    form.push({name:'Gruppa[name]',value:$('#drop_group_antro').val()});
//    form.push({name:'Year[name]',value:$('#drop_antro_year').val()});
//    form.push({name:'Month[name]',value:$('#drop_antro_month').val()});
    var arr = $('#form_modal_antro_mebel');
    console.log(form);
//    return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#renderAjax_antro_table_boss').html(response);
                $("#antro_table1").scrollTop(s);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
    });


$('#antro_table2 tr').on('click',function(){
//    console.log(000);
//    $('#form_modal_antro_mebel,#form_modal_antro_mebel2').trigger('reset');
    var id_gruppa = $(this).data('id_gruppa');
//    console.log(id_gruppa);//return;
    if(id_gruppa.length < 1)return;
    $('#drop_group_antro').val(id_gruppa);

    var form = $('#form_antro_panel_button').serializeArray();
    form.push({name: 'Id[id]',value: 3});
    var arr = $('#form_antro_panel_button');
//    console.log(form);
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#antro_left_table').html(response);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
});



})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>