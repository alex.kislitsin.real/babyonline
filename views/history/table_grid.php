<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);


Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();
?>

<div class="my_table my_table2 not_selected_text_on_block"></div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'showHeader' => false,
    'summary' => false,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover',
        'id' => 'mytable_h',
    ],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'contentOptions' => [
                'width' => '3%',
            ],
        ],
        [
            'attribute' => 'dateH',
            'format' => 'datetime',
            'contentOptions' => [
                'width' => '13%',
            ],
        ],
        [
            'attribute' => 'kto',
            /*'contentOptions' => [
                'width' => '20%',
            ],*/
        ],
        [
            'attribute' => 'kogo',
            /*'contentOptions' => [
                'width' => '20%',
            ],*/
        ],
        [
            'attribute' => 'text_content',
            'contentOptions' => [
                'width' => '13%',
            ],
        ],
        [
            'attribute' => 'prichina',
            'contentOptions' => [
                'width' => '19%',
            ],
            'content'=>function($data){
                    $pri = trim($data['prichina']);
                    $all = strlen($pri);
                    if ($all>70){
                        $value = mb_substr($pri,0, 30).'...';
                    }else{
                        $value = trim($data['prichina']);
                    }
                    return $value;
                }
        ],
        [
            'attribute' => 'nadatu',
            'contentOptions' => [
                'width' => '7%',
            ],
            'format'=>'date',
        ],
    ],
]); ?>


<!--<div class="my_table my_table2 not_selected_text_on_block" id="#">-->
<!--    --><?php
//    $i = 1;
//    foreach($array as $q){
//        /*if (strpos($q['in'],'1900-01-01') !== false){
//            $t = '';
//        }else{
//            $t = Yii::$app->formatter->asDate(trim($q['in']));
//        }
//        if (!empty($q['out'])||!is_null($q['out'])){
//            $t2 = Yii::$app->formatter->asDate(trim($q['out']));
//            $classs = 'id="row_sotr_all"';
//        }else{
//            $t2 = '';
//            $classs = '';
//        }
//        if (strpos($q['rozd'],'1900-01-01') !== false){
//            $u = '';
//        }else{
//            $u = Yii::$app->formatter->asDate(trim($q['rozd']));
//        }*/
//
//
//        echo '<table class="table-striped table-bordered table-hover" id="mytable_h">
//            <tbody>
//            <tr '.$classs.'>
//                <td style="width: 3%">'.$i.'</td>
//                <td id="id_n">'.trim($q['dateH']).'</td>
//                <td style="width: 15%">'.trim($q['kto']).'</td>
//                <td style="width: 15%">'.trim($q['kogo']).'</td>
//                <td style="width: 15%">'.trim($q['text_content']).'</td>
//                <td style="width: 15%">'.trim($q['prichina']).'</td>
//                <td style="width: 15%">'.trim($q['nadatu']).'</td>
//            </tr>
//            </tbody>
//        </table>';
//        $i++;
//    }
//
//    ?>
<!--</div>-->
