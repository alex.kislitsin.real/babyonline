<?php

//use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;

?>

<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <div class="btn-group" role="group" id="b_group_h">
                <div class="btn-group pull_right" role="group">
                    <?= Html::button('400 последних записей', ['class'=>'btn btn-md btn-default active','id' => 'b400h']); ?>
                </div>
                <div class="btn-group" role="group">
                    <?= Html::button('все записи .....', ['class'=>'btn btn-md btn-default','id' => 'ballh']); ?>
                </div>
            </div>
        </div>
        <div class="rep_boss_up_panel_1" data-id7="0">
            <?php $form = ActiveForm::begin([
                'action' => ['history/find'],
                'method' => 'POST',
                'id' => 'id_form_history',
                'enableAjaxValidation' => false,//
            ])?>
            <?=$form->field($model, 'name')
                ->textInput([
                    'action' => ['history/find'],
                    'method' => 'POST',
                    'autofocus' => true,
                    'id' => 'h1',
                    'placeholder' => 'Введите любые данные для поиска',
                ])->label(false); ?>

            <?= $form->field($model_id,'id')->hiddenInput(['id' => 'hidden_id_h',])->label(false); ?>
            <?php $form = ActiveForm::end()?>
        </div>
    </div>
    <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
        <table>
            <tr>
                <td style="width: 3%">№</td>
                <td style="width: 13%">Дата действия</td>
                <td>Кто</td>
                <td>Кого</td>
                <td style="width: 13%">Действие</td>
                <td style="width: 19%">Причина</td>
                <td style="width: 7%">На дату</td>
            </tr>
        </table>
    </div>
    <div class="rep_boss_down" id="table_sotrudniki">
        <?= $this->render('table_grid',compact(
            'dataProvider'
            )) ?>
    </div>
</div>