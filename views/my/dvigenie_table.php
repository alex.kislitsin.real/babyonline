<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
use yii\grid\GridView;

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'summary' => false,
    'id' => 'id_grid_dvig',
    'tableOptions' => [
        'class' => 'table table-bordered table-hover'
    ],
    'rowOptions'=>function ($model_dvig){
            if(trim($model_dvig['name']) == 'Итого'){
                return ['class' => 'success'];
            }
            if(trim($model_dvig['name']) == 'С - вит.'){
                return ['class' => 'danger'];
            }
        },
    'columns' => [
        [
            'attribute' => 'name',
            'label' => 'Группа',
            'contentOptions' => [
                'width' => '20%',
                'style'=>[
//                        'background' => '#11a908',
                ],
            ],
            'headerOptions' => [
                'class' => 'text-center'
            ],
        ],
        //'vsego',
        [
            'attribute' => 'vsego',
            'label' => 'Всего детей',
            'contentOptions' => [
                'width' => $percent,
                'class' => 'text-center',
//                    'style'=>'white-space: normal'
            ],
            'headerOptions' => [
                'class' => 'text-center'
            ],
        ],
        //'stoyat',
        [
            'attribute' => 'stoyat',
            'label' => 'Стоят',
            'contentOptions' => [
                'width' => $percent,
                'class' => 'text-center',
//                    'style'=>'white-space: normal'
            ],
            'headerOptions' => [
                'class' => 'text-center'
            ],
        ],
        //'snyati',
        [
            'attribute' => 'snyati',
            'label' => 'Сняты',
            'contentOptions' => [
                'width' => $percent,
                'class' => 'text-center',
//                    'style'=>'white-space: normal'
            ],
            'headerOptions' => [
                'class' => 'text-center'
            ],
        ],
        //'fakt',
        [
            'attribute' => 'fakt',
            'label' => 'Факт',
            'contentOptions' => [
                'width' => $percent,
                'class' => 'text-center',
                'style'=>[
                    'background' => 'rgba(255, 255, 0, 0.27)',
                ],
            ],
            'headerOptions' => [
                'class' => 'text-center',
                'style'=>[
                    'background' => 'rgba(255, 255, 0, 0.27)',
                ],
            ],
        ],
        //'no_sovsem',
        [
            'attribute' => 'no_sovsem',
            'label' => 'Не пришли',
            'contentOptions' => [
                'width' => $percent,
                'class' => 'text-center',
//                    'style'=>'white-space: normal'
            ],
            'headerOptions' => [
                'class' => 'text-center'
            ],
        ],
        [
            'attribute' => 'surprise',
            'label' => 'Сюрприз',
            'contentOptions' => [
                'width' => $percent,
                'class' => 'text-center',
//                    'style'=>'white-space: normal'
            ],
            'headerOptions' => [
                'class' => 'text-center'
            ],
        ],

    ],



]); ?>