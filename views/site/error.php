<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
$message = 'Произошла ошибка, перезапустите страницу сайта или обратитесь в службу поддержки babyonline3000@gmail.com';
$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="alert alert-danger" style="text-align: center">
        <?= nl2br(Html::encode($message)) ?>

    </div>
    <?= Html::a('Страница регистрации', ['site/login'], [
        'class'=>'btn btn-success btn-md btn-block',
    ]) ?>

</div>
