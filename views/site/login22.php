<?php
/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model app\models\LoginF */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
use kartik\form\ActiveForm;
use yii\helpers\Url;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$this->title = 'Baby Онлайн';
$array_region = ArrayHelper::map($array,'kod_region','region');
$array_region = array_unique($array_region);
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'method' => 'POST',
    'action' => ['site/login'],
    'type' => ActiveForm::TYPE_VERTICAL,

]); ?>
<?= $form->field($model,'region')->dropDownList($array_region,[
    'data_place' => 'Выберите область',
    'prompt' => 'Выберите область',
    'id' => 'id_region',
//    'size' => ActiveForm::SIZE_MEDIUM,
])->label(false) ?>

<?= $form->field($model,'city')->dropDownList($array,[
    'prompt' => 'Выберите город',
    'disabled'=> 'disabled',
    'id' => 'id_city',
])->label(false) ?>

<?= $form->field($model,'sad')->dropDownList($array,[
    'prompt' => 'Выберите сад',
    'disabled'=> 'disabled',
    'id' => 'id_sad'
])->label(false) ?>

<?= $form->field($model, 'snils')->widget('yii\widgets\MaskedInput', [
     'options' => [
         'id' => 'id_snils',
         'disabled'=> 'disabled',
         'placeholder' => 'Введите снилс ребенка',
     ],
     'mask' => '999-999-999 99',
])->label(false) ?>

<?= Html::submitButton('Войти',[
    'class' => 'btn btn-lg btn-success btn-block',
    'disabled'=> 'disabled',
    'id' => 'but_login'
]); ?>

    <div id="name" hidden="hidden"></div>

    <div id="yes_no" hidden="hidden">
    <div class="btn-group btn-group-justified" role="group">
        <div class="btn-group pull_right" role="group">
            <button type="button" class="btn btn-danger" id="not">Отмена</button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-success" id="yes">Да</button>
        </div>
    </div>
    </div>

<?php ActiveForm::end(); ?>

<?php
//$csrf_param = Yii::$app->request->csrfParam;
//$csrf_token = Yii::$app->request->csrfToken;
?>