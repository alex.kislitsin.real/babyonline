<?php
/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model app\models\LoginF */
//use yii\captcha\Captcha;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
use kartik\form\ActiveForm;
use yii\helpers\Url;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$this->title = 'Baby Онлайн';
$array_region = ArrayHelper::map($array,'kod_region','region');
$array_region = array_unique($array_region);
?>

<?php $form = ActiveForm::begin([
    'id' => 'login3333',
    'method' => 'POST',
    'action' => ['site/login'],
    'type' => ActiveForm::TYPE_VERTICAL,
    'formConfig' => ['deviceSize' => ActiveForm::SIZE_LARGE]
//    'enableCsrfValidation' => false

]); ?>

<?= $form->field($model,'region')->dropDownList($array_region,[
    'data_place' => 'Выберите область',
    'prompt' => 'Выберите область',
    'id' => 'id_region',
    'custom' => true,
    'class' => 'custom-select-lg'
//    'size' => ActiveForm::SIZE_LARGE,
])->label(false) ?>

<?= $form->field($model,'city')->dropDownList($array,[
    'prompt' => 'Выберите город',
    'disabled'=> 'disabled',
    'id' => 'id_city',
])->label(false) ?>

<?= $form->field($model,'sad')->dropDownList($array,[
    'prompt' => 'Выберите сад',
    'disabled'=> 'disabled',
    'id' => 'id_sad'
])->label(false) ?>

<?= $form->field($model, 'snils')->widget('yii\widgets\MaskedInput', [
     'options' => [
         'id' => 'id_snils',
         'disabled'=> 'disabled',
         'placeholder' => 'Введите снилс ребенка',
     ],
     'mask' => '999-999-999 99',
])->label(false) ?>
<?= $form->field($model, 'polis')->widget('yii\widgets\MaskedInput', [
     'options' => [
         'id' => 'id_polis',
         'disabled'=> 'disabled',
         'placeholder' => 'Введите полис ребенка',
     ],
     'mask' => '9999 9999 9999 9999',
])->label(false) ?>

<?= $form->field($model, 'captcha')->widget(Captcha::className([
    'id' => 'id_captcha'
])) ?>

<?= Html::button('Войти',[
    'class' => 'btn btn-lg btn-success btn-block',
    'disabled'=> 'disabled',
    'id' => 'but_login'
]); ?>

    <div id="name" hidden="hidden"></div>

    <div id="yes_no" hidden="hidden">
    <div class="btn-group btn-group-justified" role="group">
        <div class="btn-group pull_right" role="group">
            <button type="button" class="btn btn-danger" id="not">Отмена</button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-success" id="yes">Войти</button>
        </div>
    </div>
    </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<<JS

$(function(){
    var c = '';
    var s = '';
    var array_all;
    var arr = $('form');
    var test = $('form').serializeArray();
    test.push({name: 'id', value: 105});
//    console.log(test);

    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : test
    }).done(function(response) {
        array_all = $.parseJSON(response);
    }).fail(function() {
        console.log('not1');
    });

    /*jQuery("#clients_search_result").on('click', 'input', function(e){
        e.stopPropagation();*/

    $('#id_region').change(function(){
        var reg = $(this).val();

        if(reg.length > 0){
//            console.log(reg);

            $('#id_city').prop('disabled',false).empty().prepend($('<option value="">Выберите город</option>'));

            $.each(array_all, function(index, value) {
                var r1 = value.kod_region;
//                console.log(value.kod_region+'*kod_region');
                if(r1==reg){
//                    console.log(value.city+'*city');
                    $('#id_city').append($('<option value="'+value.kod_city+'">'+value.city+'</option>'));
                }else{
                    console.log('ne poshlo');
                }
            });

            var opt = [];
            $("#id_city option").each(function() {
                var f = $(this).val();
                var c = $.inArray(f,opt);
                if(c == -1){
                    opt.push(f);
                }
                else{
                    $(this).remove();
                }
            });
        }else{
            $('#id_city').prop('disabled',true).empty().prepend($('<option value="">Выберите город</option>'));
            $('#id_sad').prop('disabled',true).empty().prepend($('<option value="">Выберите садик</option>'));
            $('#id_snils').val('').prop('disabled',true);
            $('#id_polis').val('').prop('disabled',true);
            $('#but_login').prop('disabled',true);
        }
    });

    $('#id_city').change(function(){
        c = $(this).val();
        if(c.length > 0){
            $('#id_snils').val('').prop('disabled',true);
            $('#id_polis').val('').prop('disabled',true);
            $('#but_login').prop('disabled',true);

//            console.log(c);

            $('#id_sad').prop('disabled',false).empty().prepend($('<option value="">Выберите садик</option>'));

            $.each(array_all, function(index, value) {
                var r1 = value.kod_city;
//                console.log(value.kod_city+'*kod_city');
                if(r1==c){
                    $('#id_sad').append($('<option value="'+value.number+'">'+value.number+'</option>'));
                }else{
                    console.log('ne poshlo');
                }
            });

            var opt = [];
            $("#id_sad option").each(function() {
                var f = $(this).val();
                var c = $.inArray(f,opt);
                if(c == -1){
                    opt.push(f);
                }
                else{
                    $(this).remove();
                }
            });

        }else{
            $('#but_login').prop('disabled',true);
            $('#id_sad').prop('disabled',true).empty().prepend($('<option value="">Выберите садик</option>'));
            $('#id_snils').val('').prop('disabled',true);
            $('#id_polis').val('').prop('disabled',true);
        }
    });

    $('#id_sad').change(function(){
        s = $(this).val();
        if(s.length > 0){
//            console.log(s);
            $('#id_snils').prop('disabled',false);
            $('#id_polis').prop('disabled',false);
        }else{
            $('#id_snils').val('').prop('disabled',true);
            $('#id_polis').val('').prop('disabled',true);
            $('#but_login').prop('disabled',true);
        }
    });

    $('#id_snils').on('input',function(sss){
        var sn = $(this).val();
//        console.log($(sss.target).val());
        var inputLength = sss.target.value;
        var count = inputLength.replace(/[^-' '0-9]/,'').length;
        if(count > 13){
            $('#but_login').prop('disabled',false);
        }else{
            $('#but_login').prop('disabled',true);
        }
    });

    $('#but_login').click(function(){

        var cap = $('input[id$=-captcha]').val();
        if(cap.length < 1){
            alert('Введите проверочный код');
            return;
        }
//        console.log(cap);
        test = $('form').serializeArray();
//        console.log(test);//return;
//        console.log($('#id_snils').val());
//        console.log('u0601128_'+c+s);
        test.push({name: "id", value: '5'});//return;

        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
//                data : {id: '5', data: test}
            data : test
        }).done(function(response) {
//                console.log(response);
                if(response==400){
                    $("img[id$='-captcha-image']").trigger('click');
                    $('input[id$=-captcha]').val('');
                }else{
                    $('#but_login').fadeOut(600);
//                    $("img[id$='-captcha-image']").trigger('click');
                    $('#name').delay(500).fadeIn(500).html(response);
                    $('#id_region,#id_city,#id_sad,#id_snils,#id_polis').prop('disabled',true);
                    $('#yes_no').delay(700).fadeIn(500);
                }


            }).fail(function() {
                console.log('notb');
//                $("#name").LoadingOverlay("hide");
                alert("Ошибка ввода данных");
            });
        return false;
    });

    $('#not').click(function(){
        var eee = 'ttttttt';
//        console.log(eee);
        $("img[id$='-captcha-image']").trigger('click');
        $('input[id$=-captcha]').val('');
        $('#id_snils').val('');
        $('#id_polis').val('');
        $('#name,#yes_no').fadeOut(600).delay(200);
        $('#but_login').delay(600).fadeIn().prop('disabled',true);
        $('#id_region,#id_city,#id_sad,#id_snils,#id_polis').prop('disabled',false);

        $('#yes').prop('disabled',false);
//        $('#but_login').fadeOut();
//        $('#name').delay(300).fadeIn().LoadingOverlay("show");
//        $("#name").LoadingOverlay("show");


        return false;
    });



    $('#yes').click(function(){
        var test536 = $('#login3333').serializeArray();

        var city = $('#id_city').val().trim();
        var sad = $('#id_sad').val().trim();
        var snils = $('#id_snils').val().trim();
        var polis = $('#id_polis').val().trim();

        var town = $('#id_city option:selected').text().trim();

        test536.push({name: "id", value: '536'});
        test536.push({name: "LoginF[city]", value: city});
        test536.push({name: "LoginF[sad]", value: sad});
        test536.push({name: "LoginF[snils]", value: snils});
        test536.push({name: "LoginF[polis]", value: polis});
        test536.push({name: "LoginF[region]", value: 0});

        test536.push({name: "town", value: town});

//        console.log(test536);
//        return;

        var eee = $('#name').text();
        if(eee.indexOf('СНИЛС')>0){
            $('#yes').prop('disabled',true);
        }else{
            $('#yes').prop('disabled',false);
//            $('#id_snils').val('');
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : test536
            })/*.done(function(response) {
                console.log(response);
            }).fail(function() {
                console.log('notb');
            })*/;
        }

        return false;
    });

    /*$("#fox").on('click',function(e){
        //#testimonials-captcha-image is my captcha image id
        $("img[id$=-captcha-image]").trigger(click);
        e.preventDefault();
    });*/

    /*$('#but_login777').on('click',function(){
        console.log('frgdfhg');
        $("img[id$='-captcha-image']").trigger('click');
        e.preventDefault();
    });*/
});
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>