<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.01.20
 * Time: 12:24
 */
//debug($array);
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
$width = '2.5%';
$year = $model_y->name;
$month = $model_m->name;

?>
<!--<div style="background-color: #e83e8c"></div>-->
<div class="my_table my_table2 not_selected_text_on_block">
    <table class="table-striped table-bordered">

        <?php

        foreach($array as $q){

            $date = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
            $last_day = date('t',strtotime($date));
            $today = date('d',strtotime($date_proc));

            $t = 1;
            $red = 0;



            //depricated
            while($t <= 31){
                if ($t > $last_day){
                    ${'value'.$t} = 'X';
                    ${'styles'.$t} = '';
                }else{
                    if((date('N', strtotime($date)) > 5 || in_array($date,$model_d_date)) && !in_array($date,$model_d_antidate)){
                        ${'value'.$t} = '<span style="color: #adadad">B</span>';
                        ${'styles'.$t} = '';
                    }else{
                        $red++;
                        if ($red == 5){
                            if ($t > $today){
                                ${'value'.$t} = '';
                            }else{
                                ${'value'.$t} = $q['k'.$t];
                            }
                            ${'styles'.$t} = ';background-color: rgb(255, 219, 210)"';
                            $red = 0;
                        }else{
                            if ($t > $today){
                                ${'value'.$t} = '';
                                ${'styles'.$t} = '';
                            }else{
                                ${'value'.$t} = $q['k'.$t];
                                ${'styles'.$t} = ';background-color: rgb(216, 250, 217)"';
                            }
                        }
                    }
                }
                $date = date('Y-m-d',strtotime($date. " 1 day"));
                $t++;
            }

            if (trim($q['name'])=='Итого'){
                $id_t = '';
            }else{
                $id_t = $q['id'];
            }


            echo '<tr>
                <td style="width: 2%">'.$id_t.'</td>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: 4%">'.$q['kolvse'].'</td>';

            for($i=0;$i<=31;$i++){
                echo '<td style="width: '.$width.$styles.$i.'">'.$value.$i.'</td>';
            }

            echo '<td style="width: 5%">'.$q['summ'].'</td>
            </tr>';
        }

        ?>

    </table>
</div>