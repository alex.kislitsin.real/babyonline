<?php
$width = '5.3%';
/*if (count($array) > 14){
    $height = ';height: 82vh';
}else{
    $height = '';
}*/
?>
<div style="height: "></div>
<div class="my_table my_table2 not_selected_text_on_block" style="background-color: #eeeeee;overflow-y: scroll">
    <table>
        <tr>
            <td class="not_item" style="width: 3%;text-align: center">№</td>
            <td class="not_item">ФИО</td>
            <td class="not_item" style="width: <?= $width ?>">П.дети</td>
            <td class="not_item" style="width: <?= $width ?>">П.сотр.</td>
            <td class="not_item" style="width: <?= $width ?>">Сотр.</td>
            <td class="not_item" style="width: <?= $width ?>">Дети</td>
            <td class="not_item" style="width: <?= $width ?>">Антро</td>
            <td class="not_item" style="width: <?= $width ?>">Таб</td>
            <td class="not_item" style="width: <?= $width ?>">Жур</td>
            <td class="not_item" style="width: <?= $width ?>">Поиск</td>
            <td class="not_item" style="width: <?= $width ?>">5 дней</td>
            <td class="not_item" style="width: <?= $width ?>">Корре</td>
            <td class="not_item" style="width: <?= $width ?>">Истор</td>
            <td class="not_item" style="width: <?= $width ?>">Прив</td>
            <td class="not_item" style="width: <?= $width ?>">Круж</td>
            <td class="not_item" style="width: <?= $width ?>">Зп</td>
        </tr>
    </table>
</div>
<div class="my_table my_table2 not_selected_text_on_block" style="overflow-y: scroll;max-height: 70vh" id="id_rbac_table">
    <table class="table-striped table-bordered">

        <?php
        $i = 1;
        foreach($array as $q){

            $q['pitanie_deti'] == 1 ? $pitanie_deti = '&#10004;' : $pitanie_deti = '';
            $q['pitanie_so'] == 1 ? $pitanie_so = '&#10004;' : $pitanie_so = '';
            $q['delo_deti'] == 1 ? $delo_deti = '&#10004;' : $delo_deti = '';
            $q['delo_so'] == 1 ? $delo_so = '&#10004;' : $delo_so = '';
            $q['antro'] == 1 ? $antro = '&#10004;' : $antro = '';
            $q['tabel'] == 1 ? $tabel = '&#10004;' : $tabel = '';
            $q['journal'] == 1 ? $journal = '&#10004;' : $journal = '';
            $q['search'] == 1 ? $search = '&#10004;' : $search = '';
            $q['five'] == 1 ? $five = '&#10004;' : $five = '';
            $q['correct'] == 1 ? $correct = '&#10004;' : $correct = '';
            $q['history'] == 1 ? $history = '&#10004;' : $history = '';
            $q['injection'] == 1 ? $injection = '&#10004;' : $injection = '';
            $q['crujki'] == 1 ? $crujki = '&#10004;' : $crujki = '';
            $q['zp'] == 1 ? $zp = '&#10004;' : $zp = '';


            echo '<tr
            data-id="'.$q['id'].'"
            data-pitanie_deti="'.$q['pitanie_deti'].'"
            data-pitanie_so="'.$q['pitanie_so'].'"
            data-delo_deti="'.$q['delo_deti'].'"
            data-delo_so="'.$q['delo_so'].'"
            data-antro="'.$q['antro'].'"
            data-tabel="'.$q['tabel'].'"
            data-journal="'.$q['journal'].'"
            data-search="'.$q['search'].'"
            data-five="'.$q['five'].'"
            data-correct="'.$q['correct'].'"
            data-history="'.$q['history'].'"
            data-injection="'.$q['injection'].'"
            data-crujki="'.$q['crujki'].'"
            data-zp="'.$q['zp'].'"
            >
                <td style="width: 3%">'.$i.'</td>
                <td id="id_n">'.trim($q['name']).'<br/><span style="font-size: 12px;color: #0000ff">'.trim($q['dol']).'</span></td>
                <td id="td_pitanie_deti" data-id="'.$q['id'].'" data-valu="'.$q['pitanie_deti'].'" data-check="1" style="width: '.$width.'">'.$pitanie_deti.'</td>
                <td id="td_pitanie_so" data-id="'.$q['id'].'" data-valu="'.$q['pitanie_so'].'" data-check="2" style="width: '.$width.'">'.$pitanie_so.'</td>
                <td id="td_delo_deti" data-id="'.$q['id'].'" data-valu="'.$q['delo_deti'].'" data-check="3" style="width: '.$width.'">'.$delo_deti.'</td>
                <td id="td_delo_so" data-id="'.$q['id'].'" data-valu="'.$q['delo_so'].'" data-check="4" style="width: '.$width.'">'.$delo_so.'</td>
                <td id="td_antro" data-id="'.$q['id'].'" data-valu="'.$q['antro'].'" data-check="5" style="width: '.$width.'">'.$antro.'</td>
                <td id="td_tabel" data-id="'.$q['id'].'" data-valu="'.$q['tabel'].'" data-check="6" style="width: '.$width.'">'.$tabel.'</td>
                <td id="td_journal" data-id="'.$q['id'].'" data-valu="'.$q['journal'].'" data-check="7" style="width: '.$width.'">'.$journal.'</td>
                <td id="td_search" data-id="'.$q['id'].'" data-valu="'.$q['search'].'" data-check="8" style="width: '.$width.'">'.$search.'</td>
                <td id="td_five" data-id="'.$q['id'].'" data-valu="'.$q['five'].'" data-check="9" style="width: '.$width.'">'.$five.'</td>
                <td id="td_correct" data-id="'.$q['id'].'" data-valu="'.$q['correct'].'" data-check="10" style="width: '.$width.'">'.$correct.'</td>
                <td id="td_history" data-id="'.$q['id'].'" data-valu="'.$q['history'].'" data-check="11" style="width: '.$width.'">'.$history.'</td>
                <td id="td_injection" data-id="'.$q['id'].'" data-valu="'.$q['injection'].'" data-check="12" style="width: '.$width.'">'.$injection.'</td>
                <td id="td_crujki" data-id="'.$q['id'].'" data-valu="'.$q['crujki'].'" data-check="13" style="width: '.$width.'">'.$crujki.'</td>
                <td id="td_zp" data-id="'.$q['id'].'" data-valu="'.$q['zp'].'" data-check="14" style="width: '.$width.'">'.$zp.'</td>
            </tr>';
            $i++;
        }

        ?>

    </table>
</div>

<span style="font-size: 12px;color: #0000ff"></span>

<?php
$script = <<<JS
$(function(){



    $('td[data-valu]').on('click',function(){
        var s = $("#id_rbac_table").scrollTop();

        var id_so = $(this).data('id');
        var valu = $(this).data('valu');
        var check = $(this).data('check');
        var arr = $('#hot_form_rbac');
        if(valu > 0){
            $('#id_rbac_ajax').val(1);
            var form = $('#hot_form_rbac').serializeArray();
            form.push({name: 'check',value: check});
            form.push({name: 'id_so',value: id_so});
            console.log(form);//return;
            $(this).LoadingOverlay("show");$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $("#id_render_modal_rbac").html(response);
                    $("#id_rbac_table").scrollTop(s);
    //                $('#but_message_save_settings').hide();
    //                $('#id_settings_ajax').val(1);
                    $('#modal_hot_rbac').modal('show');
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                });
                return false;
        }else{
            $('#id_rbac_ajax').val(2);
            var form = $('#hot_form_rbac').serializeArray();
            form.push({name: 'check',value: check});
            form.push({name: 'id_so',value: id_so});
            console.log(form);//return;
            $(this).LoadingOverlay("show");$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $("#id_render_modal_rbac").html(response);
                    $("#id_rbac_table").scrollTop(s);
    //                $('#but_message_save_settings').hide();
    //                $('#id_settings_ajax').val(1);
                    $('#modal_hot_rbac').modal('show');
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                });
                return false;
        }

        return false;
    });



})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

