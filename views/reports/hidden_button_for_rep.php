<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//debug($model_m['name']);
?>

<!--<div class="pull-right">-->
<div class="date_picker_so">
    <?php $form = ActiveForm::begin([
        'id' => 'form_rep_hidden_button',
//    'layout'=>'horizontal',
//    'value' => '1111',
        'action' => ['reports/rep'],
//    'action' => ['sp/spview'],
//    'action' => ['sp/spitemgroupajax'],
        'method' => 'POST',
//    'size' => 'lg',
//    'fieldConfig' => [
//        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
////        'horizontalCssClasses' => [
////            'label' => 'col-sm-4',
////            'offset' => 'col-sm-offset-4',
////            'wrapper' => 'col-sm-8',
////            'error' => '',
////            'hint' => '',
////        ],
//    ],
        'type' => ActiveForm::TYPE_INLINE,
//    'enableAjaxValidation' => false,//
    ])?>


    <div id="h_b_deti" hidden="hidden">
        <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
            'id' => 'drop_rep_hidden_but0',
            'disabled'=> 'disabled',
        ])->label(false) ?>
        <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
            'id' => 'drop_rep_hidden_but1',
            'disabled'=> 'disabled',
        ])->label(false) ?>
        <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
            'id' => 'drop_rep_hidden_but2',
            'disabled'=> 'disabled',
        ])->label(false) ?>

        <div class="btn-group" id="dropdown_cru_excel">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Сохранить табель в excel &nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
                <li><?= Html::a('Сохранить в Excel для бухгалтерии', [
                        'reports/excel','id' => 1,
                        'year' => $model_y->name,
                        'month' => $model_m->name,
                        'id_group' => $model_group->name
                    ], ['id' => 'button34']); ?></li>
                <li><?= Html::a('Сохранить в Excel для садика', [
                        'reports/excel','id' => 2,
                        'year' => $model_y->name,
                        'month' => $model_m->name,
                        'id_group' => $model_group->name
                    ], ['id' => 'button35']); ?></li>
                <li><?= Html::a('Справка дети', [
                        'reports/excel','id' => 4,
                        'year' => $model_y->name,
                        'month' => $model_m->name,
                        'id_group' => $model_group->name
                    ],['id'=>'button300','style' => 'height:40px']) ?></li>
            </ul>
        </div>

        <div class="btn-group" id="dropdown_cru_excel_all_group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Табель в excel все группы &nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
                <li><?= Html::a('Excel для бухгалтерии все группы', [
                        'reports/excel','id' => 5,
                        'year' => $model_y->name,
                        'month' => $model_m->name,
                        'id_group' => $model_group->name
                    ], ['id' => 'button_all_group_buh']); ?></li>
                <li><?= Html::a('Excel для садика все группы', [
                        'reports/excel','id' => 6,
                        'year' => $model_y->name,
                        'month' => $model_m->name,
                        'id_group' => $model_group->name
                    ], ['id' => 'button_all_group_my']); ?></li>
            </ul>
        </div>



    </div>
    <div id="h_b_so" hidden="hidden">
        <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
            'id' => 'drop_rep_hidden_but0so',
            'disabled'=> 'disabled',
        ])->label(false) ?>
        <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
            'id' => 'drop_rep_hidden_but1so',
            'disabled'=> 'disabled',
        ])->label(false) ?>
        <?= Html::a('Сохранить в Excel для бухгалтерии', [
            'reports/excel','id' => 3,
            'year' => $model_y->name,
            'month' => $model_m->name,
            'id_group' => $model_group->name
        ], ['class'=>'btn btn-md btn-default','id' => 'button36']); ?>
        <?/*= Html::a('Сохранить в Excel для садика', [
            'reports/excel','id' => 4,
            'year' => $model_y->name,
            'month' => $model_m->name,
//            'id_group' => $model_group->name
        ], ['class'=>'btn btn-md btn-default','id' => 'button37']); */?>

    </div>


    <div id="day_5" hidden="hidden">
        <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
            'id' => 'drop_rep_hidden_but3',
            'disabled'=> 'disabled',
        ])->label(false) ?>
        <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
            'id' => 'drop_rep_hidden_but4',
            'disabled'=> 'disabled',
        ])->label(false) ?>
        <!--    --><?//= $form->field($model_m,'name')->dropDownList($_monthsList,['id' => 'drop_rep_hidden_but5'])->label(false) ?>
    </div>


<!--    --><?//= $form->field($model_y,'name')->hiddenInput()->label(false); ?>


    <?php $form = ActiveForm::end()?>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'form_rep_hidden_to_excel',
    'action' => ['reports/excel'],
    'method' => 'POST',
])?>
<?php $form = ActiveForm::end()?>


<?php
$scr = <<< JS



$(function() {
    function show_deti(){
        $('#day_5,#h_b_so').hide();
        $('#day_5 select, #h_b_so select').prop('disabled',true);
        $('#h_b_deti select').prop('disabled',false);
        $('#h_b_deti').fadeIn();
    }
    function show_so(){
        $('#day_5,#h_b_deti').hide();
        $('#day_5 select, #h_b_deti select').prop('disabled',true);
        $('#h_b_so select').prop('disabled',false);
        $('#h_b_so').fadeIn();
    }
    function show_5(){
        $('#h_b_so,#h_b_deti').hide();
        $('#h_b_so select, #h_b_deti select').prop('disabled',true);
        $('#day_5 select').prop('disabled',false);
        $('#day_5').fadeIn();
    }


    var value = $('#drop_rep').val();
    var old_rep = value;
    console.log(value);
    if(value==1){
            show_deti();
        }
        if(value==2){
            show_so();
        }
        if(value==3){
            show_5();
        }



    $('#drop_rep').on('change', function() {

        // Получаем объект формы
        value = $(this).val();
//        var name_group = $(this).val('name');
        console.log(value);
        if(value==1){
            show_deti();
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            var form = $('#form_rep_item_reports');
            var form2 = $('form').serializeArray();
            form2.push({name:'old_rep',value:old_rep});
            console.log(form2);
            $.ajax({
                type : form.attr('method'),
                url : form.attr('action'),
                data : form2
            }).done(function(response) {
//                console.log(response+' otvet ot controllera');
                $("*").LoadingOverlay("hide");
                $('.rep_boss_down').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
            });
        }
        if(value==2){
            show_so();
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            form = $('#form_rep_item_reports');
            form2 = $('form').serializeArray();
            form2.push({name:'old_rep',value:old_rep});
            console.log(form);
            $.ajax({
                type : form.attr('method'),
                url : form.attr('action'),
                data : form2
            }).done(function(response) {
//                console.log(response+' otvet ot controllera');
                $("*").LoadingOverlay("hide");
                $('.rep_boss_down').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
            });
        }
        if(value==3){
            show_5();
//            $(".rep_boss").LoadingOverlay("show");
            form = $('#form_rep_item_reports');
            form2 = $('form').serializeArray();
            form2.push({name:'old_rep',value:old_rep});
            console.log(form.serializeArray());
//            return;
            $.ajax({
                type : form.attr('method'),
                url : form.attr('action'),
                data : form2
            }).done(function(response) {
//                console.log(response+' otvet ot controllera');
                $("*").LoadingOverlay("hide");
                $('.rep_boss').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
            });
        }



        return false;
    });

    $('#h_b_deti select,#h_b_so select').on('change', function() {

//        $('#button34').attr("href","/index.php?r=reports%2Fexcel&id="+2);

//        console.log($(this).val()+' 000');
//        $('#drop_rep_hidden_but1').val($(this).val());
//
//        console.log($('#drop_rep_hidden_but1').val()+' 2');
//        $('#button36').data('id',555);

//        console.log(ggg+' 1111111');
        // Получаем объект формы
        value = $('#drop_rep').val();
//        var name_group = $(this).val('name');
        console.log(value);
        if(value==1){

            show_deti();
        }
        if(value==2){
            show_so();
        }
        if(value==3){
            show_5();
        }

        var form2 = $('#form_rep_item_reports');
        var form = $('#form_rep_hidden_button,#form_rep_item_reports').serializeArray();
        console.log(form);//return;
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
                type : form2.attr('method'),
                url : form2.attr('action'),
                data : form
        }).done(function(response) {
//                console.log(response+' otvet ot controllera');
//                $(".item_child_sp").text("");
                $("*").LoadingOverlay("hide");
                $('.rep_boss_down').html(response);
        }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
        });

        return false;
    });

    /*$('#button35').on('click',function(){

//        var me = $('#button34').data('id');
        $('#button34').attr("href","/index.php?r=reports%2Fexcel&id="+2);

//        href="/index.php?r=reports%2Fexcel&amp;id=1&amp;year=2019&amp;month=12"
//        href="/index.php?r=reports%2Fexcel&amp;id=1"


        console.log(me);
//        console.log(data.serializeArray());
        return;
        var form = $('form').serializeArray();
        var form2 = $('#form_rep_hidden_to_excel');
        console.log(form);
        $.ajax({
                type : form2.attr('method'),
                url : form2.attr('action'),
                data : form
        });

        return false;
    });*/

    $('#drop_rep_hidden_but0').on('change',function(){
        $('#button34').attr("href","/index.php?r=reports%2Fexcel&id=1&year="+$(this).val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$('#drop_rep_hidden_but2').val());
        $('#button35').attr("href","/index.php?r=reports%2Fexcel&id=2&year="+$(this).val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$('#drop_rep_hidden_but2').val());
        $('#button300').attr("href","/index.php?r=reports%2Fexcel&id=4&year="+$(this).val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$('#drop_rep_hidden_but2').val());

        $('#button_all_group_buh').attr("href","/index.php?r=reports%2Fexcel&id=5&year="+$(this).val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$('#drop_rep_hidden_but2').val());
        $('#button_all_group_my').attr("href","/index.php?r=reports%2Fexcel&id=6&year="+$(this).val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$('#drop_rep_hidden_but2').val());
//        console.log($(this).val()+' year '+$('#drop_rep_hidden_but1').val()+' month '+$('#drop_rep_hidden_but2').val()+' month');
        return false;
    });
    $('#drop_rep_hidden_but1').on('change',function(){
        $('#button34').attr("href","/index.php?r=reports%2Fexcel&id=1&year="+$('#drop_rep_hidden_but0').val()+"&month="+$(this).val()+"&id_group="+$('#drop_rep_hidden_but2').val());
        $('#button35').attr("href","/index.php?r=reports%2Fexcel&id=2&year="+$('#drop_rep_hidden_but0').val()+"&month="+$(this).val()+"&id_group="+$('#drop_rep_hidden_but2').val());
        $('#button300').attr("href","/index.php?r=reports%2Fexcel&id=4&year="+$('#drop_rep_hidden_but0').val()+"&month="+$(this).val()+"&id_group="+$('#drop_rep_hidden_but2').val());

        $('#button_all_group_buh').attr("href","/index.php?r=reports%2Fexcel&id=5&year="+$('#drop_rep_hidden_but0').val()+"&month="+$(this).val()+"&id_group="+$('#drop_rep_hidden_but2').val());
        $('#button_all_group_my').attr("href","/index.php?r=reports%2Fexcel&id=6&year="+$('#drop_rep_hidden_but0').val()+"&month="+$(this).val()+"&id_group="+$('#drop_rep_hidden_but2').val());
//        console.log($(this).val()+' month '+$('#drop_rep_hidden_but0').val()+' year '+$('#drop_rep_hidden_but2').val()+' group');
        return false;
    });
    $('#drop_rep_hidden_but2').on('change',function(){
        $('#button34').attr("href","/index.php?r=reports%2Fexcel&id=1&year="+$('#drop_rep_hidden_but0').val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$(this).val());
        $('#button35').attr("href","/index.php?r=reports%2Fexcel&id=2&year="+$('#drop_rep_hidden_but0').val()+"&month="+$('#drop_rep_hidden_but1').val()+"&id_group="+$(this).val());
//        console.log($(this).val()+' group '+$('#drop_rep_hidden_but1').val()+' month '+$('#drop_rep_hidden_but0').val()+' year');
        return false;
    });


    $('#drop_rep_hidden_but0so').on('change',function(){
        $('#button36').attr("href","/index.php?r=reports%2Fexcel&id=3&year="+$(this).val()+"&month="+$('#drop_rep_hidden_but1so').val()+"&id_group=0");
        return false;
    });
    $('#drop_rep_hidden_but1so').on('change',function(){
        $('#button36').attr("href","/index.php?r=reports%2Fexcel&id=3&year="+$('#drop_rep_hidden_but0so').val()+"&month="+$(this).val()+"&id_group=0");
        return false;
    });

    /*$('#button_all_group_buh').click(function(){
        $('#modal_progressbar').modal('show');
    });*/





});


JS;
$this->registerJs($scr, yii\web\View::POS_END);
//?>




