<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\switchinput\SwitchInput;
//$model->flagActive = false;

$form = ActiveForm::begin([
    'id' => 'form_modal_settings',
    'method' => 'POST',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'action' => ['reports/settings'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-6',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-5',
        ],
    ],
]); ?>

<?= $form->field($model, 'day_tabel')->textInput([
    'id' => 'day_tabel_modal_settings',
    'placeholder' => 'Количество дней до закрытия табеля',
    'type'=> 'number',
//    'readonly' => true
]) ?>

<?= $form->field($model,'city')->textInput([
    'id' => 'city_modal_settings',
    'placeholder' => 'Название города'
]) ?>
<?= $form->field($model, 'boss')->textInput([
    'id' => 'boss_modal_settings',
    'placeholder' => 'в формате "И.О. Фамилия"'
]) ?>
<?= $form->field($model, 'boss_rod')->textInput([
    'id' => 'boss_rod_modal_settings',
    'placeholder' => 'в формате "Ивановой Елены Васильевны"'
]) ?>
<?= $form->field($model, 'medsestra')->textInput([
    'id' => 'medsestra_modal_settings',
    'placeholder' => 'в формате "И.О. Фамилия"'
]) ?>
<?= $form->field($model, 'lico_cru')->textInput([
    'id' => 'lico_cru_modal_settings',
    'placeholder' => 'в формате "И.О. Фамилия"'
]) ?>
<?= $form->field($model, 'lico_cru2')->textInput([
    'id' => 'lico_cru_modal_settings',
    'placeholder' => 'в формате "И.О. Фамилия"'
]) ?>

<?= $form->field($model,'flagActive')->widget(SwitchInput::className(),[
//    'name' => 'status_5',
//    'type' => SwitchInput::RADIO,
//    'value' => -1,
//    'name'=>'status_41',
    'pluginOptions'=>[
//        'margin-left' => 30,
        'handleWidth'=>30,
        'onText'=>'вкл',
        'offText'=>'выкл',
        'onColor' => 'success',
        'offColor' => 'danger',
    ],
]);

?>



<?php ActiveForm::end(); ?>
