<?php
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
?>

    <div class="rep_boss">
        <div class="rep_boss_up_panel">
            <div class="rep_boss_up_panel_item_rep">
                <div class="date_picker_so">
                <?php $form = ActiveForm::begin([
                    'id' => 'form_dvij_new',
                    'action' => ['reports/dvij'],
                    'method' => 'POST',
                    'type' => ActiveForm::TYPE_VERTICAL,
                    'enableAjaxValidation' => false,
                ])?>
                <?= $form->field($model_item_date,'item_date')->widget(DatePicker::className(),[
//                    'name' => 'dp_dvij_new',
                    'size' => 'md',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'readonly' => true,
                    'options' => [
                        'placeholder' => 'Выберите дату ...',
                        'id' => 'dp_dvij_new'
                    ],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'todayBtn' => true,
//                        'daysOfWeekDisabled' => [0, 6],
                        'daysOfWeekHighlighted' => [0, 6],
                        'datesDisabled' => $array_disabled_dates,
                        'toggleActive'   => true,
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy'
                    ],
                    'pluginEvents' => [
                    'changeDate' => 'function(e) {
                     var date1 = $("#dp_dvij_new").val();
                     if(date1 != ""){
                         var testform = $("#form_dvij_new");
                         $("#dp_dvij_new").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                         //$("#id_grid_dvig").LoadingOverlay("show");
                         $.ajax({
                             type : testform.attr("method"),
                             url : testform.attr("action"),
                             data : testform.serializeArray()
                         }).done(function(response) {
                             $("*").LoadingOverlay("hide");
                             $("#id_grid_dvig").html(response);
                         }).fail(function() {
                             $("*").LoadingOverlay("hide");
                             console.log("not");
                         });
                     }
                     }',
                    ]
                ])->label(false); ?>
                <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="rep_boss_up_panel_1" data-id7="0">
            </div>
        </div>
        <div class="rep_boss_down_dvij" id="id_grid_dvig">
            <?= $this->render('dvigenie_table',compact(
                'dataProvider',
                'model_dvig',
                'percent')) ?>
        </div>
    </div>