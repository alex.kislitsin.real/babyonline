<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 21.10.19
 * Time: 14:08
 */
use yii\bootstrap\Modal;
use yii\bootstrap\Progress;

//debug('hello');
//$arr1 = array_fill(0,23,0);
//$arr2 = array_fill(0,5,0);
//$fff = $model->name;
//debug($fff);
?>




<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <?= $this->render('form_item_reports',compact(
                'model_id',
                'array',
                'model_rep')) ?>
        </div>
        <div class="rep_boss_up_panel_1">
            <?= $this->render('hidden_button_for_rep',compact(
                'model_rep',
                'array_year',
                'model_group',
                'array_gruppa',
                'item_gruppa',
                'model_y',
                'model_m',
                'array_year_tabel_deti',
                '_monthsList'
                )) ?>
        </div>
    </div>
    <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
        <table>
            <tr>
                <td style="width: 2%">№</td><td style="width: 90px">Фамилия Имя</td><td style="width: 5%">№ счета</td><td class="nnn">1</td><td class="nnn">2</td><td class="nnn">3</td>
                <td class="nnn">4</td><td class="nnn">5</td><td class="nnn">6</td><td class="nnn">7</td><td class="nnn">8</td><td class="nnn">9</td><td class="nnn">10</td><td class="nnn">11</td><td class="nnn">12</td>
                <td class="nnn">13</td><td class="nnn">14</td><td class="nnn">15</td><td class="nnn">16</td><td class="nnn">17</td><td class="nnn">18</td><td class="nnn">19</td><td class="nnn">20</td>
                <td class="nnn">21</td><td class="nnn">22</td><td class="nnn">23</td><td class="nnn">24</td><td class="nnn">25</td><td class="nnn">26</td><td class="nnn">27</td><td class="nnn">28</td>
                <td class="nnn">29</td><td class="nnn">30</td><td class="nnn">31</td><td style="width: 5%">Пришли</td><td style="width: 5%">Пропуски</td><td style="width: 8%">Причина</td>
            </tr>
        </table>
    </div>
    <div class="rep_boss_down">
        <?= $this->render('diseases/table',compact(
            'number_reports',
            'dataProvider',
            'model2')) ?>
    </div>
</div>