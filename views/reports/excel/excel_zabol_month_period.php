<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.04.20
 * Time: 16:42
 */
$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);

$style_border_all_medium = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
        ),
    ),
);

$_monthsList = array(
    1=>"Январь",2=>"Февраль",3=>"Март",
    4=>"Апрель",5=>"Май", 6=>"Июнь",
    7=>"Июль",8=>"Август",9=>"Сентябрь",
    10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");

$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(11);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.5);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);

$width_col = 11;
$width_col_percent = 9;

$sheet->getColumnDimension('A')->setWidth(5);
$sheet->getColumnDimension('B')->setWidth(18);
$sheet->getColumnDimension('C')->setWidth($width_col);
$sheet->getColumnDimension('D')->setWidth($width_col)->setVisible(false);
$sheet->getColumnDimension('E')->setWidth($width_col);
$sheet->getColumnDimension('F')->setWidth($width_col_percent);
$sheet->getColumnDimension('G')->setWidth($width_col);
$sheet->getColumnDimension('H')->setWidth($width_col_percent);
$sheet->getColumnDimension('I')->setWidth($width_col);
$sheet->getColumnDimension('J')->setWidth($width_col_percent);
$sheet->getColumnDimension('K')->setWidth($width_col);
$sheet->getColumnDimension('L')->setWidth($width_col_percent);
$sheet->getColumnDimension('M')->setWidth($width_col);
$sheet->getColumnDimension('N')->setWidth($width_col);


foreach($array as $a){
    $count_works_days = $a['worksday'];
    break;
}

$name_file = 'Показатели посещаемости-заболеваемости за '.$_monthsList[$month].' '.$year.' года ('.$count_works_days.' раб.дн.)';

$line = 1;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", $name_file);
$sheet->mergeCells("A{$line}:N{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);

$line++;//2
//$sheet->getRowDimension($line)->setRowHeight(20);
$line++;//2
$line_start = $line;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", "Группа")->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", "Ср.сп.\nчисл.")->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", "Кол-во\nраб.дн.")->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", "Посеща-\nемость")->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", "%")->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", 'Пропуски общие')->getStyle("G{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("H{$line}", '%')->getStyle("H{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("I{$line}", "Пропуски\nпо болезни")->getStyle("I{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("J{$line}", '%')->getStyle("J{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("K{$line}", "Пропуски\nпрочее")->getStyle("K{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("L{$line}", '%')->getStyle("L{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("M{$line}", "Ниразу\nне болели")->getStyle("M{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("N{$line}", "Индекс\nздоровья")->getStyle("N{$line}")->getAlignment()->setWrapText(true);

$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:N{$line}")->applyFromArray($style_border_all);

//$line++;//12
$line_formula = $line;
$line_formula++;
$iteration = 1;
foreach($array as $a){

    $line++;
    $sheet->getRowDimension($line)->setRowHeight(25);
    $sheet->getStyle("A{$line}:N{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $a['id'])->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", $a['name'])->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
    $sheet->setCellValue("C{$line}", $a['kolvse'])->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("D{$line}", $a['worksday'])->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("E{$line}", $a['hodili'])->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("F{$line}", $a['hodili_percent'])->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("G{$line}", $a['nehodili'])->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("H{$line}", $a['nehodili_percent'])->getStyle("H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("I{$line}", $a['nehodili_boleli'])->getStyle("I{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("J{$line}", $a['nehodili_boleli_percent'] == NULL ? 0 : $a['nehodili_boleli_percent'])->getStyle("J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("K{$line}", $a['nehodili_prochee'])->getStyle("K{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("L{$line}", $a['nehodili_prochee_percent'] == null ? 0 : $a['nehodili_prochee_percent'])->getStyle("L{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("M{$line}", $a['nirazu'])->getStyle("M{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("N{$line}", $a['z-index'])->getStyle("N{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}
$line_formula_end = $line;



$line++;
$sheet->getRowDimension($line)->setRowHeight(22);
$sheet->setCellValue("C{$line}","=SUM("."C{$line_formula}:C{$line_formula_end})");
$sheet->setCellValue("E{$line}","=ROUND(SUM("."E{$line_formula}:E{$line_formula_end}),2)");
$sheet->setCellValue("F{$line}","=ROUND(AVERAGE("."F{$line_formula}:F{$line_formula_end}),2)");
$sheet->setCellValue("G{$line}","=ROUND(SUM("."G{$line_formula}:G{$line_formula_end}),2)");
$sheet->setCellValue("H{$line}","=ROUND(AVERAGE("."H{$line_formula}:H{$line_formula_end}),2)");
$sheet->setCellValue("I{$line}","=ROUND(SUM("."I{$line_formula}:I{$line_formula_end}),2)");
$sheet->setCellValue("J{$line}","=ROUND(AVERAGE("."J{$line_formula}:J{$line_formula_end}),2)");
$sheet->setCellValue("K{$line}","=ROUND(SUM("."K{$line_formula}:K{$line_formula_end}),2)");
$sheet->setCellValue("L{$line}","=ROUND(AVERAGE("."L{$line_formula}:L{$line_formula_end}),2)");
$sheet->setCellValue("M{$line}","=ROUND(AVERAGE("."M{$line_formula}:M{$line_formula_end}),2)");
$sheet->setCellValue("N{$line}","=ROUND(AVERAGE("."N{$line_formula}:N{$line_formula_end}),2)");
$sheet->getStyle("A{$line}:N{$line}")->applyFromArray($style_border_all);
$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->setCellValue("B{$line}", 'в среднем по саду')->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
$sheet->getStyle("A{$line}:N{$line}")->getFont()->setBold(true);
$sheet->getStyle("E{$line_start}:F{$line}")->applyFromArray($style_border_all_medium);
$sheet->getStyle("G{$line_start}:H{$line}")->applyFromArray($style_border_all_medium);
$sheet->getStyle("I{$line_start}:J{$line}")->applyFromArray($style_border_all_medium);
$sheet->getStyle("K{$line_start}:L{$line}")->applyFromArray($style_border_all_medium);

$bg = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'f1f1f1')
    )
);
$sheet->getStyle("F{$line_start}:F{$line}")->applyFromArray($bg);
$sheet->getStyle("H{$line_start}:H{$line}")->applyFromArray($bg);
$sheet->getStyle("J{$line_start}:J{$line}")->applyFromArray($bg);
$sheet->getStyle("L{$line_start}:L{$line}")->applyFromArray($bg);


//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:N{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);



header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
ob_clean();
$objWriter->save('php://output');

exit;