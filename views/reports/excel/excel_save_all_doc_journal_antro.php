<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 28.03.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(9);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.7);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);

$sheet->getColumnDimension('A')->setWidth(5);
$sheet->getColumnDimension('B')->setWidth(37);
$sheet->getColumnDimension('C')->setWidth(13);
$sheet->getColumnDimension('D')->setWidth(13);
$sheet->getColumnDimension('E')->setWidth(10);
$sheet->getColumnDimension('F')->setWidth(10);
$sheet->getColumnDimension('G')->setWidth(15);
$sheet->getColumnDimension('H')->setWidth(15);
$sheet->getColumnDimension('I')->setWidth(15);
$sheet->getColumnDimension('J')->setWidth(15);
$sheet->getColumnDimension('K')->setWidth(1);
$sheet->getColumnDimension('L')->setWidth(8);
$sheet->getColumnDimension('M')->setWidth(8);

/*$empty_rost=0;
$empty_ves=0;
foreach($array as $e){
    empty($e['rost'])?$empty_rost++:null;
    empty($e['ves'])?$empty_ves++:null;
}*/

$array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
if (count($array_gruppa)>0){
    $name_group = 'группа № '.$id_gruppa.' '.trim($array_gruppa[$id_gruppa]).' ('.count($array).')';
//    $name_group = 'группа № '.$id_gruppa.' '.trim($array_gruppa[$id_gruppa]).' ('.count($array).' - '.$empty_rost.' - '.$empty_ves.')';
}else{
    $name_group = '';
}

$name_file = $period==1 ? "Журнал антропометрии весна ".$name_group." ".$year : "Журнал антропометрии осень ".$name_group." ".$year;

$line = 1;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", $name_file);
$sheet->mergeCells("A{$line}:J{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);

//$line++;//2
//$sheet->getRowDimension($line)->setRowHeight(20);
$line++;//2
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", 'Фамилия Имя')->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", "Дата\nрождения")->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", "Дата\nизмерения")->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", "Рост,\nмм")->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", "Вес,\nкг")->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", "№ комплекта")->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("H{$line}", "Рост стандарт,\nмм")->getStyle("H{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("I{$line}", "Высота стола,\nмм")->getStyle("I{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("J{$line}", "Высота стула,\nмм")->getStyle("J{$line}")->getAlignment()->setWrapText(true);


$sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:J{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:J{$line}")->applyFromArray($style_border_all);

//$line++;//12

$k1=0;
$k2=0;
$k3=0;
$k4=0;
$k5=0;
$k6=0;
$iteration = 1;
foreach($array as $a){

    switch($a['code_mebel']){
        case 1:
            $k1+=1;
            break;
        case 2:
            $k2+=1;
            break;
        case 3:
            $k3+=1;
            break;
        case 4:
            $k4+=1;
            break;
        case 5:
            $k5+=1;
            break;
        case 6:
            $k6+=1;
            break;

    }



    $line++;
    $sheet->getRowDimension($line)->setRowHeight(17);
    $sheet->getStyle("A{$line}:J{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $iteration)->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", $a['name'])->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
    $sheet->setCellValue("C{$line}", ($a['rozd']=='01.01.1900' ? '' : $a['rozd']))->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("D{$line}", $a['dat'])
        ->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("E{$line}", $a['rost'])
        ->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("F{$line}", $a['ves'])
        ->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("G{$line}", $a['mebel'])
        ->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("H{$line}", $a['rost_standart'])
        ->getStyle("H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("I{$line}", $a['visota_stola'])
        ->getStyle("I{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("J{$line}", $a['visota_stula'])
        ->getStyle("J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}
$line22 = 3;
$sheet->setCellValue("L{$line22}", "00-А")->getStyle("L{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$sheet->setCellValue("M{$line22}", $k1==0 ? '' : $k1)->getStyle("M{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$line22++;
$sheet->setCellValue("L{$line22}", "0-Б")->getStyle("L{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$sheet->setCellValue("M{$line22}", $k2==0 ? '' : $k2)->getStyle("M{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$line22++;
$sheet->setCellValue("L{$line22}", "1-В")->getStyle("L{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$sheet->setCellValue("M{$line22}", $k3==0 ? '' : $k3)->getStyle("M{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$line22++;
$sheet->setCellValue("L{$line22}", "2-Г")->getStyle("L{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$sheet->setCellValue("M{$line22}", $k4==0 ? '' : $k4)->getStyle("M{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$line22++;
$sheet->setCellValue("L{$line22}", "3-Д")->getStyle("L{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$sheet->setCellValue("M{$line22}", $k5==0 ? '' : $k5)->getStyle("M{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$line22++;
$sheet->setCellValue("L{$line22}", "4")->getStyle("L{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$sheet->setCellValue("M{$line22}", $k6==0 ? '' : $k6)->getStyle("M{$line22}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);



//$sheet->getStyle("G{$line}")->getFont()->setSize(9);


//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:M{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);



header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;