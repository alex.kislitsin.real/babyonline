<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 06.02.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(9);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.5);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);

$sheet->getColumnDimension('A')->setWidth(5);
$sheet->getColumnDimension('B')->setWidth(30);
$sheet->getColumnDimension('C')->setWidth(13);
$sheet->getColumnDimension('D')->setWidth(15);
$sheet->getColumnDimension('E')->setWidth(15);
$sheet->getColumnDimension('F')->setWidth(15);
$sheet->getColumnDimension('G')->setWidth(15);
$sheet->getColumnDimension('H')->setWidth(15);
$sheet->getColumnDimension('I')->setWidth(20);
$sheet->getColumnDimension('J')->setWidth(20);

$array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
if (count($array_gruppa)>0){
    $name_group = 'группа № '.$id_gruppa.' '.trim($array_gruppa[$id_gruppa]).' ('.count($array).')';
}else{
    $name_group = '';
}



$line = 1;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", 'Журнал р.Манту '.$name_group);
$sheet->mergeCells("A{$line}:J{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);

//$line++;//2
//$sheet->getRowDimension($line)->setRowHeight(20);
$line++;//2
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", 'Фамилия Имя')->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", "Дата\nрождения")->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", date('Y')-4)->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", date('Y')-3)->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", date('Y')-2)->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", date('Y')-1)->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("H{$line}", date('Y'))->getStyle("H{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("I{$line}", 'Медотвод')->getStyle("I{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("J{$line}", 'Примечание '.date('Y'))->getStyle("J{$line}")->getAlignment()->setWrapText(true);


$sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:J{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:J{$line}")->applyFromArray($style_border_all);

//$line++;//12

$iteration = 1;
foreach($array as $a){

    if (strpos($a['dat4'],'1900-01-01') !== false){
        $dat4 = '';
    }else{
        $dat4 = Yii::$app->formatter->asDate(trim($a['dat4']));
    }

    if (strpos($a['dat3'],'1900-01-01') !== false){
        $dat3 = '';
    }else{
        $dat3 = Yii::$app->formatter->asDate(trim($a['dat3']));
    }

    if (strpos($a['dat2'],'1900-01-01') !== false){
        $dat2 = '';
    }else{
        $dat2 = Yii::$app->formatter->asDate(trim($a['dat2']));
    }

    if (strpos($a['dat1'],'1900-01-01') !== false){
        $dat1 = '';
    }else{
        $dat1 = Yii::$app->formatter->asDate(trim($a['dat1']));
    }

    if (strpos($a['dat'],'1900-01-01') !== false){
        $dat = '';
    }else{
        $dat = Yii::$app->formatter->asDate(trim($a['dat']));
    }




    if (strpos($a['rozd'],'1900-01-01') !== false){
        $rozd = '';
    }else{
        $rozd = Yii::$app->formatter->asDate(trim($a['rozd']));
    }

    $line++;
    $sheet->getRowDimension($line)->setRowHeight(30);
    $sheet->getStyle("A{$line}:J{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $iteration)->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", $a['name'])->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $sheet->setCellValue("C{$line}", $rozd)->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("D{$line}", $dat4."\n".$a['year4']." ".$a['year4_2']." ".$a['progress4'])
        ->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("E{$line}", $dat3."\n".$a['year3']." ".$a['year3_2']." ".$a['progress3'])
        ->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("F{$line}", $dat2."\n".$a['year2']." ".$a['year2_2']." ".$a['progress2'])
        ->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("G{$line}", $dat1."\n".$a['year1']." ".$a['year1_2']." ".$a['progress1'])
        ->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("H{$line}", $dat."\n".$a['currentyear1']." ".$a['currentyear1_2']." ".$a['progress'])
        ->getStyle("H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("I{$line}", $a['medotvod'])
        ->getStyle("I{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("J{$line}", $a['coment'])
        ->getStyle("J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(1);
    $sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}

//$sheet->getStyle("G{$line}")->getFont()->setSize(9);


//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:J{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);

$name_file = "Журнал р.Манту ".$name_group;

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;