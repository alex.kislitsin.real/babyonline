<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 28.03.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(10);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.4);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);

$sheet->getColumnDimension('A')->setWidth(4);
$sheet->getColumnDimension('B')->setWidth(40);
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->getColumnDimension('D')->setWidth(12);

$sheet->getColumnDimension('E')->setWidth(40);
$sheet->getColumnDimension('F')->setWidth(16);
$sheet->getColumnDimension('G')->setWidth(16);
$sheet->getColumnDimension('H')->setWidth(10);


$name_file = "Сотрудники";

$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,2);


$line = 1;
$sheet->getRowDimension($line)->setRowHeight(30);
$sheet->setCellValue("A{$line}", $name_file);
$sheet->mergeCells("A{$line}:H{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);
$sheet->getStyle("A{$line}:H{$line}")->applyFromArray($style_border_all);

$p = 0;
foreach ($array as $item){
    !empty(trim($item['pitanie'])) ? $p++ : null;
}

$line++;
$sheet->getRowDimension($line)->setRowHeight(33);
$sheet->getStyle("A{$line}:H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:H{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:H{$line}")->getFont()->setBold(true)->setSize(10);
$sheet->getStyle("A{$line}:H{$line}")->applyFromArray($style_border_all);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", "ФИО")->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", "Должность")->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", "Дата\nрождения")->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", "Адрес")->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", "Телефон")->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", "Дата приема\nна работу")->getStyle("G{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("H{$line}", "Пит.\n{$p}")->getStyle("H{$line}")->getAlignment()->setWrapText(true);
//$sheet->setCellValue("I{$line}", "Пропуски\nпо болезни")->getStyle("I{$line}")->getAlignment()->setWrapText(true);
//$sheet->setCellValue("J{$line}", '%')->getStyle("J{$line}")->getAlignment()->setWrapText(true);
//$sheet->setCellValue("K{$line}", "Пропуски\nпрочее")->getStyle("K{$line}")->getAlignment()->setWrapText(true);
//$sheet->setCellValue("L{$line}", '%')->getStyle("L{$line}")->getAlignment()->setWrapText(true);
//$sheet->setCellValue("M{$line}", "Ниразу\nне болели")->getStyle("M{$line}")->getAlignment()->setWrapText(true);
//$sheet->setCellValue("N{$line}", "Индекс\nздоровья")->getStyle("N{$line}")->getAlignment()->setWrapText(true);



$iii = 1;
foreach($array as $a){

    $line++;
    $sheet->getRowDimension($line)->setRowHeight(25);
    $sheet->getStyle("A{$line}:H{$line}")->applyFromArray($style_border_all);
    $sheet->getStyle("A{$line}:H{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet->setCellValue("A{$line}", $iii)->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", trim($a['name']))->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true);
    $sheet->setCellValue("C{$line}", trim($a['dol']))->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true);
    $sheet->setCellValue("D{$line}", date('d.m.Y', strtotime($a['rozdso'])))->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("E{$line}", trim($a['address']))->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true);
    $sheet->setCellValue("F{$line}", trim($a['tel']))->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("G{$line}", date('d.m.Y', strtotime($a['inS'])))->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("H{$line}", (!empty(trim($a['pitanie'])) ? '+' : ''))->getStyle("H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


    $iii++;
}



//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:H{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);



header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;