<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 06.02.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(9);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.5);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);

$sheet->getColumnDimension('A')->setWidth(5);
$sheet->getColumnDimension('B')->setWidth(5);
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->getColumnDimension('D')->setWidth(18);
$sheet->getColumnDimension('E')->setWidth(23);
$sheet->getColumnDimension('F')->setWidth(11);
$sheet->getColumnDimension('G')->setWidth(27);
$sheet->getColumnDimension('H')->setWidth(21);
$sheet->getColumnDimension('I')->setWidth(16);
$sheet->getColumnDimension('J')->setWidth(22);

$array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
if (count($array_gruppa)>0 && $id_gruppa != 0){
    $name_group = 'группа № '.$id_gruppa.' '.trim($array_gruppa[$id_gruppa]).' ('.count($array).')';
}else{
    $name_group = '';
}

if ($vozrast == 0){
    $vstavka_vozrast = '';
}else{
    switch($vozrast){
        case 2:
        case 3:
        case 4:
        $vstavka_vozrast = 'возраст '.$vozrast.' года';
        break;
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        $vstavka_vozrast = 'возраст '.$vozrast.' лет';
        break;

    }
}

$line = 1;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", 'Список детей '.$name_group.' '.$vstavka_vozrast);
$sheet->mergeCells("A{$line}:J{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);

//$line++;//2
//$sheet->getRowDimension($line)->setRowHeight(20);
$line++;//2
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", "№\nгр")->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", 'Фамилия')->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", 'Имя')->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", 'Отчество')->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", "Дата\nрожд")->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", 'Адрес')->getStyle("G{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("H{$line}", 'Полис')->getStyle("H{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("I{$line}", 'Снилс')->getStyle("I{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("J{$line}", 'Примечание')->getStyle("J{$line}")->getAlignment()->setWrapText(true);

$sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:J{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:J{$line}")->applyFromArray($style_border_all);

//$line++;//12
$iteration = 1;
foreach($array as $a){
    $m = explode(' ',trim($a['name']));

    $line++;
    $sheet->getRowDimension($line)->setRowHeight(25);
    $sheet->getStyle("A{$line}:J{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $iteration)->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", $a['id_gruppa'])->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("C{$line}", $m[0])->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(1);
    $sheet->setCellValue("D{$line}", $m[1])->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(1);
    $sheet->setCellValue("E{$line}", trim($a['otche']))->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(1);
    $sheet->setCellValue("F{$line}", $a['rozd'])->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("G{$line}", trim($a['adress']))->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(1);
    $sheet->getStyle("G{$line}")->getFont()->setSize(9);
    $sheet->setCellValue("H{$line}", $a['polis'])->getStyle("H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("I{$line}", $a['snils'])->getStyle("I{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->getStyle("A{$line}:J{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}




//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:J{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);

$name_file = "Список детей ".$name_group.' '.$vstavka_vozrast;

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;