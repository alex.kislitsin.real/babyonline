<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 26.01.20
 * Time: 15:57
 */

function m2t($millimeters){
    return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
}

$PHPWord = new \PhpOffice\PhpWord\PhpWord();

$PHPWord->setDefaultFontName('Times New Roman');
$PHPWord->setDefaultFontSize(8.5);

$sectionStyle = array('orientation' => null,
    'marginLeft' => m2t(8),
    'marginRight' => m2t(8),
    'marginTop' => m2t(8),
    'marginBottom' => m2t(8),
//    'colsNum' => 1,
//    'breakType' => 'continuous'
);
$section = $PHPWord->addSection($sectionStyle);

$cellStyle = array(
    'borderTopSize' => 1,
    'borderRightSize' => 1,
    'borderBottomSize' => 1,
    'borderLeftSize' => 1,
    'borderTopColor' => '#000000',
    'borderRightColor' => '#000000',
    'borderBottomColor' => '#000000',
    'borderLeftColor' => '#000000',
);


$transaction = Yii::$app->db->beginTransaction();
try{
    if ($id_gruppa==0){
        $childs = Yii::$app->db->createCommand('select * from deti where `out` is null')->queryAll();
        $file_name = 'Все группы';
    }else{
        $childs = Yii::$app->db->createCommand('select * from deti where `out` is null and id_gruppa=:id_gruppa',[
            'id_gruppa' => $id_gruppa
        ])->queryAll();
        $file_name = 'группа № '.$id_gruppa;
    }
    $rostves = Yii::$app->db->createCommand('select * from rostves where year(dat) = year(now())')->queryAll();
    $transaction->commit();
}catch (Exception $e){
    $transaction->rollBack();
}

foreach($childs as $key => $child){



//Yii::debug($rostves);
foreach($rostves as $r){
    if ($child['id']==$r['id_child']){
        $rost = str_replace('.',',',$r['rost']);
        $ves = str_replace('.',',',$r['ves']);
    }
}

$fio_name = explode(' ',$child['name']);
$family = $fio_name[0];
$name = $fio_name[1];

$under_text_m = '';
$under_text_g = '';
if (trim($child['pol'])=='М')$under_text_m = ['underline' => 'single'];
if (trim($child['pol'])=='Ж')$under_text_g = ['underline' => 'single'];

$rost4 = '_______';
$ves4 = '_______';
$rost5 = '_______';
$ves5 = '_______';

$date_rozd = date('Y',strtotime($child['rozd']));
if (date('Y')-$date_rozd > 4){
    $rost5 = !empty($rost) ? $rost : $rost5;
    $ves5 = !empty($ves) ? $ves : $ves5;
}else{
    $rost4 = !empty($rost) ? $rost : $rost4;
    $ves4 = !empty($ves) ? $ves : $ves4;
}





    $table = $section->addTable();
    $table->addRow(m2t(6));
    $table->addCell(m2t(50),$cellStyle)->addText("        Наименование учреждения",['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);

    $table_run = $table->addCell(m2t(90));
    $table_run->addText("                       ТАЛОН АМБУЛАТОРНОГО ПАЦИЕНТА<w:br/>             (заполняется на 1 случай поликлинического обслуживания)",['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);

    $table_run = $table->addCell(m2t(50),$cellStyle);
    $table_run->addText("№",['spaceAfter' => 0,'spaceBefore' => 0]);
    $table_run->addText("             054654",['size' => 16],['spaceAfter' => 0,'spaceBefore' => 0]);

    $table->addRow(100);
    $table->addCell(m2t(140))->addText("");

    $table = $section->addTable();
    $table->addRow(m2t(6));
    $table->addCell(m2t(140),$cellStyle)->addText("№ кабинета ______ Дата ___ | ___ | _____ Время приема ______ час.____ мин.<w:br/>Врач: специальность                         Ф.И.О.",['spaceAfter' => 0,'spaceBefore' => 0]);
    $table_run = $table->addCell(m2t(50));
    $table_run->addText("Форма № 025-10/у-97",['bold' => true,'spaceAfter' => 0,'spaceBefore' => 0]);
    $table_run->addText("В ред. Пр. МЗСР от 22.11.04 г. № 255",['size' => 7],['spaceAfter' => 0,'spaceBefore' => 0]);

//    if ($key != count($childs)-1)$section->addPageBreak();
    break;
}



header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="Форма 30 '.$file_name.'  ('.date('d.m.Y h:i:s').').docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
ob_clean();
$objWriter->save("php://output");
exit;