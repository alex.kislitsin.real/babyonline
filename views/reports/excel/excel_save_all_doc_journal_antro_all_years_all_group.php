<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 28.03.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(9);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.6);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);
$width_col = 7;
$sheet->getColumnDimension('A')->setWidth(4);
$sheet->getColumnDimension('B')->setWidth(25);
$sheet->getColumnDimension('C')->setWidth(11);
$sheet->getColumnDimension('D')->setWidth($width_col);
$sheet->getColumnDimension('E')->setWidth($width_col);
$sheet->getColumnDimension('F')->setWidth($width_col);
$sheet->getColumnDimension('G')->setWidth($width_col);
$sheet->getColumnDimension('H')->setWidth($width_col);
$sheet->getColumnDimension('I')->setWidth($width_col);
$sheet->getColumnDimension('J')->setWidth($width_col);
$sheet->getColumnDimension('K')->setWidth($width_col);
$sheet->getColumnDimension('L')->setWidth($width_col);
$sheet->getColumnDimension('M')->setWidth($width_col);
$sheet->getColumnDimension('N')->setWidth(5);

$array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
if (count($array_gruppa)>0){
    $name_group = 'группа № '.$id_gruppa.' '.trim($array_gruppa[$id_gruppa]).' ('.count($array).')';
}else{
    $name_group = '';
}
if ($id_gruppa==0){
    $name_file = "Журнал антропометрии все года все группы";
}else{
    $name_file = "Журнал антропометрии все года ".$name_group;
}


$line = 1;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", $name_file);
$sheet->mergeCells("A{$line}:N{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);

//$line++;//2
//$sheet->getRowDimension($line)->setRowHeight(20);
$line++;//2
$sheet->getRowDimension($line)->setRowHeight(25);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", 'Фамилия Имя')->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", "Дата\nрождения")->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", date('Y')-4)->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("D{$line}:E{$line}");
$sheet->setCellValue("F{$line}", date('Y')-3)->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("F{$line}:G{$line}");
$sheet->setCellValue("H{$line}", date('Y')-2)->getStyle("H{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("H{$line}:I{$line}");
$sheet->setCellValue("J{$line}", date('Y')-1)->getStyle("J{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("J{$line}:K{$line}");
$sheet->setCellValue("L{$line}", date('Y'))->getStyle("L{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("L{$line}:M{$line}");
$sheet->setCellValue("N{$line}", "№ гр.")->getStyle("N{$line}")->getAlignment()->setWrapText(true);
$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:N{$line}")->applyFromArray($style_border_all);

$line++;
$sheet->getRowDimension($line)->setRowHeight(15);
$sheet->setCellValue("A{$line}", "")->getStyle("A{$line}")->getAlignment();
$sheet->setCellValue("B{$line}", '')->getStyle("B{$line}")->getAlignment();
$sheet->setCellValue("C{$line}", "")->getStyle("C{$line}")->getAlignment();
$sheet->setCellValue("D{$line}", 'весна')->getStyle("D{$line}")->getAlignment();
$sheet->setCellValue("E{$line}", 'осень')->getStyle("E{$line}")->getAlignment();
$sheet->setCellValue("F{$line}", 'весна')->getStyle("F{$line}")->getAlignment();
$sheet->setCellValue("G{$line}", 'осень')->getStyle("G{$line}")->getAlignment();
$sheet->setCellValue("H{$line}", 'весна')->getStyle("H{$line}")->getAlignment();
$sheet->setCellValue("I{$line}", 'осень')->getStyle("I{$line}")->getAlignment();
$sheet->setCellValue("J{$line}", 'весна')->getStyle("J{$line}")->getAlignment();
$sheet->setCellValue("K{$line}", 'осень')->getStyle("K{$line}")->getAlignment();
$sheet->setCellValue("L{$line}", 'весна')->getStyle("L{$line}")->getAlignment();
$sheet->setCellValue("M{$line}", 'осень')->getStyle("M{$line}")->getAlignment();
$sheet->setCellValue("N{$line}", '')->getStyle("N{$line}")->getAlignment();



$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:N{$line}")->applyFromArray($style_border_all);

//$iter_id_gruppa = 0;
//$id_gruppa555 = $id_gruppa;
$iteration = 1;
//$ckeck = 0;
foreach($array as $a){

    /*if ($id_gruppa555==0){
        $iter_id_gruppa = $a['id_gruppa'];
        if ($ckeck != $iter_id_gruppa){
            $line++;
            $ckeck = $iter_id_gruppa;
//            $sheet->setMergeCells("A{$line}:M{$line}");
            $sheet->setCellValue("A{$line}",$array_gruppa[$iter_id_gruppa])->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $line++;
        }else{
            $line++;
        }
    }else{
        $line++;
    }*/
    $line++;


    $sheet->getRowDimension($line)->setRowHeight(27);
    $sheet->getStyle("A{$line}:N{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $iteration)->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", $a['name'])->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1)->setWrapText(true);
    $sheet->setCellValue("C{$line}", $a['rozd'])->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("D{$line}", $a['rost_v4']."\n".$a['ves_v4'])->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("E{$line}", $a['rost_o4']."\n".$a['ves_o4'])->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("F{$line}", $a['rost_v3']."\n".$a['ves_v3'])->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("G{$line}", $a['rost_o3']."\n".$a['ves_o3'])->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("H{$line}", $a['rost_v2']."\n".$a['ves_v2'])->getStyle("H{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("I{$line}", $a['rost_o2']."\n".$a['ves_o2'])->getStyle("I{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("J{$line}", $a['rost_v1']."\n".$a['ves_v1'])->getStyle("J{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("K{$line}", $a['rost_o1']."\n".$a['ves_o1'])->getStyle("K{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);

    $sheet->setCellValue("L{$line}", $a['rost_v']."\n".$a['ves_v'])->getStyle("L{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("M{$line}", $a['rost_o']."\n".$a['ves_o'])->getStyle("M{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("N{$line}", $a['id_gruppa'])->getStyle("N{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);




    $sheet->getStyle("A{$line}:N{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}
if (count($array==28)){
    $sheet->getPageMargins()->setBottom(0);
}

//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:N{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);



header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;