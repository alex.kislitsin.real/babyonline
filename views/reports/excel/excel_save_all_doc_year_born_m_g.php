<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 28.03.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(9);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.4);
$sheet->getPageMargins()->setBottom(0.4);
$sheet->getPageMargins()->setLeft(0.7);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2);

$sheet->getColumnDimension('A')->setWidth(20);
$sheet->getColumnDimension('B')->setWidth(20);
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->getColumnDimension('D')->setWidth(20);


$name_file = "Годы рождения и м/ж";

$line = 1;
$sheet->getRowDimension($line)->setRowHeight(35);
$sheet->setCellValue("A{$line}", $name_file);
$sheet->mergeCells("A{$line}:D{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);
$sheet->getStyle("A{$line}:D{$line}")->applyFromArray($style_border_all);


foreach($array as $a){

    $line++;
    $sheet->getRowDimension($line)->setRowHeight(17);
    $sheet->getStyle("A{$line}:D{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $a['rozd2'])->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", $a['m'])->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("C{$line}", $a['g'])->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("D{$line}", $a['summa'])->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
}



//$sheet->setBreak("J{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:M{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);



header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;