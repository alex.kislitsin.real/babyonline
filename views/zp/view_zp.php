<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 01.02.20
 * Time: 20:42
 */
$countSum = 0;
$countSum2 = 0;

foreach ($array as $value) {
    $countSum += $value['summ2'];
    $countSum2 += $value['posle'];
}
?>



<div class="well" style="margin: 2px;text-align: center;padding: 0">
    <span style="font-size: 25px;color: red"><?= str_replace('.',',',$countSum); ?></span>   <span style="font-size: 25px">   / </span> <span style="font-size: 25px;color: green"><?= str_replace('.',',',$countSum2); ?></span></div>


<div class="well" style="margin: 2px;text-align: center;padding: 0;font-size: 20px" id="zp_name" hidden="hidden"></div>

<div style="margin: 2px" hidden="hidden" id="zp_div_but_add_oklad">
<?= Html::button('Добавить оклад', ['class'=>'btn btn-sm btn-success','id' => 'zp_but_add_oklad','style' => ['width' => '100%']]); ?>
</div>


<div class="well" style="margin: 2px;text-align: center;padding: 0" hidden="hidden" id="zp_int">
    <span style="font-size: 18px;color: red" id="zp_span1"></span>   <span style="font-size: 18px">   / </span> <span style="font-size: 18px;color: green" id="zp_span2"></span></div>

<div id="zptable1" style="margin: 5px">

</div>

<?php
Modal::begin([
    'id' => 'modal_zp_add',
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">Добавить должностной оклад</h4>',
    'size' => Modal::SIZE_DEFAULT,
    'footer' => '<button type="submit" class="btn btn-success btn-md" id="zp_but_add_oklad_save">Сохранить изменения</button>',
]);
?>
<?= $this->render('zp_form_add_oklad') ?>
<?php Modal::end(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'form_modal_zp_hide',
    'method' => 'POST',
    'action' => ['zp/zp'],

]); ?>
<?= $form->field($model_id, 'id')->hiddenInput([
    'id' => 'id_hidden_modal_zp',
])->label(false); ?>
<?= $form->field($model, 'id_so')->hiddenInput([
    'id' => 'id_hidden_modal_zp_id_so',
])->label(false); ?>
<?php ActiveForm::end(); ?>

<?php
$scr = <<<JS
$(function(){

    $('#zp_but_add_oklad').on('click',function(){
        $('#zp_form_add_oklad').trigger('reset');
//        $('#zp_form_ppk').val(0);
//        $('#zp_form_children').val(0);
//        $('#zp_form_vred').val(0);
        var array_all;
        var arr = $('#form_modal_zp_hide');
        $('#id_hidden_modal_zp').val(5);
        var test = $('#form_modal_zp_hide').serializeArray();
//        console.log(test);return;

        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : test
        }).done(function(response) {
            array_all = $.parseJSON(response);
            console.log(array_all);//return;
            $('#zp_form_id_oklad').empty().prepend($('<option value="">Выберите оклад</option>'));
            $.each(array_all, function(index, value) {
//                console.log(value.name_dol);
                $('#zp_form_id_oklad').append($('<option value="'+value.id+'">'+value.name_dol+' ('+value.oklad+')</option>'));
            });
            $('#modal_zp_add').modal('show');
//            $('#zp_div_but_add_oklad_form').show();

        }).fail(function() {
            console.log('not1');
        });

        return false;

    });

    /*$('#zp_form_id_oklad').on('change',function(){

    });*/

    $('#zp_but_add_oklad_save').on('click',function(){
        var dr = $('#zp_form_id_oklad').val();
        var dr_percent = $('#zp_form_percent').val();
        console.log(dr);
        if(dr.length < 1 || dr_percent.length < 1){
            alert('Выберите оклад и заполните % оклада');
            return;
        }

        $('#id_hidden_modal_zp').val(6);
        var form = $('#zp_form_add_oklad,#form_modal_zp_hide').serializeArray();
        console.log(form);//return;

        var nameso = $('#zp_name').text().trim();

        var arr = $('#form_modal_zp_hide');
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
                $("*").LoadingOverlay("hide");
    //            console.log(response);
                if(response==400){
    //                $('#id_show_zp_form').fadeIn();
//                    $('#zptable1').hide();
                }else{
                    $('.modal').modal('hide');

                    $('#renderAjax_zp_table_boss').html(response).show();

                    $('#antro_table1 tr').each(function(){
                    if($(this).text().indexOf(nameso) > -1){
                            var summ2 = $(this).data('summ2');
                            var posle = $(this).data('posle');
                            $('#zp_int').hide(function(){
                                $('#zp_span1').text(summ2);
                                $('#zp_span2').text(posle);
                            }).show();
                        }
                    });
                    console.log(111111);
                    $('#id_hidden_modal_zp').val(0);
                    var form = $('#form_modal_zp_hide').serializeArray();
                    $.ajax({
                        type : arr.attr('method'),
                        url : arr.attr('action'),
                        data : form
                    }).done(function(response) {
                            $("*").LoadingOverlay("hide");
                            if(response==400){
                                $('#zptable1').hide();
                            }else{
                                $('#zptable1').html(response).show();
                                $('#zp_div_but_add_oklad').show();
                                $('#zp_name').text(nameso).show();
                            }

                        }).fail(function() {
                            $("*").LoadingOverlay("hide");
                            console.log('not');
                        });


    //                $('#id_show_zp_form').hide();
                }

            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });


        return false;


    });


});
JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>

