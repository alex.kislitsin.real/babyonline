<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.01.20
 * Time: 12:24
 */
//debug($array);
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$width = '2.5%';
//$year = $model_y->name;
//$month = $model_m->name;
//$array = array(1,2,3,4,5,0,0,0,0,0,0,0,0,0,0);
if (count($array) > 14){
    $height = ';height: 82vh';
}else{
    $height = '';
}
//$json_array = json_encode($array);

/*foreach($array5 as $ar5){

}*/

/*$array_count = array_count_values($array5);
$duplicates = [];
foreach ($array_count as $k=>$v){
    if ($v > 1) $duplicates[] = $k;
}
print_r($duplicates);*/
/*echo count($array5);
$arraye = array_unique($array5, SORT_REGULAR);
echo count($arraye);*/

?>
<div class="my_table not_selected_text_on_block" style="height: 5vh;overflow-y: scroll;background-color: #eeeeee">
    <table>
        <tr>
            <td class="not_item" style="width: 5%;text-align: center">№</td>
            <td class="not_item">ФИО</td>
            <td class="not_item" style="width: 30%">Должность</td>
            <td class="not_item" style="width: 12%">До вычета</td>
            <td class="not_item" style="width: 12%">На руки</td>
        </tr>
    </table>
</div>
<div class="my_table my_table2 not_selected_text_on_block" style="overflow-y: scroll<?= $height ?>" id="antro_table1">
    <table class="table-striped table-bordered">

        <?php
        $i = 1;
        $iter = null;
        $save = 0;
        foreach($array as $q){

            if($q['summ2'] < 1){
                $check = 0;
                $sum2 = '';
                $posle = '';
            }else{
                $check = 1;
                $sum2 = str_replace('.',',',$q['summ2']);
                $posle = str_replace('.',',',$q['posle']);
            }

                echo '<tr data-id_s="'.$q['id_s'].'" data-check="'.$check.'" data-nameso="'.$q['nameso'].'" data-summ2="'.str_replace('.',',',$q['summ2']).'" data-posle="'.str_replace('.',',',$q['posle']).'">
                <td style="width: 5%">'.$i.'</td>
                <td id="id_n">'.trim($q['nameso']).'</td>
                <td style="width: 30%">'.$q['dol'].'</td>
                <td style="width: 12%">'.$sum2.'</td>
                <td style="width: 12%">'.$posle.'</td>
            </tr>';
            $i++;
        }

        ?>

    </table>
</div>



<?php

$script = <<<JS



$(function(){

//    var massiv = eval($json_array);
//    console.log(massiv);

    $('#antro_table1 tr').on('click',function(){

    var id_s = $(this).data('id_s');
    var nameso = $(this).data('nameso');
    var summ2 = $(this).data('summ2');
    var posle = $(this).data('posle');
    var check = $(this).data('check');

    $('#zp_name').hide().text(nameso).fadeIn();
    $('#zp_div_but_add_oklad').fadeIn();

    $('#id_hidden_modal_zp_id_so').val(id_s);
    console.log(check);
    if(check == 1){
//        console.log(summ2);
        $('#zp_int').fadeOut(function(){
            $('#zp_span1').text(summ2);
            $('#zp_span2').text(posle);
        }).fadeIn();
    }else{
        $('#zp_int').fadeOut(function(){
            $('#zp_span1').text('');
            $('#zp_span2').text('');
        });
        $('#zptable1').hide();
        return;
    }

    console.log('dalee');

    $('#id_hidden_modal_zp').val(0);

    var form = $('#form_modal_zp_hide').serializeArray();
    var arr = $('#form_modal_zp_hide');
//    console.log(form);return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            if(response==400){
//                $('#id_show_zp_form').fadeIn();
                $('#zptable1').hide();
            }else{
                $('#zptable1').html(response).fadeIn();
//                $('#id_show_zp_form').hide();
            }

        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });

    return false;
});
})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

