<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;

$model = new \app\models\Zp();
$array_okladi = array();

$form = ActiveForm::begin([
    'id' => 'zp_form_add_oklad',
    'method' => 'POST',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'action' => ['zp/zp'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-5',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-7',
        ],
    ],
]); ?>

<?= $form->field($model,'id_oklad'/*,[
    'addon' => [
        'prepend' => [
            'content' => Html::button('Цена', ['class'=>'btn btn-default btn-md','id' => '#','disabled' => 'disabled','style' => ['width' =>  '65px']]),
            'asButton' => true
        ]
    ]
]*/)->dropDownList($array_okladi,[
    'id' => 'zp_form_id_oklad',
//        'style' => 'width:420px'
//        'placeholder' => 'Фамилия Имя',
//        'readonly'=> true
]) ?>
<?= $form->field($model,'percent')->textInput([
    'id' => 'zp_form_percent',
    'type' => 'number',
    'min' => 0,
    'max' => 100,
    'step' => 25,
    'placeholder' => '% оклада'
]) ?>
<?= $form->field($model,'ppk')->textInput([
    'id' => 'zp_form_ppk',
    'type' => 'number',
    'min' => 0,
    'max' => 100,
    'step' => 0.01,
    'placeholder' => 'ппк'
]) ?>

<?= $form->field($model, 'children')->textInput([
    'id' => 'zp_form_children',
    'type' => 'number',
    'min' => 0,
    'max' => 100000,
    'step' => 100,
    'placeholder' => 'Вычет на детей'
]) ?>
<?= $form->field($model, 'vred')->textInput([
    'id' => 'zp_form_vred',
    'type' => 'number',
    'min' => 0,
    'max' => 100,
    'step' => 1,
    'placeholder' => 'Вредность'
]) ?>



<?php ActiveForm::end(); ?>