<?php

//use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

?>

<?/*= $this->render('_search') */?>

<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <?= Html::button('#', ['class'=>'btn btn-md btn-default btn-block','id' => '#','disabled' => 'disabled']); ?>
        </div>
        <div class="rep_boss_up_panel_1" data-id7="0">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => false,//
            ])?>
            <?=$form->field($model, 'name')
                ->textInput([
                    'autofocus' => true,
                    'id' => 'search555',
                    'placeholder' => 'Введите любые данные для поиска',
                ])->label(false); ?>

            <?//= $form->field($model_id,'id')->hiddenInput()->label(false); ?>
            <?php $form = ActiveForm::end()?>
        </div>
    </div>
    <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
        <table>
            <tr>
                <td style="width: 3%">№</td>
                <td style="width: 2%">!</td>
                <td>Фамилия Имя</td>
                <td style="width: 8%">Группа</td>
                <td style="width: 6%">№ л.счета</td>
                <td style="width: 7%">Дата зачис.</td>
                <td style="width: 7%">Дата отчис.</td>
                <td style="width: 7%">Дата рожд.</td>
<!--                <td style="width: 11%">Должность</td>-->
<!--                <td style="width: 7%">На питании</td>-->
                <td style="width: 3%">Пол</td>
<!--                <td style="width: 8%">Телефон</td>-->
                <td style="width: 15%">Адрес</td>
                <td style="width: 12%">Полис</td>
                <td  style="width: 10%">СНИЛС</td>
            </tr>
        </table>
    </div>
    <div class="rep_boss_down" id="table_sotrudniki">
        <?= $this->render('table',compact(
            'array'
            )) ?>
    </div>
</div>