<?php
use app\models\Id;
use app\models\Zp;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$model_id = new Id();
?>


<div id="renderAjax_cru_table_boss33" style="height: 87vh">

</div>


<?php $form = ActiveForm::begin([
    'id' => 'form_cru_hide555',
    'method' => 'POST',
    'action' => ['crujki/cru'],

]); ?>
<?= $form->field($model_id, 'id')->hiddenInput([
    'id' => 'id_hidden_modal_cru555',
])->label(false); ?>
<?php ActiveForm::end(); ?>


<?php
$scr = <<<JS
    $(function(){
//        var p = prompt('пароль для входа');
        var p = 1;
        var arr = $('#form_cru_hide555');
        $('#id_hidden_modal_cru555').val(15);
//        $('#id_hidden_modal_cru_id_so555').val(p);
        var form = $('#form_cru_hide555').serializeArray();
        form.push({name: 'idso',value: p});
        console.log(form);
//        return false;
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
                $("#renderAjax_cru_table_boss33").html(response);
            }).fail(function() {
                console.log('error');
            });
        return false;
    });

JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>
