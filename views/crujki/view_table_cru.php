<div class="my_table my_table2 not_selected_text_on_block" id="table_crijki"><!--D:\OSPanel\domains\inet\web\css\reports\diseases\table.css-->
    <table class="table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 45%">Название</th>
            <th style="width: 39%">Педагог</th>
            <th style="width: 8%">Цена</th>
            <th style="width: 8%">%</th>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach($array as $q){
            if ($q['pn']>0){
                $pn = 'пн ';
            }else{
                $pn = '';
            }
            if ($q['vt']>0){
                $vt = 'вт ';
            }else{
                $vt = '';
            }
            if ($q['sr']>0){
                $sr = 'ср ';
            }else{
                $sr = '';
            }
            if ($q['ch']>0){
                $ch = 'чт ';
            }else{
                $ch = '';
            }
            if ($q['pt']>0){
                $pt = 'пт';
            }else{
                $pt = '';
            }


            echo '<tr data-category_id="'.$q['category_id'].'" data-id="'.$q['id'].'" data-name="'.$q['name'].'" data-id_so="'.$q['id_so'].'" data-price="'.$q['price'].'" data-pn="'.$q['pn'].'" data-vt="'.$q['vt'].'" data-sr="'.$q['sr'].'" data-ch="'.$q['ch'].'" data-pt="'.$q['pt'].'" data-zarplata="'.$q['zarplata'].'">
                <td style="width: 45%" id="id_n"><span style="font-size:16px">'.trim($q['name']).'</span>'."<br/>".'<span style="font-size:14px;color: #636363">'.$pn.$vt.$sr.$ch.$pt.'</span></td>
                <td style="width: 39%">'.trim($q['ped']).'</td>
                <td style="width: 8%">'.trim($q['price']).'</td>
                <td style="width: 8%">'.$q['zarplata'].'</td>
            </tr>';
        }

        ?>
        </tbody>
    </table>
</div>

<?php
$scr = <<< JS

$(function() {

    $('#table_crijki tr').on('click',function(){

//        $('#but_cru_add input').attr('disabled',true);

        var id = $(this).data('id');
        var category_id = $(this).data('category_id');
        var name = $(this).data('name');
        var id_so = $(this).data('id_so');
        var price = $(this).data('price');
        var pn = $(this).data('pn');
        var vt = $(this).data('vt');
        var sr = $(this).data('sr');
        var ch = $(this).data('ch');
        var pt = $(this).data('pt');
        var zarplata = $(this).data('zarplata');

        console.log(category_id);
//        console.log(name);
//        console.log(id_so);
//        console.log(price);
//        console.log(pn);
//        console.log(vt);
//        console.log(sr);
//        console.log(ch);
//        console.log(pt);

        $('#hidden_pole_cru_id').val(id);

        if(name.length>0){
            $('#name_cru_form').val(name);
        }else{
            $('#name_cru_form').val('');
        }
        if(id_so>0){
            $('#drop_pedagog_cru_form').val(id_so);
        }else{
            $('#drop_pedagog_cru_form').val('');
        }
        $('#price_cru_form').val(price);
        $('#zp_cru_form').val(zarplata);

        if(pn > 0){
            $('#but_pn').addClass('active');
        }else{
            $('#but_pn').removeClass('active');
        }
        if(vt > 0){
            $('#but_vt').addClass('active');
        }else{
            $('#but_vt').removeClass('active');
        }
        if(sr > 0){
            $('#but_sr').addClass('active');
        }else{
            $('#but_sr').removeClass('active');
        }
        if(ch > 0){
            $('#but_ch').addClass('active');
        }else{
            $('#but_ch').removeClass('active');
        }
        if(pt > 0){
            $('#but_pt').addClass('active');
        }else{
            $('#but_pt').removeClass('active');
        }

        $('#drop_category_cru').val(category_id);

        return false;
    });





});

JS;
$this->registerJs($scr, yii\web\View::POS_END);
?>