<div class="alert alert-info" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Error:</span>
    Добавьте кружки! <br/><br/>Для этого заполните поля слева и нажмите кнопку "сохран / добав" (сохранить/добавить). Чтобы отредактировать внесенный кружок - нажмите на него справа,
    данные о кружке появятся слева в полях, внесите изменения и нажмите кнопку "сохран / добав".
</div>