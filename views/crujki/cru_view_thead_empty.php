<div class="alert alert-info" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Error:</span>
    Внесите кружки! <br/><br/>Для этого нажмите на кнопку "Добавить или изменить кружки".
</div>