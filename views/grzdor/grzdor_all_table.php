<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 07.01.20
 * Time: 20:58
 */
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

/** @var $model_grzdor app\models\Grzdor */

?>
<div class="block_listview_sp_boss">
    <div class="block_listview_sp" id="grzdor_left_table">
        <?= $this->render('grzdor_table',compact(
            'array_names',
            'array',
            'model_grzdor',
            'model_id',
            'array_number_grzdor',
            'array_number_fisculture'
        )) ?>
    </div>
    <div class="block_listview_sp" style="overflow: auto">
        <?= $this->render('grzdor_table_svodka',compact('array2')) ?>
    </div>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'form_modal_grzdor_mebel2',
//    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<?= $form->field($model_id, 'id')->hiddenInput([
    'id' => 'id_hidden_modal_grzdor',
])->label(false); ?>
<?= $form->field($model_grzdor, 'child_id')->hiddenInput([
    'id' => 'id_child_modal_grzdor',
])->label(false); ?>
<?= $form->field($model_grzdor, 'number_grzdor')->hiddenInput([
    'id' => 'number_grzdor_modal_grzdor',
])->label(false); ?>
<?= $form->field($model_grzdor, 'number_fisculture')->hiddenInput([
    'id' => 'number_fisculture_modal_grzdor',
])->label(false); ?>



<?php ActiveForm::end(); ?>
<?php
Modal::begin([
    'id' => 'modal_grzdor_mebel',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="header_modal_grzdor_mebel"></h4>',
    'size' => Modal::SIZE_DEFAULT,
    'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_grzdor_mebel">Удалить замеры</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_grzdor_mebel">Сохранить изменения</button>',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form_modal_grzdor_mebel',
    'method' => 'POST',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'action' => ['grzdor/grzdor'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-4',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-8',
        ],
    ],
]); ?>

<?= $form->field($model_grzdor, 'name')->textInput([
    'id' => 'name_modal_grzdor',
    'placeholder' => 'Фамилия Имя',
    'readonly'=> true
]) ?>

<?/*= $form->field($model_grzdor,'number_grzdor')->dropDownList($array_number_grzdor,[
    'id' => 'drop_grzdor_year',
]) */?>

    <?php $h = 74.5 ?>

    <div style="position: absolute;right: 69%;top: 27%">Группа здоровья</div>
    <div class="btn-group" data-toggle="buttons" style="position: absolute;right: 2.5%;top: 25%">
        <label class="btn btn-default" id="but_1" style="width: <?= $h ?>px">
            <input type="radio" name="grr" value="1" autocomplete="off">1
        </label>
        <label class="btn btn-default" id="but_2" style="width: <?= $h ?>px">
            <input type="radio" name="grr" value="2" autocomplete="off">2
        </label>
        <label class="btn btn-default" id="but_3" style="width: <?= $h ?>px">
            <input type="radio" name="grr" value="3" autocomplete="off">3
        </label>
        <label class="btn btn-default" id="but_4" style="width: <?= $h ?>px">
            <input type="radio" name="grr" value="" autocomplete="off">4
        </label>
        <label class="btn btn-default" id="but_5" style="width: <?= $h ?>px">
            <input type="radio" name="grr" value="5" autocomplete="off">5
        </label>
    </div>
    <br/>
    <br/>
<?= $form->field($model_grzdor, 'comment')->textArea([
    'id' => 'comment_modal_grzdor',
    'rows' => 4
]) ?>


    <br/>
    <div style="position: absolute;right: 69%;top: 80%">Физкульт. группа</div>
    <div class="btn-group" data-toggle="buttons" style="position: absolute;right: 2.5%;top: 80%">
        <?php
        $h2 = 93;//ширина кнопки
        $i = 1;
        foreach($model_grzdor::fiscultTypeLabels() as $item){?>
            <label class="btn btn-default" id="but_fiscult<?= $i ?>" style="width: <?= $h2 ?>px">
                <input type="radio" name="grr222" value="<?= $i ?>" autocomplete="off"><?= $item ?>
            </label>
        <?php $i++; }

        ?>
    </div>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>

<?php
$script = <<<JS
$(function(){


    $('input:radio[name="grr"]').change(function(){
        $('#number_grzdor_modal_grzdor').val($(this).val());
    });

    $('input:radio[name="grr222"]').change(function(){
        $('#number_fisculture_modal_grzdor').val($(this).val());
    });

    $('#but_save_grzdor_mebel').on('click',function(){

    if($('#number_grzdor_modal_grzdor').val() == 0 || $('#number_fisculture_modal_grzdor').val() == 0){
        alert('Необходимо выбрать обе группы: здоровья и физкультурную !');
        return;
    }

    var s = $("#grzdor_table1").scrollTop();
    $('.modal').modal('hide');
    var form = $('#form_modal_grzdor_mebel,#form_modal_grzdor_mebel2,#form_grzdor_panel_button').serializeArray();
    var arr = $('#form_modal_grzdor_mebel');

    console.log(form);
//    return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
                alert('Ошибка');
            }else{
//                console.log(response);return;
                $('#renderAjax_grzdor_table_boss').html(response);
                $("#grzdor_table1").scrollTop(s);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
    });

    $('#but_del_grzdor_mebel').on('click',function(){

    var s = $("#grzdor_table1").scrollTop();

    $('#id_hidden_modal_grzdor').val(2);
    $('.modal').modal('hide');
    var form = $('#form_modal_grzdor_mebel,#form_modal_grzdor_mebel2,#form_grzdor_panel_button').serializeArray();
//    form.push({name:'Gruppa[name]',value:$('#drop_group_grzdor').val()});
//    form.push({name:'Year[name]',value:$('#drop_grzdor_year').val()});
//    form.push({name:'Month[name]',value:$('#drop_grzdor_month').val()});
    var arr = $('#form_modal_grzdor_mebel');
    console.log(form);
//    return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#renderAjax_grzdor_table_boss').html(response);
                $("#grzdor_table1").scrollTop(s);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
    });


$('#grzdor_table2 tr').on('click',function(){
//    console.log(000);
//    $('#form_modal_grzdor_mebel,#form_modal_grzdor_mebel2').trigger('reset');
    var id_gruppa = $(this).data('id_gruppa');
//    console.log(id_gruppa);//return;
    if(id_gruppa.length < 1)return;
    $('#drop_group_grzdor').val(id_gruppa);

    var form = $('#form_grzdor_panel_button').serializeArray();
    form.push({name: 'Id[id]',value: 3});
    var arr = $('#form_grzdor_panel_button');
//    console.log(form);
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#grzdor_left_table').html(response);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
});



})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>