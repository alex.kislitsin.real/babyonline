<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);



?>

<div class="rep_boss">
    <div class="rep_boss_up_panel">

        <div class="rep_boss_up_panel_1">
            <?php $form = ActiveForm::begin([
                'id' => 'form_grzdor_panel_button',
                'action' => ['grzdor/grzdor'],
                'method' => 'POST',
                'type' => ActiveForm::TYPE_INLINE,
            ])?>
            <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
                'id' => 'drop_group_grzdor',
            ])->label(false) ?>
            <?/*= $form->field($model_y,'name')->dropDownList($array_year,[
                'id' => 'drop_grzdor_year',
            ])->label(false) */?>
            <?= $form->field($model_y, 'name')->hiddenInput([
                'id' => 'year_grzdor_modal_grzdor',
            ])->label(false); ?>


            <div class="btn-group" data-toggle="buttons" style=";height: 40px">
            <?php
            $h = 100;
            foreach($array_year as $yyy){?>
                <label class="btn btn-default <?= $yyy == $model_y->name ? 'active' : '' ?>" id="but_<?= $yyy ?>" style="width: <?= $h ?>px;height: 40px">
                    <input type="radio" name="Year[name]" value="<?= $yyy ?>" autocomplete="off"><?= $yyy ?>
                </label>
            <?php }
            ?>
            </div>

            <?php $form = ActiveForm::end()?>
        </div>
    </div>

    <div id="renderAjax_grzdor_table_boss" style="height: 87vh">
        <?= $this->render('grzdor_all_table',compact(
            'array2',
            'array',
            'model_grzdor',
            'model_id',
            'array_number_grzdor',
            'array_number_fisculture'
        )) ?>
    </div>

</div>

<?php
$scr = <<<JS
$(function(){

$('#drop_group_grzdor,input:radio[name="Year[name]"]').on('change',function(){
//    $('#id_hidden_form_grzdor').val(3);
    var form = $('#form_grzdor_panel_button').serializeArray();
    form.push({name: 'Id[id]',value: 3});
    var arr = $('#form_grzdor_panel_button');
//    console.log(form);return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#renderAjax_grzdor_table_boss').html(response);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
});

/*$('#drop_grzdor_year,#drop_grzdor_month').on('change',function(){
//    $('#id_hidden_form_grzdor').val(0);
    var form = $('#form_grzdor_panel_button').serializeArray();
    form.push({name: 'Id[id]',value: 0});
    var arr = $('#form_grzdor_panel_button');
//    console.log(form);return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#renderAjax_grzdor_table_boss').html(response);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
});*/







});
JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>
