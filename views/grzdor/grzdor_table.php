<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.01.20
 * Time: 12:24
 */
//debug($array);
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$width = '2.5%';
//$year = $model_y->name;
//$month = $model_m->name;
//$array = array(1,2,3,4,5,0,0,0,0,0,0,0,0,0,0);
if (count($array) > 14){
    $height = ';height: 82vh';
}else{
    $height = '';
}

/** @var $model_grzdor app\models\Grzdor */
?>
<div class="my_table not_selected_text_on_block" style="height: 5vh;overflow-y: scroll;background-color: #eeeeee">
    <table>
        <tr>
            <td class="not_item" style="width: 5%;text-align: center">№</td>
            <td class="not_item">Фамилия и имя ребёнка</td>
            <td class="not_item" style="width: 20%">Группа здоровья</td>
            <td class="not_item" style="width: 15%">Диагноз</td>
            <td class="not_item" style="width: 15%">Физкуль</td>
        </tr>
    </table>
</div>
<div class="my_table my_table2 not_selected_text_on_block" style="overflow-y: scroll<?= $height ?>" id="grzdor_table1">
    <table class="table-striped table-bordered">

        <?php
        $i = 1;
        foreach($array as $q){

            if (!empty($q['number_fisculture'])){
                $number_fisculture = $model_grzdor::fiscultTypeLabels()[$q['number_fisculture']];
            }else{
                $number_fisculture = '';
            }

            $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.$q['rozd'].'</span>';

                echo '<tr data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-number_grzdor="'.$q['number_grzdor'] .'" data-comment="'.trim($q['comment']).'" data-number_fisculture="'.trim($q['number_fisculture']).'">
                <td style="width: 5%">'.$i.'</td>
                <td id="id_n">'.trim($q['name']).$rozd.'</td>
                <td style="width: 20%">'.$q['number_grzdor'].'</td>
                <td style="width: 15%">'.trim($q['comment']).'</td>
                <td style="width: 15%">'. $number_fisculture .'</td>
            </tr>';
            $i++;
        }

        ?>

    </table>
</div>



<?php
$script = <<<JS
$(function(){
    $('#grzdor_table1 tr').on('click',function(){

    $('#form_modal_grzdor_mebel,#form_modal_grzdor_mebel2').trigger('reset');
    var id_child = $(this).data('id_child');
    var name = $(this).data('name');
    var number_grzdor = $(this).data('number_grzdor');
    var check = parseInt($(this).data('number_grzdor'), 10);
    var comment = $(this).data('comment');
    var number_fisculture = $(this).data('number_fisculture');

    if(isNaN(check)){
        $('#id_hidden_modal_grzdor').val(0);//insert
        $('#number_grzdor_modal_grzdor').val(0);
        $('#number_fisculture_modal_grzdor').val(0);
        $('#but_del_grzdor_mebel').hide();
        $('#header_modal_grzdor_mebel').text('Добавить запись');
        $('#comment_modal_grzdor').val(null);
    }else{
        $('#id_hidden_modal_grzdor').val(1);//update
        $('#number_grzdor_modal_grzdor').val(number_grzdor);
        $('#number_fisculture_modal_grzdor').val(number_fisculture);
        $('#but_del_grzdor_mebel').show();
        $('#header_modal_grzdor_mebel').text('Редактировать запись');
        $('#but_'+number_grzdor).addClass('active');
        $('#but_fiscult'+number_fisculture).addClass('active');
        $('#comment_modal_grzdor').val(comment);
    }

    $('#name_modal_grzdor').val(name);
    $('#id_child_modal_grzdor').val(id_child);

    console.log(id_child);



    $('input:radio[name="grr"]').each(function(){
        if($(this).val() != number_grzdor){
            $('#but_'+$(this).val()).removeClass('active');
        }
    });

    $('input:radio[name="grr222"]').each(function(){
        if($(this).val() != number_fisculture){
            $('#but_fiscult'+$(this).val()).removeClass('active');
        }
    });





    $('#modal_grzdor_mebel').modal('show');
    return false;
});
})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

