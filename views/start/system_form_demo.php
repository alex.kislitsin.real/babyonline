<?php
use kartik\form\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;

$height = '40px';
?>

    <div style="margin: 0 25% 0 25%">

        <div class="well well-sm" style="text-align: center;background: ghostwhite"><h4>Отправьте нам заявку для получения <span style="color: #0000ff">демо доступа</span></h4></div>

        <?php $form = ActiveForm::begin([
            'id' => 'id_start_form_demo',
            'action' => ['start/system'],
            'method' => 'POST',
            'type' => ActiveForm::TYPE_VERTICAL,
//            'enableAjaxValidation'   => true,
//            'enableClientValidation' => true
        ]);?>

        <?= $form->field($model,'region')->textInput([
            'id' => 'id_start_demo_form_region',
            'placeholder' => 'Регион*',
            'style' => 'height: '.$height,
        ])->label(false);?>

        <?= $form->field($model,'city')->textInput([
            'id' => 'id_start_demo_form_city',
            'placeholder' => 'Населенный пункт*',
            'style' => 'height: '.$height,
        ])->label(false);?>

        <?= $form->field($model,'name')->textInput([
            'id' => 'id_start_demo_form_name',
            'placeholder' => 'Имя*',
            'style' => 'height: '.$height,
        ])->label(false);?>

        <?= $form->field($model,'name_org')->textInput([
            'id' => 'id_start_demo_form_name_org',
            'placeholder' => 'Название организации',
            'style' => 'height: '.$height,
        ])->label(false);?>

        <?= $form->field($model,'email')->textInput([
            'id' => 'id_start_demo_form_email',
            'placeholder' => 'Электронная почта*',
            'style' => 'height: '.$height,
        ])->label(false);?>

        <?= $form->field($model, 'tel')->widget('yii\widgets\MaskedInput', [
            'options' => [
                'id' => 'id_start_demo_form_tel',
                'placeholder' => 'Номер телефона',
                'style' => 'height: '.$height,
            ],
            'mask' => '+7(999) 999-99-99',

        ])->label(false);?>

        <?= $form->field($model, 'captcha')->widget(Captcha::className(),['captchaAction' => Url::to('/start/captcha')]) ?>

        <?= Html::button('Отправить заявку для получения демо доступа', ['class'=>'btn btn-md btn-success btn-block','id' => 'id_start_demo_form_button_send','style' => 'height: '.$height,]); ?>
        <br/>
        <div class="alert alert-danger alert-sm" id="id_start_demo_form_message" hidden="hidden">Вы ввели не все данные! (обязательные поля: Имя, адрес эл.почты, регион, нас.пункт)</div>
        <?php $form = ActiveForm::end()
        ?>


    </div>





<?php
$script = <<<JS
    $(function(){

        setTimeout(function(){
        $('#presentation-captcha').attr('placeholder','Проверочный код');
        }, 200);

        $('#id_start_demo_form_button_send').on('click',function(){

        var w1 = $('#id_start_demo_form_region').val().trim();
        var w2 = $('#id_start_demo_form_city').val().trim();
        var w3 = $('#id_start_demo_form_name').val().trim();
        var w4 = $('#id_start_demo_form_email').val().trim();

        if(w1.length == 0 || w2.length ==0 || w3.length ==0 || w4.length ==0){
            $('#id_start_demo_form_message').fadeIn().delay(7000).fadeOut();
            return;
        }

        var arr = $('#id_start_form_demo');
        $('#id_hidden_start').val(5);
        var form = $('#id_start_form_demo,#id_form_start_hidden').serializeArray();

            $.ajax({
                type: arr.attr('method'),
                url:  arr.attr('action'),
                data: form
            }).done(function(response){
            $("img[id$='-captcha-image']").trigger('click');
                $('#id_start_render_content_ajax').html(response);
            }).fail(function(){
            $("img[id$='-captcha-image']").trigger('click');
                alert('error');
            });

            return false;
        });





    })
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>