<?php use kartik\form\ActiveForm;
use yii\helpers\Html; ?>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header" style="margin-right: 100px">
                <div style="width: 150px;text-align: left;margin-top: 7px" class="not_selected_text_on_block" id="baby_label1_start">Baby Онлайн</div>
            </div>
            <ul class="nav navbar-nav">
<!--                <li><a href="index.php?r=start/test">Главная</a></li>-->
<!--                <li><a href="index.php?r=start/system">Главная</a></li>-->
                <li class="active"><a id="id_start_presentation" href="#">Презентация</a></li>
                <li><a id="id_start_contakts" href="#">Контакты</a></li>
<!--                <li class="dropdown">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Прочее <b class="caret"></b></a>-->
<!--                    <ul class="dropdown-menu">-->
<!--                        <li class="divider"></li>-->
<!--                        <li><a id="id_start_ask_questions" href="#">Вопросы и ответы</a></li>-->
<!--                        <li class="divider"></li>-->
<!--                        <li><a id="id_start_otzivi" href="#">Отзывы</a></li>-->
<!--                        <li class="divider"></li>-->
<!--                        <li><a href="#">Something else here</a></li>-->
<!--                        <li class="divider"></li>-->
<!--                        <li><a href="#">Separated link</a></li>-->
<!--                        <li class="divider"></li>-->
<!--                        <li><a href="#">One more separated link</a></li>-->
<!--                        <li class="divider"></li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li><a id="id_start_connect" href="#">Отправить заявку на подключение</a></li>
                <li><a id="id_start_demo" href="#"><span style="color: #f7ffa2">Демо доступ</span></a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href='index.php?r=site/login'><span class="glyphicon glyphicon-user"></span> Личный кабинет</a></li>
<!--                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
            </ul>


        </div>
    </nav>

    <div class="container-fliude" style="margin: 0 20% 0 20%" id="id_start_render_content_ajax">
               <?= $this->render('system5',compact('model')) ?>
    </div>

<?php $form = ActiveForm::begin([
    'id' => 'id_form_start_hidden',
    'action' => ['start/system'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_VERTICAL,
])?>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'id_hidden_start'
])->label(false); ?>
<?php $form = ActiveForm::end()?>

<?php
$script = <<<JS
    $(function(){

        $(".nav a").on("click", function(){
            $(".nav").find(".active").removeClass("active");
            $(this).parent().addClass("active");
        });

        $('#id_start_connect').on('click',function(){
//        $("img[id$='-captcha-image']").trigger('click');
//        $('input[id$=-captcha]').val('');
        var arr = $('#id_form_start_hidden');
        $('#id_hidden_start').val(0);
        var form = $('#id_form_start_hidden').serializeArray();
            $.ajax({
                type: arr.attr('method'),
                url:  arr.attr('action'),
                data: form
            }).done(function(response){
                $('#id_start_render_content_ajax').html(response);
                $("img[id$='-captcha-image']").trigger('click');
            }).fail(function(){
                alert('error');
            });

            return false;
        });

        $('#id_start_presentation').on('click',function(){
        $('#id_start_form_presentation').trigger('reset');
        var arr = $('#id_form_start_hidden');
        $('#id_hidden_start').val(2);
        var form = $('#id_form_start_hidden').serializeArray();
            $.ajax({
                type: arr.attr('method'),
                url:  arr.attr('action'),
                data: form
            }).done(function(response){
                $('#id_start_render_content_ajax').html(response);
            }).fail(function(){
                alert('error');
            });

            return false;
        });

        $('#id_start_contakts').on('click',function(){
        var arr = $('#id_form_start_hidden');
        $('#id_hidden_start').val(3);
        var form = $('#id_form_start_hidden').serializeArray();
            $.ajax({
                type: arr.attr('method'),
                url:  arr.attr('action'),
                data: form
            }).done(function(response){
                $('#id_start_render_content_ajax').html(response);
            }).fail(function(){
                alert('error');
            });

            return false;
        });

        $('#id_start_demo').on('click',function(){
        var arr = $('#id_form_start_hidden');
        $('#id_hidden_start').val(4);
        var form = $('#id_form_start_hidden').serializeArray();
            $.ajax({
                type: arr.attr('method'),
                url:  arr.attr('action'),
                data: form
            }).done(function(response){
                $('#id_start_render_content_ajax').html(response);
                $("img[id$='-captcha-image']").trigger('click');
            }).fail(function(){
                alert('error');
            });

            return false;
        });





    })
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>