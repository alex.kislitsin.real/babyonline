<?php
use kv4nt\owlcarousel\OwlCarouselWidget;

OwlCarouselWidget::begin([
    'container' => 'div',
    'assetType' => OwlCarouselWidget::ASSET_TYPE_CDN,
    'containerOptions' => [
        'id' => 'container-id',
        'class' => 'container-class owl-theme'
    ],
    'pluginOptions'    => [
        'navText'           => ['<div class="btn btn-default btn-lg" style="width: 120px">назад</div>','<div class="btn btn-default btn-lg" style="width: 120px">вперёд</div>'],
        'nav'               => true,
        'margin'            => 20,
        'autoplay'          => false,
        'lazyLoad'          => true,
        'autoplayTimeout'   => 300000,
        'items'             => 1,
        'loop'              => true,
        'itemsDesktop'      => [1199, 3],
        'itemsDesktopSmall' => [979, 3]
    ]
]);

//$i = 1;
?>

    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide_start.jpg" alt="Image 1"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide1.JPG" alt="Image 4"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide3.JPG" alt="Image 6"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide4.JPG" alt="Image 7"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide5.JPG" alt="Image 8"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide8.JPG" alt="Image 11"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide24.JPG" alt="Image 27"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide_comics.jpg" alt="Image 2"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide7.JPG" alt="Image 10"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide6.JPG" alt="Image 9"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide10.JPG" alt="Image 13"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide11.JPG" alt="Image 14"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide12.JPG" alt="Image 15"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide13.JPG" alt="Image 16"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide16.JPG" alt="Image 19"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide17.JPG" alt="Image 20"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide18.JPG" alt="Image 21"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide20.JPG" alt="Image 23"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide9.JPG" alt="Image 12"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide14.JPG" alt="Image 17"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide15.JPG" alt="Image 18"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide23.JPG" alt="Image 26"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide2.JPG" alt="Image 5"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide21.JPG" alt="Image 24"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide19.JPG" alt="Image 22"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide22.JPG" alt="Image 25"></div>
	<div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide25.JPG" alt="Image 26"></div>
	<div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide26.JPG" alt="Image 27"></div>
    <div class="item-class"><img class=" item-class_img" src="/images/presentation_baby/slide_end.jpg" alt="Image 3"></div>









<?php OwlCarouselWidget::end(); ?>